#!/usr/bin/env python3
###########################################################################################
#  package:   pNbody
#  file:      test_cooling.py
#  brief:     unit test for cooling.py
#  copyright: GPLv3
#             Copyright (C) 2019 EPFL (Ecole Polytechnique Federale de Lausanne)
#             LASTRO - Laboratory of Astrophysics of EPFL
#  author:    Mladen Ivkovic <mladen.ivkovic@hotmail.com>
#
# This file is part of pNbody.
###########################################################################################

import pNbody.cooling as cooling

cooling.check()
