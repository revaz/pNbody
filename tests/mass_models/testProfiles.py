#!/usr/bin/env python3

import unittest
from pNbody.mass_models import nfw
from pNbody.mass_models import gen_2_slopes
from astropy import units as u
from astropy import constants as ctes
from astropy.cosmology import Planck18 as cosmo
import numpy as np
  

class TestProfiles(unittest.TestCase):


    def testNFW(self):
    
      # the old implementation
      
      rho0 = 0.007
      rs   = 0.5
      r    = 1.
      G    = 1.
      density = nfw.Density(rho0, rs, r, G=1.)
      self.assertEqual(density,0.0003888888888888889)
    
      # the new object
      model = nfw.NFW(rho0,rs)
      self.assertEqual(model.Density(r),0.0003888888888888889)
      
      
      # check the new implementation agains the new one
      self.assertEqual(model.Potential(r)     ,nfw.Potential(rho0, rs, r, G=1.))
      self.assertEqual(model.CumulativeMass(r),nfw.CumulativeMass(rho0, rs, r, G=1.))
      self.assertEqual(model.Density(r)       ,nfw.Density(rho0, rs, r, G=1.))
      self.assertEqual(model.Vcirc(r)         ,nfw.Vcirc(rho0, rs, r, G=1.))
      self.assertEqual(model.Kappa2(r)        ,nfw.Kappa2(rho0, rs, r, G=1.))
      self.assertEqual(model.Kappa(r)         ,nfw.Kappa(rho0, rs, r, G=1.))
      
      # alternative parametrisation
      u_Length   = 1* u.kpc
      u_Mass     = 10**10 * u.M_sun
      u_Velocity = 1* u.km/u.s
      u_Time     = u_Length/u_Velocity 

      c    = 8
      M200 = (1e9*u.Msun).to(u_Mass).value
      H0   = cosmo.H0.to(1/u_Time).value
      G    = ctes.G.to(u_Length**3/(u_Mass * u_Time**2)).value
      
      # other parametrisation
      rho0  = 0.000331469
      rs   = 2.6376006852933727
      
      # bins
      r = 10**np.linspace(-3,3,1000)
      model1 = nfw.NFW(c=c,M200=M200,G=G,H0=H0)
      density1 = model1.Density(r)

      model2 = nfw.NFW(rho0=rho0,rs=rs,G=G)
      density2 = model2.Density(r)
      
      '''
      from matplotlib import pyplot as plt
      plt.plot(r,density1)
      plt.plot(r,density2)
      plt.loglog()
      plt.show()
      '''
      
    def testGEN2SLOPES(self):
    
      # the old implementation
      
      rho0 = 0.007
      rs   = 0.5
      r    = 1.
      G    = 1.
      alpha= 1
      beta = 3
      density = gen_2_slopes.Density(rho0=rho0, r_s=rs, alpha=alpha, beta=beta, r=r, G=1.)
      self.assertEqual(density,0.0003888888888888889)

      # the new object
      model = gen_2_slopes.GEN2SLOPES(alpha=alpha,beta=beta,rho0=rho0,rs=rs)
      self.assertEqual(model.Density(r),0.0003888888888888889)

      # check the new implementation agains the new one
      self.assertEqual(model.Potential(r)     ,gen_2_slopes.Potential(rho0, rs, alpha ,beta, r, G=1.))
      self.assertEqual(model.CumulativeMass(r),gen_2_slopes.CumulativeMass(rho0, rs, alpha, beta, r, G=1.))
      self.assertEqual(model.Density(r)       ,gen_2_slopes.Density(rho0, rs, alpha, beta, r, G=1.))
      self.assertEqual(model.Vcirc(r)         ,gen_2_slopes.Vcirc(rho0, rs, alpha, beta, r, G=1.))
      
      
      
      # alternative parametrisation
      u_Length   = 1* u.kpc
      u_Mass     = 10**10 * u.M_sun
      u_Velocity = 1* u.km/u.s
      u_Time     = u_Length/u_Velocity 

      alpha = 1
      beta  = 3
      c     = 8
      M200  = (1e9*u.Msun).to(u_Mass).value
      H0    = cosmo.H0.to(1/u_Time).value
      G     = ctes.G.to(u_Length**3/(u_Mass * u_Time**2)).value
      
      # other parametrisation
      rho0  = 0.000331469
      rs   = 2.6376006852933727
      
      # bins
      r = 10**np.linspace(-3,3,1000)
      model1 = gen_2_slopes.GEN2SLOPES(alpha=alpha,beta=beta,c=c,M200=M200,G=G,H0=H0)
      density1 = model1.Density(r)

      model2 = gen_2_slopes.GEN2SLOPES(alpha=alpha,beta=beta,rho0=rho0,rs=rs,G=G)
      density2 = model2.Density(r)
      
      '''
      from matplotlib import pyplot as plt
      plt.plot(r,density1)
      plt.plot(r,density2)
      plt.loglog()
      plt.show()
      '''






if __name__ == '__main__':
    unittest.main()
