#!/usr/bin/env python3

import unittest
import os
from pNbody import Nbody
from pNbody import ic
import numpy as np


  

class TestSelection(unittest.TestCase):

  def setUp(self):
    
    self.filename = "tmp.hdf5"
    
    # generate an nbody object
    n = 10000
    e = 0.1
    rmax = 10
    M0 = 1e5
    nb = ic.plummer(n,1,1,1,e,rmax,M=1,irand=1,vel='no',ftype="swift")
    nb.write(self.filename)

  def tearDown(self):
    
    os.remove(self.filename) 

      
  def test_selectc(self):
    
    nb = Nbody(self.filename)
    nb1 = nb.selectc(nb.rxyz()<0.1)
    self.assertEqual(nb1.nbody, 3571)
    self.assertEqual(nb1.get_list_of_arrays(), ['mass', 'num', 'pos', 'tpe', 'vel'])
    
  def test_selecti(self):
    
    nb = Nbody(self.filename)
    idx = np.arange(nb.nbody//2)
    nb1 = nb.selecti(idx)
    # we test num here as here it is equivalent to idx
    self.assertEqual(list(nb1.num), list(idx))


  def test_selectp(self):
    
    nb = Nbody(self.filename)
    idx = np.arange(nb.nbody//2)
    nb1 = nb.selectp(lst=idx)
    self.assertEqual(list(nb1.num), list(idx))

  def test_sort(self):
    
    nb = Nbody(self.filename)
    vec = nb.x()
    nb1 = nb.sort(vec)
    self.assertEqual(list(nb1.num[:3]), [8840, 8967, 5240])
    
  
  def test_sort_type(self):
    
    nb = Nbody(self.filename)
    vec = nb.x()
    nb1 = nb.sort_type(vec)
    #self.assertEqual(list(nb1.num[:3]), [8840, 8967, 5240])        
    


if __name__ == '__main__':
    unittest.main()
