#!/usr/bin/env python3
###########################################################################################
#  package:   pNbody
#  file:      test_fourier.py
#  brief:     unit test for fourier.py
#  copyright: GPLv3
#             Copyright (C) 2019 EPFL (Ecole Polytechnique Federale de Lausanne)
#             LASTRO - Laboratory of Astrophysics of EPFL
#  author:    Mladen Ivkovic <mladen.ivkovic@hotmail.com>
#
# This file is part of pNbody.
###########################################################################################
import ut_helpers as h
import pNbody.fourier as f
from numpy import linspace, sin



# just calling fourier.fourier should suffice for now,
# it calls the only other function defined in the file
h.announce('fourier()')
t = linspace(0,1000,50)
x = sin(t)
y = sin(2*t)
print(f.fourier(x,y))


