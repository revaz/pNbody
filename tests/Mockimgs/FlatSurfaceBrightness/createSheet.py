#!/usr/bin/env python3
###########################################################################################
#  package:   pNbody
#  file:      createSheet.sh
#  brief:     Run the FlatSurfaceBrightness test
#  copyright: GPLv3
#             Copyright (C) 2023 EPFL (Ecole Polytechnique Federale de Lausanne)
#             LASTRO - Laboratory of Astrophysics of EPFL
#  author:    Yves Revaz <yves.revaz@epfl.ch>
#
# This file is part of pNbody.
###########################################################################################


import numpy as np
from pNbody import *
from astropy import units as u

# define units
u_Length   = 1* u.kpc
u_Mass     = 10**10 * u.M_sun
u_Velocity = 1* u.km/u.s
u_Time     = u_Length/u_Velocity
toMsol     = u_Mass.to(u.M_sun).value

n = 100000
outputfile = "sheet.hdf5"

x = 25*np.random.random(n) -12            # <<<  x in kpc
y = 25*np.random.random(n) -12            # <<<  y in kpc
z = np.zeros(n)                     # <<<  z in kpc
pos = np.transpose(np.array([x, y, z]))
mass = np.ones(n)/n * 1e4          # <<<  mass in 10^10 solar mass

# create the pNbody object
nb = Nbody(status='new',pos=pos,mass=mass,ftype='swift')
# set all particles to stellar particles
nb.set_tpe(4)

nb.age                =   np.ones(n)*10             # <<< age in Gyr
nb.mh                 =   np.ones(n)* -2            # <<< metallicity  [M/H]
nb.ComputeRsp(5)                                    # rsp


# add units
nb.UnitLength_in_cm         = u_Length.to(u.cm).value
nb.UnitMass_in_g            = u_Mass.to(u.g).value
nb.UnitVelocity_in_cm_per_s = u_Velocity.to(u.cm/u.s).value
nb.Unit_time_in_cgs         = u_Time.to(u.s).value

nb.hubblefactorcorrection      = False
nb.comovingtoproperconversion  = False
nb.atime                       = 1

nb.rename(outputfile)
nb.write()
