#!/usr/bin/env python3

import numpy as np
import argparse
import matplotlib.pyplot as plt
import os
from astropy.io import fits

description="plot the histogram of pixel values"
epilog     ="""
Examples:
--------
./plotHistogram.py tmp1.fits.gz  tmp2.fits.gz  tmp3.fits.gz

"""

parser = argparse.ArgumentParser(description=description,epilog=epilog,formatter_class=argparse.RawDescriptionHelpFormatter)


parser.add_argument(action="store", 
                    dest="files", 
                    metavar='FILE', 
                    type=str,
                    default=None,
                    nargs='*',
                    help='a list of fits files') 


####################################################################
# main
####################################################################



if __name__ == '__main__':


  opt = parser.parse_args()
  
  for f in opt.files:
    
    hdul = fits.open(f)
    image = hdul[0].data
    
    image = image.ravel()
    image = np.compress(image!=100,image)
    
    bins = np.linspace(12,20,100)
    plt.hist(image,bins,label=f)
    
  plt.legend()
  plt.xlabel("Magnitude")    
  plt.ylabel("Counts")    
  plt.semilogy()

plt.show()  
  
