#!/usr/bin/env bash
###########################################################################################
#  package:   pNbody
#  file:      run.sh
#  brief:     Run the FlatSurfaceBrightness test
#  copyright: GPLv3
#             Copyright (C) 2023 EPFL (Ecole Polytechnique Federale de Lausanne)
#             LASTRO - Laboratory of Astrophysics of EPFL
#  author:    Yves Revaz <yves.revaz@epfl.ch>
#
# This file is part of pNbody.
###########################################################################################

# generate the sheet (N-body model)
./createSheet.py 

# generate a surface brightness map
mockimgs_sb_compute_images sheet.hdf5 --instrument arrakihs_vis_SDSSg.py --distance 5  -o tmp1.fits

# generate a surface brightness map
mockimgs_sb_compute_images sheet.hdf5 --instrument arrakihs_vis_SDSSg.py --distance 20  -o tmp2.fits

# generate a surface brightness map
mockimgs_sb_compute_images sheet.hdf5 --instrument arrakihs_vis_SDSSg.py --distance 40  -o tmp3.fits

# generate a surface brightness map
mockimgs_sb_compute_images sheet.hdf5 --instrument arrakihs_vis_SDSSg.py --distance 60  -o tmp4.fits

# plot the histogram of the three 
./plotHistogram.py tmp1.fits.gz tmp2.fits.gz tmp3.fits.gz tmp4.fits.gz
