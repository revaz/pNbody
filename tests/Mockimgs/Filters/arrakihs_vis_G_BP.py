instrument = instrument.Instrument(
  name        = "arrakihs_vis_G_BP",
  telescope   = telescope.Telescope(name="iSIM-170",focal=1477.5*u.mm),
  ccd         = ccd.CCD(name="CCD273-84"   ,shape=[3400,3400],pixel_size=[12*u.micron,12*u.micron]),
  filter_type = filters.Filter("BastI_GAIA_G_BP"),
  )
