#!/usr/bin/env python3

import unittest
from pNbody.Mockimgs import filters
from pNbody.Mockimgs import luminosities
from pNbody import *

class TestFilters(unittest.TestCase):

    def setUp(self):
      
      self.Mass = 1e11 # in Msol
      self.Age  = 13   # in Gyr
      self.MH   = -2   # in log10([M/H])

    
    def test_BastI_GAIA_G(self):
      # set the filter
      Filter = filters.Filter("BastI_GAIA_G")
      # compute the magnitude
      mag  = Filter.Magnitudes(self.Mass,self.Age,self.MH)
      self.assertEqual(mag, -21.26646547856253)

    def test_BastI_GAIA_G_RP(self):
      # set the filter
      Filter = filters.Filter("BastI_GAIA_G_RP")
      # compute the magnitude
      mag  = Filter.Magnitudes(self.Mass,self.Age,self.MH)
      self.assertEqual(mag, -21.824488103255256)      
      
    def test_BastI_GAIA_G_RP(self):
      # set the filter
      Filter = filters.Filter("BastI_GAIA_G_BP")
      # compute the magnitude
      mag  = Filter.Magnitudes(self.Mass,self.Age,self.MH)
      self.assertEqual(mag, -20.888389844950517)        

    def test_BastI_SDSS_u(self):
      # set the filter
      Filter = filters.Filter("BastI_SDSS_u")
      # compute the magnitude
      mag  = Filter.Magnitudes(self.Mass,self.Age,self.MH)
      self.assertEqual(mag, -19.64066074370712)         
      
    def test_BastI_SDSS_g(self):
      # set the filter
      Filter = filters.Filter("BastI_SDSS_g")
      # compute the magnitude
      mag  = Filter.Magnitudes(self.Mass,self.Age,self.MH)
      self.assertEqual(mag, -20.733379900666957)      
      
    def test_BastI_SDSS_r(self):
      # set the filter
      Filter = filters.Filter("BastI_SDSS_r")
      # compute the magnitude
      mag  = Filter.Magnitudes(self.Mass,self.Age,self.MH)
      self.assertEqual(mag, -21.247028763181742) 
      
    def test_BastI_SDSS_i(self):
      # set the filter
      Filter = filters.Filter("BastI_SDSS_i")
      # compute the magnitude
      mag  = Filter.Magnitudes(self.Mass,self.Age,self.MH)
      self.assertEqual(mag, -21.474948940518082)       
      
    def test_BastI_SDSS_z(self):
      # set the filter
      Filter = filters.Filter("BastI_SDSS_z")
      # compute the magnitude
      mag  = Filter.Magnitudes(self.Mass,self.Age,self.MH)
      self.assertEqual(mag, -21.60031592058328)      
      
    def test_BastI_HST_F475X(self):
      # set the filter
      Filter = filters.Filter("BastI_HST_F475X")
      # compute the magnitude
      mag  = Filter.Magnitudes(self.Mass,self.Age,self.MH)
      self.assertEqual(mag, -20.770540802110858)

    def test_BastI_Euclid_VIS(self):
      # set the filter
      Filter = filters.Filter("BastI_Euclid_VIS")
      # compute the magnitude
      mag  = Filter.Magnitudes(self.Mass,self.Age,self.MH)
      self.assertEqual(mag, -21.375807313643893)

    def test_BastI_Euclid_J(self):
      # set the filter
      Filter = filters.Filter("BastI_Euclid_J")
      # compute the magnitude
      mag  = Filter.Magnitudes(self.Mass,self.Age,self.MH)
      self.assertEqual(mag, -21.689991937036552)
      
    def test_BastI_Euclid_Y(self):
      # set the filter
      Filter = filters.Filter("BastI_Euclid_Y")
      # compute the magnitude
      mag  = Filter.Magnitudes(self.Mass,self.Age,self.MH)
      self.assertEqual(mag, -21.640684273653346)
      
    def test_BastI_Euclid_H(self):
      # set the filter
      Filter = filters.Filter("BastI_Euclid_H")
      # compute the magnitude
      mag  = Filter.Magnitudes(self.Mass,self.Age,self.MH)
      self.assertEqual(mag, -21.62164146810174)      

    def test_GAEA_B(self):
      # set the filter
      Filter = filters.Filter("GAEA_B")
      # compute the magnitude
      mag  = Filter.Magnitudes(self.Mass,self.Age,self.MH)
      self.assertEqual(mag, -20.667489302168747)
      
    def test_CMD_F475X(self):
      # set the filter
      Filter = filters.Filter("CMD_F475X")
      # compute the magnitude
      mag  = Filter.Magnitudes(self.Mass,self.Age,self.MH)
      self.assertEqual(mag, -20.81532531220846)
      

    def test_BastI_GAIA_G_current_mass(self):
      # set the filter
      Filter = filters.Filter("BastI_GAIA_G")
      # compute the magnitude
      mag  = Filter.Magnitudes(self.Mass,self.Age,self.MH,current_mass=True)
      self.assertEqual(mag, -21.86224131362)
   
    
    
    
    
    # Luminosities
    
    def test_CMD_L(self):
      # set the filter
      LuminosityModel = luminosities.LuminosityModel("CMD_L")
      # compute the magnitude
      L  = LuminosityModel.Luminosities(self.Mass,self.Age,self.MH)
      self.assertEqual(L, 27823186798.92422)

    '''
    def test_BastI_L(self):
      # set the filter
      LuminosityModel = luminosities.LuminosityModel("BastI_L")
      # compute the magnitude
      L  = LuminosityModel.Luminosities(self.Mass,self.Age,self.MH)
      self.assertEqual(L, 27927040055.22935)

    def test_BastI_L_current_mass(self):
      # set the filter
      LuminosityModel = luminosities.LuminosityModel("BastI_L")
      # compute the magnitude
      L  = LuminosityModel.Luminosities(self.Mass,self.Age,self.MH,current_mass=True)
      self.assertEqual(L, 48343183024.88294)
    
    
    def test_test_BastI_on_snapshot(self):
      
      nb = Nbody("GG_h177_004_snapshot_0255_stars.hdf5")
      InitialMass = nb.InitialMass(units="Msol")
      FinalMass   = nb.Mass(units="Msol") 
      Age  = nb.StellarAge(units="Gyr")
      MH   = nb.MetalsH()
      
      LuminosityModel = luminosities.LuminosityModel("BastI_L")
      Mratio  = LuminosityModel.SSPMassRatio(Age,MH)
      
      L1  = LuminosityModel.Luminosities(InitialMass,Age,MH,current_mass=False)
      L2  = LuminosityModel.Luminosities(FinalMass,Age,MH,current_mass=True)
      
      self.assertEqual(L1.sum(), 171964.27784392535)
      self.assertEqual(L2.sum(), 263332.8109256794)
      
      Filter = filters.Filter("BastI_GAIA_G")
      Mag1  = Filter.Magnitudes(InitialMass,Age,MH,current_mass=False)
      Mag2  = Filter.Magnitudes(FinalMass,Age,MH,current_mass=True)
    '''  
      
          

if __name__ == '__main__':
    unittest.main()
