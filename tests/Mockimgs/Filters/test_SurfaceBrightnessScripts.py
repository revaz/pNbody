#!/usr/bin/env python3

import unittest
from pNbody import *
import os
from astropy.io import fits

def getPlummer(n=10000):
  
  from pNbody import ic
  from astropy import units as u

  
  # generate an nbody object
  e = 0.1
  rmax = 10
  M0 = 1e5
  minFe = -4 
  maxFe = -2 
  minAge= 12500  
  maxAge= 13500
  nb = ic.plummer(n,1,1,1,e,rmax,M=1,irand=1,vel='no',ftype="swift")  
  
  if n==1:
    nb.pos = nb.pos*0
  
  
  # set particles to be stars
  md = nb.getParticleMatchingDict()
  nb.set_tpe(md["stars"])
  
  nb.mass  = np.ones(n)*M0/n/1e10 # output units are in 1e10 Msol
  nb.age   = np.random.uniform(minAge,maxAge,n)    
  nb.mh    = np.random.uniform(minFe,maxFe,n)  
  
  if n > 1:
    nb.ComputeRsp(5)
    nb.rsp = nb.rsp * 0.5
  else:
    nb.rsp = np.ones(nb.nbody)
  
  # define units
  u_Length   = 1* u.kpc
  u_Mass     = 10**10 * u.M_sun
  u_Velocity = 1* u.km/u.s
  u_Time     = u_Length/u_Velocity 
  toMsol     = u_Mass.to(u.M_sun).value
  
  
  # add units
  nb.UnitLength_in_cm         = u_Length.to(u.cm).value
  nb.UnitMass_in_g            = u_Mass.to(u.g).value
  nb.UnitVelocity_in_cm_per_s = u_Velocity.to(u.cm/u.s).value
  nb.Unit_time_in_cgs         = u_Time.to(u.s).value  
  
  # non cosmological #galaxy = Object(filename=opt.file,unitsfile=opt.unitsfile,adjust_coords=adjust_coords,distance=opt.distance*u.Mpc)
  nb.setComovingIntegrationOff()
  nb.cosmorun=0# add the object to the instrument
  nb.time    =0#instrument.add_object(galaxy)  
  
  return nb




class TestSB(unittest.TestCase):
  
  def test_addfields(self):
  
    # add some additional fields
    out = os.system("python3 ../../../scripts/mockimgs/mockimgs_sb_addfields GG_h177_004_snapshot_0255_stars.hdf5 --do_not_compute_magnitudes -o tmp.hdf5")
    self.assertEqual(out, 0)
    

  def test_mockimgs_sb_compute_images_1(self):
      
    # generate the surface brightness image
    out = os.system("python3 ../../../scripts/mockimgs/mockimgs_sb_compute_images tmp.hdf5 --instrument arrakihs_vis_G.py --distance 35 --fov 60 --rsp_fac=0.1 -o tmp.fits")
    self.assertEqual(out, 0)
            
    # now, we can check the content of the fits file
    hdu   = fits.open("tmp.fits.gz")
    data1 = hdu[0].data  

    hdu   = fits.open("ref1.fits.gz")
    data2 = hdu[0].data 
    
    self.assertEqual(data1.tolist(), data2.tolist()) 
    os.remove("tmp.fits.gz")
    


  def test_mockimgs_sb_compute_images_2(self):
    
    # generate the surface brightness image
    out = os.system("python3 ../../../scripts/mockimgs/mockimgs_sb_compute_images tmp.hdf5 --instrument arrakihs_vis_G_BP.py --distance 35 --fov 120 --rsp_fac=10 -o tmp.fits")
    self.assertEqual(out, 0)
            
    # now, we can check the content of the fits file
    hdu   = fits.open("tmp.fits.gz")
    data1 = hdu[0].data  

    hdu   = fits.open("ref2.fits.gz")
    data2 = hdu[0].data 
    
    self.assertEqual(data1.tolist(), data2.tolist())     
    os.remove("tmp.fits.gz")
    
  def test_mockimgs_sb_compute_images_mass_point(self):
    
    nb = getPlummer(1)
    nb.write("tmp.hdf5")
        
    # generate a surface brightness image
    out = os.system("python3 ../../../scripts/mockimgs/mockimgs_sb_compute_images tmp.hdf5 --instrument instrument1.py --distance 35 --fov 120 --rsp_fac=1 -o tmp1.fits")
    self.assertEqual(out, 0)

    # generate a surface brightness image
    out = os.system("python3 ../../../scripts/mockimgs/mockimgs_sb_compute_images tmp.hdf5 --instrument instrument2.py --distance 35 --fov 120 --rsp_fac=1 -o tmp2.fits")
    self.assertEqual(out, 0)          

if __name__ == '__main__':
    unittest.main()
