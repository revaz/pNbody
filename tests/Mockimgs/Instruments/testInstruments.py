#!/usr/bin/env python3

import unittest
from pNbody.Mockimgs import ccd
from pNbody.Mockimgs import instrument
from pNbody.Mockimgs.instrument import Instrument 
from pNbody.Mockimgs import telescope
from pNbody.Mockimgs import filters
from pNbody.Mockimgs.obs_object import Object
from pNbody import *
from pNbody import ic
from astropy import units as u
from astropy.io import fits

def getPlummer():
  
  # generate an nbody object
  n = 10000
  e = 0.1
  rmax = 10
  M0 = 1e5
  minFe = -4 
  maxFe = -2 
  minAge= 12500  
  maxAge= 13500
  nb = ic.plummer(n,1,1,1,e,rmax,M=1,irand=1,vel='no',ftype="swift")  
  
  # set particles to be stars
  md = nb.getParticleMatchingDict()
  nb.set_tpe(md["stars"])
  
  nb.mass  = np.ones(n)*M0/n/1e10 # output units are in 1e10 Msol
  nb.age   = np.random.uniform(minAge,maxAge,n)    
  nb.mh    = np.random.uniform(minFe,maxFe,n)  
  nb.ComputeRsp(5)
  nb.rsp = nb.rsp * 0.5
  
  # define units
  u_Length   = 1* u.kpc
  u_Mass     = 10**10 * u.M_sun
  u_Velocity = 1* u.km/u.s
  u_Time     = u_Length/u_Velocity 
  toMsol     = u_Mass.to(u.M_sun).value
  
  
  # add units
  nb.UnitLength_in_cm         = u_Length.to(u.cm).value
  nb.UnitMass_in_g            = u_Mass.to(u.g).value
  nb.UnitVelocity_in_cm_per_s = u_Velocity.to(u.cm/u.s).value
  nb.Unit_time_in_cgs         = u_Time.to(u.s).value  
  
  # non cosmological #galaxy = Object(filename=opt.file,unitsfile=opt.unitsfile,adjust_coords=adjust_coords,distance=opt.distance*u.Mpc)
  nb.setComovingIntegrationOff()
  nb.cosmorun=0# add the object to the instrument
  nb.time    =0#instrument.add_object(galaxy)  
  
  return nb
  



  

class TestFilters(unittest.TestCase):

    def setUp(self):
      
      pass
      # self.qq
      
      
    def utestCCD(self,CCD):
    
      # fov
      self.assertEqual(CCD.fov[0], 0.027742457524840645*u.rad)
      self.assertEqual(CCD.fov[1], 0.027742457524840645*u.rad)  
      
      # shape
      self.assertEqual(CCD.shape[0], 4096)
      self.assertEqual(CCD.shape[1], 4096)
      
      # size
      self.assertEqual(CCD.size[0], 41.0*u.mm)
      self.assertEqual(CCD.size[1], 41.0*u.mm)
      
      # pixel size
      self.assertEqual(CCD.pixel_size[0], 0.010009765625*u.mm)
      self.assertEqual(CCD.pixel_size[1], 0.010009765625*u.mm)
      
      # pixel fov
      self.assertEqual("%g"%CCD.pixel_fov[0].to(u.rad).value, "6.7748e-06")
      self.assertEqual("%g"%CCD.pixel_fov[1].to(u.rad).value, "6.7748e-06")
      
      # xmin, xmax, ymin, ymax
      self.assertEqual(CCD.xmin.to(u.deg),-0.7947628647471606*u.deg)
      self.assertEqual(CCD.xmax.to(u.deg), 0.7947628647471606*u.deg)
      self.assertEqual(CCD.ymin.to(u.deg),-0.7947628647471606*u.deg)
      self.assertEqual(CCD.ymax.to(u.deg), 0.7947628647471606*u.deg)     
      
      # xmin_kpc, xmax_kpc, ymin_kpc, ymax_kpc
      self.assertEqual(CCD.xmin_kpc,1.0*u.kpc)
      self.assertEqual(CCD.xmax_kpc,1.0*u.kpc)
      self.assertEqual(CCD.ymin_kpc,1.0*u.kpc)
      self.assertEqual(CCD.ymax_kpc,1.0*u.kpc)          
      
    
    def test_CCD(self):
      
      CCD         = ccd.CCD(name="arrakihs_vis",shape=[4096,4096],size=[41*u.mm,41*u.mm],focal=1477.5*u.mm)
      #CCD.info()
      self.utestCCD(CCD)

      CCD         = ccd.CCD(name="arrakihs_vis",shape=[4096,4096],pixel_size=[0.010009765625*u.mm,0.010009765625*u.mm],focal=1477.5*u.mm)
      self.utestCCD(CCD)

      CCD         = ccd.CCD(name="arrakihs_vis",shape=[4096,4096],pixel_size=[0.010009765625*u.mm,0.010009765625*u.mm],fov=[1.5895257294943212*u.deg,1.5895257294943212*u.deg])
      self.utestCCD(CCD)   
      
      CCD         = ccd.CCD(name="arrakihs_vis",shape=[4096,4096],size=[41*u.mm,41*u.mm],fov=[1.5895257294943212*u.deg,1.5895257294943212*u.deg])
      CCD.info()
      self.utestCCD(CCD)
      
      CCD         = ccd.CCD(name="arrakihs_vis",shape=[4096,4096],size=[41*u.mm,41*u.mm])
      CCD.set_focal(focal=1477.5*u.mm)
      self.utestCCD(CCD)      
      
      CCD         = ccd.CCD(name="arrakihs_vis",shape=[4096,4096],size=[41*u.mm,41*u.mm],focal=1477.5*u.mm)
      CCD.change_fov([0.15895257294943212*u.deg,0.15895257294943212*u.deg])
      CCD.info()
      
      
    def test_Instrument1(self):

      instrument = Instrument(
        name        = "Instrument1",
        telescope   = telescope.Telescope(name="iSIM-170",focal=1477.5*u.mm),
        ccd         = ccd.CCD(name="arrakihs_vis",shape=[4096,4096],size=[41*u.mm,41*u.mm]),
        filter_type = filters.Filter("BastI_GAIA_G"),
        )

      #Instrument.info()
      
      # fov
      self.assertEqual(instrument.fov()[0], 0.027742457524840645*u.rad)
      self.assertEqual(instrument.fov()[1], 0.027742457524840645*u.rad)  
      
      # shape
      self.assertEqual(instrument.ccd.shape[0], 4096)
      self.assertEqual(instrument.ccd.shape[1], 4096)
      
      # size
      self.assertEqual(instrument.ccd.size[0], 41.0*u.mm)
      self.assertEqual(instrument.ccd.size[1], 41.0*u.mm)
      
      # pixel size
      self.assertEqual(instrument.ccd.pixel_size[0], 0.010009765625*u.mm)
      self.assertEqual(instrument.ccd.pixel_size[1], 0.010009765625*u.mm)

      # pixel fov
      self.assertEqual(instrument.ccd.pixel_fov[0], 6.774799069270293e-06*u.rad)
      self.assertEqual(instrument.ccd.pixel_fov[1], 6.774799069270293e-06*u.rad)

      # xmin, xmax, ymin, ymax
      self.assertEqual(instrument.ccd.xmin.to(u.deg),-0.7947628647471606*u.deg)
      self.assertEqual(instrument.ccd.xmax.to(u.deg), 0.7947628647471606*u.deg)
      self.assertEqual(instrument.ccd.ymin.to(u.deg),-0.7947628647471606*u.deg)
      self.assertEqual(instrument.ccd.ymax.to(u.deg), 0.7947628647471606*u.deg)     

      # xmin_kpc, xmax_kpc, ymin_kpc, ymax_kpc
      self.assertEqual(instrument.ccd.xmin_kpc,1.0*u.kpc)
      self.assertEqual(instrument.ccd.xmax_kpc,1.0*u.kpc)
      self.assertEqual(instrument.ccd.ymin_kpc,1.0*u.kpc)
      self.assertEqual(instrument.ccd.ymax_kpc,1.0*u.kpc)         
      

    def test_Instrument2(self):
      """
      test the creation of an instrument as well as an object
      and compute a surface brightness
      """

      instrument = Instrument(
        name        = "Instrument1",
        telescope   = telescope.Telescope(name="iSIM-170",focal=1477.5*u.mm),
        ccd         = ccd.CCD(name="arrakihs_vis",shape=[4096,4096],size=[41*u.mm,41*u.mm]),
        filter_type = filters.Filter("BastI_GAIA_G"),
        )
      
      instrument.change_fov([100*u.arcsec,100*u.arcsec]) 
      
      nbody_file = "tmp.hdf5"
      distance   = 35*u.Mpc

      nb = getPlummer()
      nb.rename(nbody_file)
      nb.write()
      
      galaxy = Object(filename=nbody_file,distance=distance)
      instrument.add_object(galaxy)
      
      instrument.set_object_for_exposure()  
      instrument.ComputeFluxMap()
      instrument.FilterFluxMap()
      instrument.SurfaceBrightnessMap() 

      sb_image = instrument.getSurfaceBrightnessMap()
      instrument.saveFitsImage(sb_image,"tmp.fits",units="mag/arcsec^2")
      
      
      # compare images
      hdu   = fits.open("tmp.fits.gz")
      data1 = hdu[0].data  

      hdu   = fits.open("ref.fits.gz")
      data2 = hdu[0].data 
    
      #self.assertEqual(data1.tolist(), data2.tolist())     
      
      
      os.remove("tmp.fits.gz")   
      os.remove("tmp.hdf5")    
      

      


if __name__ == '__main__':
    unittest.main()
