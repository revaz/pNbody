#!/usr/bin/env python3

import unittest
import os
from pNbody import *
import filecmp
import numpy.testing as npt

class TestICs(unittest.TestCase):

  def test_multi_component_disk(self):
    """
    This tests the script makeDisk.py which is used
    to generate the IsolatedGalaxy_multi_component
    example in the Swift code
    """
      
    ref_file = "galaxy_multi_component.ref.hdf5"
    tmp_file = "galaxy_multi_component.hdf5"
  
    # add some additional fields
    cmd = "python3 ./makeDisk.py"
    print(cmd)
    out = os.system(cmd)
    self.assertEqual(out, 0)

    # reduce
    nb = Nbody(tmp_file)
    nb = nb.reduc(256)
    nb.write()
    
    # compare (this fails on gitlab, hdf5 are not stricktly the same)   
    #status = filecmp.cmp(tmp_file, ref_file)
    #self.assertTrue(status, 'file %s is different than file %s !'%(tmp_file,ref_file))    
    
    nb1 = Nbody(ref_file)
    nb2 = Nbody(tmp_file)
    
    npt.assert_array_equal(nb1.pos, nb2.pos)
    npt.assert_array_equal(nb1.vel, nb2.vel)
    npt.assert_array_equal(nb1.mass, nb2.mass)
    

  def test_ic_nfw(self):

    ref_file = "nfw.ref.hdf5"
    tmp_file = "nfw.hdf5"
    
    cmd = "python3 ../../scripts/ic/ic_nfw  -q --Rmin 1e-3 --Rmax 105 --M200 1.0e9 --c 17 --ptype 1 --mass 5e3  -t swift -o %s"%tmp_file
    print(cmd)    
    out = os.system(cmd)
    self.assertEqual(out, 0) 

    # reduce
    nb = Nbody(tmp_file)
    nb = nb.reduc(256)
    nb.write()

    # compare (this fails on gitlab, hdf5 are not stricktly the same)   
    #status = filecmp.cmp(tmp_file, ref_file)
    #self.assertTrue(status, 'file %s is different than file %s !'%(tmp_file,ref_file))    
    
    nb1 = Nbody(ref_file)
    nb2 = Nbody(tmp_file)
    
    npt.assert_array_equal(nb1.pos, nb2.pos)
    npt.assert_array_equal(nb1.vel, nb2.vel)
    npt.assert_array_equal(nb1.mass, nb2.mass)
    
    

  def test_ic_nfw_plummer(self):

    ref_file = "nfw+plummer.ref.hdf5"
    tmp_file = "nfw+plummer.hdf5"
        
    cmd = "python3 ../../scripts/ic/ic_nfw+plummer -q --Rmin 1e-3 --Rmax 30  --rho0 0.007 --rs 0.5   --Mtot 1e5 -a 0.1 --ptype1 4 --ptype2 1  --mass1 1000 --mass2 1000  -t swift -o %s"%tmp_file   
    print(cmd) 
    out = os.system(cmd)
    self.assertEqual(out, 0) 

    # reduce
    nb = Nbody(tmp_file)
    nb = nb.reduc(256)
    nb.write()

    # compare (this fails on gitlab, hdf5 are not stricktly the same)   
    #status = filecmp.cmp(tmp_file, ref_file)
    #self.assertTrue(status, 'file %s is different than file %s !'%(tmp_file,ref_file))    
    
    nb1 = Nbody(ref_file)
    nb2 = Nbody(tmp_file)
    
    npt.assert_array_equal(nb1.pos, nb2.pos)
    npt.assert_array_equal(nb1.vel, nb2.vel)
    npt.assert_array_equal(nb1.mass, nb2.mass)


  def test_ic_gen_2_slopes_plummer(self):

    ref_file = "gen2+plummer.ref.hdf5"
    tmp_file = "gen2+plummer.hdf5"
    
    cmd = "python3 ../../scripts/ic/ic_gen_2_slopes+plummer -q --power_cutoff 8 --r_cutoff 30 --alpha 1 --beta 3 --Rmin 1e-3 --Rmax 30 --Mtot 1e5 -a 0.1 --ptype1 4 --ptype2 1 --mass1 1000 --mass2 1000 -t swift --rho0 0.0012 --rs 1.5 -o %s"%tmp_file
    print(cmd)    
    out = os.system(cmd)
    self.assertEqual(out, 0) 

    # reduce
    nb = Nbody(tmp_file)
    nb = nb.reduc(256)
    nb.write()

    # compare (this fails on gitlab, hdf5 are not stricktly the same)   
    #status = filecmp.cmp(tmp_file, ref_file)
    #self.assertTrue(status, 'file %s is different than file %s !'%(tmp_file,ref_file))    
    
    nb1 = Nbody(ref_file)
    nb2 = Nbody(tmp_file)
    
    npt.assert_array_equal(nb1.pos, nb2.pos)
    npt.assert_array_equal(nb1.vel, nb2.vel)
    npt.assert_array_equal(nb1.mass, nb2.mass)
    
    



if __name__ == '__main__':
    unittest.main()
