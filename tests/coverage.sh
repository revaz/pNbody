#!/bin/bash

###########################################################################################
#  package:   pNbody
#  file:      coverage.sh
#  brief:     Run tests with coverage
#  copyright: GPLv3
#             Copyright (C) 2019 EPFL (Ecole Polytechnique Federale de Lausanne)
#             LASTRO - Laboratory of Astrophysics of EPFL
#  author:    Loic Hausammann <loic_hausammann@hotmail.com>
#
# This file is part of pNbody.
###########################################################################################

rm .coverage
rm .coverage.*


function cleanup {
    # clean up after yourself after you're done playing
    rm *.dat
    rm *.dmp
    rm params*
    rm -r png
    rm img.fits merge.gad dsph.dat output
}

function test_python_script {
    # Crash this script if python script crashes
    python3 -m coverage run $1
    CODE=$?
    if [[ $CODE -ne 0 ]]; then
      echo $1 " Exit Code " $CODE
      echo "Test failed"
      cleanup
      exit 1
    fi
}


function mpitest {
    # Crash this script if python script crashes, for scripts using MPI
    if [[ "`mpirun --version`" == *"mpich"* ]]; then
        # assume mpich
        mpirun -n 2 python3 -m coverage run $1
        if [[ $? -ne 0 ]]; then
          echo $1 " Exit Code " $CODE
          echo "Test failed"
          cleanup
          exit 1
        fi
    elif [[ "`mpirun --version`" == *"Open MPI"* ]]; then
        # assume openmpi
        mpirun --allow-run-as-root -n 2 python3 -m coverage run $1
        if [[ $? -ne 0 ]]; then
          echo $1 " Exit Code " $CODE
          echo "Test failed"
          cleanup
          exit 1
        fi
    fi
}




#--------------------------
# Original tests
#--------------------------

test_python_script "basic_test.py"
test_python_script "io_test.py"
test_python_script "format_test.py"
#mpitest "pio_test.py"
test_python_script "grid_test.py"



#--------------------------
# unit tests
#--------------------------

# test "python3 -m coverage run ut_test_cooling.py" # doesn't work yet
#test "ut_test_cosmo.py"
test_python_script "ut_test_fourier.py"
test_python_script "ut_test_main.py"
#test "ut_test_mpi.py"
test_python_script "ut_test_thermodyn.py"
test_python_script "ut_test_units.py"
#mpitest "ut_test_mpi.py"


#--------------------------
# Examples
#--------------------------


mkdir -p png
#test_python_script "../examples/embedding/test.py"
test_python_script "../examples/ic/generate_galaxy/addmetals/addmetals_for_gas.py data/snap.dat -o output"
# test_python_script "../examples/ic/addmetals/addmetals-gasonly.py"    # needs PyChem
# test_python_script "../examples/ic/addmetals/addmetals-gas+stars.py"  # needs PyChem
# test_python_script "../examples/ic/addmetals/addmetals.py"            # needs PyChem
# test_python_script "../examples/ic/addmetals/generate_galaxy.py"
test_python_script "../examples/ic/generate_galaxy/generate_cylindrical_model_exponential.py"
test_python_script "../examples/ic/generate_galaxy/generate_cylindrical_model_miyamoto.py"
#test_python_script "../examples/ic/generate_galaxy.py"
# test_python_script "../examples/ic/generate_spherical_model_g2c.py"   # plots stuff
# test_python_script "../examples/ic/generate_spherical_model_nfw.py"   # plots stuff
#test_python_script "../examples/ic/generate_spherical_model_plummer.py"
#test_python_script "../examples/memory/memory_info.py"
#mpitest "../examples/parallel_ic/mkall.py"

cp ../examples/snapshots/gadget*.dat .
test_python_script "../examples/scripts/example01.py"
test_python_script "../examples/scripts/example02.py"
test_python_script "../examples/scripts/example03.py"
test_python_script "../examples/scripts/example04.py"
test_python_script "../examples/scripts/example05.py"
# test_python_script "../examples/scripts/example06.py"   # plots stuff
# test_python_script "../examples/scripts/example07.py"   # plots stuff
test_python_script "../examples/scripts/example08.py"
test_python_script "../examples/scripts/example10.py"
# test_python_script "../examples/scripts/example11.py"   # plots stuff
# test_python_script "../examples/scripts/example12.py"   # plots stuff
# test_python_script "../examples/scripts/example13.py"   # plots stuff
test_python_script "../examples/scripts/slice.py"
test_python_script "../examples/scripts/slice-p1.py"
test_python_script "../examples/scripts/slice-p2.py"
#test_python_script "../examples/scripts/makesnapshot.py"
#test_python_script "../examples/scripts/mkmodel_for_display.py"
# test_python_script "../examples/SSP/plot_Lv-Z.py"     # needs Ptools
# test_python_script "../examples/SSP/plot_MatLv.py"    # needs Ptools
# test_python_script "../examples/SSP/plot_Lv-Age.py"   # needs Ptools
# test_python_script "../examples/units/setunits.py"    # hardcoded filename
# test_python_script "../examples/units/setunits_2.py"  # hardcoded filename
cp ../examples/units/params* .
cp ../examples/units/dsph.dat .
test_python_script "../examples/units/setunits_3.py"
test_python_script "../examples/units/setunits_4.py"
test_python_script "../examples/units/setunits_5.py"
# test_python_script "../examples/units/setunits_6.py"  # has hardcoded file on unige
test_python_script "../examples/units/setunits_7.py"
test_python_script "../examples/units/setunits_8.py"
test_python_script "../examples/units/setunits_9.py"
test_python_script "../examples/units/setunits_10.py"

test_python_script "../scripts/pNbody/pNbody_checkall"

# new unittests
cd  ./Core/
test_python_script "./testSelection.py"
cd ..

cd  ./Mockimgs/Filters/
test_python_script "./testFilters.py"
test_python_script "./test_SurfaceBrightnessScripts.py"
cd ../..

cd  ./Mockimgs/Instruments/
test_python_script "./testInstruments.py"
cd ../..

cd  ./mass_models/
test_python_script "./testProfiles.py"
cd ..

cd  ./Mapping/
test_python_script "./testMapping.py"
cd ..

cd  ./orbits/Integration
test_python_script "./TestOrbitIntegration.py"
cd ../..

cd  ./ICs
test_python_script "./testICs.py"
cd ..

cd  ./PhysicalQuantities
test_python_script "./testPhysicalQuantities.py"
cd ..

cleanup




python3 -m coverage combine
python3 -m coverage report
python3 -m coverage html
