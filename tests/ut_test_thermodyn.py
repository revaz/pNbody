#!/usr/bin/env python3
###########################################################################################
#  package:   pNbody
#  file:      test_thermodyn.py
#  brief:     unit tests for thermodyn.py
#  copyright: GPLv3
#             Copyright (C) 2019 EPFL (Ecole Polytechnique Federale de Lausanne)
#             LASTRO - Laboratory of Astrophysics of EPFL
#  author:    Mladen Ivkovic <mladen.ivkovic@hotmail.com>
#
# This file is part of pNbody.
###########################################################################################

import ut_helpers as h
import pNbody.thermodyn as t

T   = h.getPosArr()
rho = h.getPosArr()
P   = h.getPosArr()
A   = h.getPosArr()
U   = h.getPosArr()
Xi  = h.getPosArr(); Xi /= Xi.max()

dpars = t.defaultpars


h.announce('old_MeanWeightT')
h.hprint(t.old_MeanWeightT(T))
print()

h.announce('MeanWeightT')
h.hprint(t.MeanWeightT(T))
print()

h.announce('UNt(T)')
h.hprint(t.UNt(T))
print()

h.announce('Tun(UNt(T))')
h.hprint(t.Tun(t.UNt(T)))
print()

h.announce('Prt')
h.hprint(t.Prt(rho, T))
print()

h.announce('Trp')
h.hprint(t.Trp(rho, P))
print()

h.announce('Art')
h.hprint(t.Art(rho, T))
print()

h.announce('Tra')
h.hprint(t.Tra(rho, A))
print()

h.announce('Urt')
h.hprint(t.Urt(T))
print()

h.announce('Tru')
h.hprint(t.Tru(U))
print()

h.announce('Tmuru')
h.hprint(t.Tmuru(U))
print()

h.announce('Pra')
h.hprint(t.Pra(rho, A))
print()

h.announce('Arp')
h.hprint(t.Arp(rho, P))
print()

h.announce('Pru')
h.hprint(t.Pru(rho, U))
print()

h.announce('Urp')
h.hprint(t.Urp(rho, P))
print()

h.announce('Ura')
h.hprint(t.Ura(rho, A))
print()

h.announce('Aru')
h.hprint(t.Aru(rho, U))
print()

h.announce('SoundSpeed_rt')
h.hprint(t.SoundSpeed_rt(T))
print()

h.announce('SoundSpeed_ru')
h.hprint(t.SoundSpeed_ru(U))
print()

h.announce('JeansLength_rt')
h.hprint(t.JeansLength_rt(rho, T))
print()

h.announce('JeansLength_ru')
h.hprint(t.JeansLength_ru(rho, U))
print()

h.announce('JeansMass_rt')
h.hprint(t.JeansMass_rt(rho, T))
print()

h.announce('JeansMass_ru')
h.hprint(t.JeansMass_ru(rho, U))
print()

h.announce('MeanWeight')
for ionized in [True, False]:
    h.hprint(t.MeanWeight(Xi, ionized))
    print()


#! TODO:
#  t.ElectronDensity() # Need pars
#  t.Lambda() # need localsystem, coolingfile
#  t.CoolingTime() # need localsystem, coolingfile
