#!/usr/bin/env python3

import unittest
from pNbody import Nbody

class TestPhysicalQuantities(unittest.TestCase):
      
    def testSWIFTcos(self):

      nb = Nbody("../../examples/snapshots/cosmo_zoom.hdf5")
       
      self.assertEqual(nb.doHubbleFactorCorrection(),       False)  
      self.assertEqual(nb.HubbleConversionFactor(),         1) 
      self.assertEqual(nb.doComovingToProperConversion(),   True) 
      
      # test time variables
      self.assertEqual(nb.atime,         0.19607843137254904) 
      self.assertEqual(nb.time,          1.5282821852672024) 
      self.assertEqual(nb.redshift,      4.1) 
      self.assertEqual(nb.scalefactor,   0.19607843137254904) 
      
      
      #self.assertEqual(nb.ScaleFactor(),              0.19607843137254904) 
      #self.assertEqual(nb.Redshift(),                 4.1) 
      #self.CosmicTime()  # currently return values for all particles
      #self.CosmicTime()      
      #nb.Time()

    def testSWIFTiso(self):

      nb = Nbody("../../examples/snapshots/MW_galaxy.hdf5")
       
      self.assertEqual(nb.doHubbleFactorCorrection(),       False)  
      self.assertEqual(nb.HubbleConversionFactor(),         1) 
      self.assertEqual(nb.doComovingToProperConversion(),   False) 
      
      # test time variables
      self.assertEqual(nb.atime,         0.0) 
      self.assertEqual(nb.time,          0.0) 
      self.assertEqual(nb.redshift,      0.0) 
      self.assertEqual(nb.scalefactor,   0.0) 
      

if __name__ == '__main__':
    unittest.main()
