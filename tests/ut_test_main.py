#!/usr/bin/env python3
###########################################################################################
#  package:   pNbody
#  file:      test_main.py
#  brief:     unit tests for main.py
#  copyright: GPLv3
#             Copyright (C) 2019 EPFL (Ecole Polytechnique Federale de Lausanne)
#             LASTRO - Laboratory of Astrophysics of EPFL
#  author:    Mladen Ivkovic <mladen.ivkovic@hotmail.com>
#
# This file is part of pNbody.
###########################################################################################

import ut_helpers as h
import pNbody as pn
import numpy as np
import os

import warnings



plot = False    # whether to call Nbody.display()
delfiles = True # whether to explicitly delete written files

rt1 = '../examples/snapshots/gadget_z00.dat'
rt2 = '../examples/snapshots/gadget_z40.dat'
rt3 = 'data/snap.dat'
rt4 = 'data/snap.hdf5'
wt1 = 'writingtest.dat'
wt2 = 'writingtest_binary.dat'
wt3 = 'writingtest_merge.dat'
wt4 = 'image.gif'
wt5 = 'writingtest2.dat'
wtT = 'T11.num'
numfile = 'numfile.num'






# do this to avoid crashing the code for ImportWarnings and DeprecationWarnings from scipy
warnlist=[RuntimeWarning, SyntaxWarning, UserWarning, FutureWarning, 
    UnicodeWarning, BytesWarning, ResourceWarning]
for w in warnlist:
    warnings.simplefilter('error', category=w) # turn warnings into errors






#==================================================
# Part 1: Execute examples from documentation
#==================================================



#---------------------------------------------
# Creating pNbody objects from scratch
#---------------------------------------------
nb = pn.Nbody(rt1, ftype='gadget')
nb.info()
if plot:  nb.display(size=(10000,10000), shape=(512,512), palette='light')

for val in [nb.cm(), nb.Ltot()]:
    print(val)

nb.translate([3125,-4690,1720])
if plot:  nb.display(size=(10000,10000),shape=(512,512),palette='light')
if plot:  nb.display(size=(1000,1000),shape=(512,512),palette='light')

nb.rotate(angle = np.pi)

if plot:  nb.display(size=(1000,1000),shape=(512,512),palette='light')



#------------------------------
# Selection of particles
#------------------------------

nb_gas = nb.select('gas')
if plot:  nb_gas.display(size=(1000,1000),shape=(512,512),palette='rainbow4',mode='T',filter_name='gaussian')
if plot:  nb_gas.display(size=(5000,5000),shape=(512,512),palette='light',mode='m',scale="log",frsp=0.1)


nb_sub = nb.selectc((nb.rxyz()<500))
if plot:  nb_sub.display(size=(1000,1000),shape=(512,512),palette='light')

nb_sub.rename(wt1)
nb_sub.write()


print(np.log10(max(nb_gas.T())))
nb_sub = nb_gas.selectc( (nb_gas.T()>1e5) )
if plot:  nb_sub.display(size=(5000,5000),shape=(512,512),palette='light')
nb_sub.write_num(wtT)

nb40 = pn.Nbody(rt2, ftype='gadget')
if plot:  nb40.display(size=(10000,10000),shape=(512,512),palette='light')

nb_sub = nb40.selectp(file=wtT)
if plot:  nb_sub.display(size=(10000,10000),shape=(512,512),palette='light')




#-------------------------
# Merging two models
#-------------------------

nb1 = pn.Nbody(rt1)
nb2 = pn.Nbody(rt1)
nb1.rotate(angle=np.pi/4, axis=[0,1,0])
nb1.translate([-150, 0, 0])
nb1.vel = nb1.vel + [50, 0, 0]
nb2.rotate(angle=np.pi/4, axis=[1,0,0])
nb2.translate([+150, 0, 50])
nb2.vel = nb2.vel - [50,0,0]
nb3 = nb1 + nb2
nb3.rename(wt3)
nb3.write()
nb3.display(size=(300,300),shape=(512,512),palette='lut2',xp=[-100,0,0],save=wt4)
if plot:  nb3.display()





#==================================================
# Part 2: Call everything possible that you can
#==================================================


formats = [None, 'gadget', 'gh5', 'swift', 'swift_logger']
# output from Nbody.get_list_of_vars()
varlist = ['ChimieElements', 'ChimieNelements', 'ChimieSolarMassAbundances',
    'Density', 'DesNumNgb', 'G', 'Hsml', 'MaxNumNgbDeviation', 'Tree', 
    'UnitLength_in_cm', 'UnitMass_in_g', 'UnitVelocity_in_cm_per_s', 
    'Unit_time_in_cgs', '_header', 'art_bulk_visc_const', 
    'art_bulk_visc_const_l', 'art_bulk_visc_const_max', 
    'art_bulk_visc_const_min', 'atime', 'boxsize', 'byteorder', 
    'chimie_kinetic_feedback_fraction', 'chimie_max_size_timestep', 
    'chimie_parameter_file', 'chimie_snia_thermal_time', 
    'chimie_snii_thermal_time', 'chimie_supernova_energy', 
    'chimie_wind_speed', 'chimie_wind_time', 'comovingintegration', 
    'cooling_type', 'courant_fac', 'critical_energy_spec', 
    'cutoff_cooling_temperature', 'defaultparameters', 'des_num_ngb', 
    'empty', 'err_tol_force_acc', 'err_tol_int_accuracy', 'err_tol_theta', 
    'flag_age', 'flag_chimie_extraheader', 'flag_cooling', 'flag_entr_ic', 
    'flag_feedback', 'flag_metals', 'flag_sfr', 'flag_thermaltime', 'ftype', 'grackle_hs_shielding_density_threshold', 'grackle_redshift', 
    'grackle_uv_background', 'hubbleparam', 'hubbleparam_niu', 
    'init_gas_metallicity', 'init_gas_temp', 'jeans_mass_factor', 
    'localsystem_of_units', 'mass_tot', 'massarr', 'max_num_ngb_deviation', 'max_rms_displacement_fac', 'max_size_timestep', 
    'min_gas_hsml_fractional', 'min_gas_temp', 'min_size_timestep', 'nall', 
    'nallhw', 'nbg_factor_timestep', 'nbody', 'nbody_tot', 'npart', 'npart_tot', 
    'num_files', 'nzero', 'omega0', 'omegabaryon', 'omegalambda', 'p_name', 
    'p_name_global', 'parameters', 'periodicboundaries', 'pio', 'randomseed', 
    'redshift', 'skipped_io_blocks', 'softening_bndry', 'softening_bndry_max_phys', 
    'softening_bulge', 'softening_bulge_max_phys', 'softening_disk', 
    'softening_disk_max_phys', 'softening_gas', 'softening_gas_max_phys', 
    'softening_halo', 'softening_halo_max_phys', 'softening_stars', 
    'softening_stars_max_phys', 'spec_vars', 'spec_vect', 'star_formation_cstar', 'star_formation_density', 'star_formation_jeans_mass_factor', 
    'star_formation_mg_ms_fraction', 'star_formation_n_stars_from_gas', 
    'star_formation_softening', 'star_formation_softening_max_phys', 
    'star_formation_star_mass', 'star_formation_temperature', 
    'star_formation_time', 'star_formation_type', 'status', 
    'tree_domain_update_frequency', 'type_opening_criterion', 
    'type_timestep_criterion', 'unitsfile', 'unitsparameters', 'verbose']
# output from Nbody.get_list_of_arrays()
arraylist = ['idp', 'mass', 'metals', 'minit', 'num', 'opt1', 'opt2', 
    'pos', 'rho', 'rsp', 'tpe', 'tstar', 'u', 'vel']


#  for infile in [rt1]:
#  for infile in [rt1, rt2]:
#  for infile in [rt1, rt2, rt3]:
for infile in [rt1, rt2, rt3, rt4]:
#  for infile in [rt4]:

    nb = pn.Nbody(infile)

    h.announce('Nbody.check_arrays()')
    nb.check_arrays()

    h.announce('Nbody.check_ftype()')
    nb.check_ftype()

    # Not checked yet
    #  h.announce('Nbody.extend_format()')
    #  nb.extend_format()

    h.announce('Nbody.find_format()')
    for ft in formats:
        nb.find_format(ft)

    h.announce('Nbody.find_vars()')
    print(nb.find_vars())

    h.announce('Nbody.get_default_spec_array()')
    print(nb.get_default_spec_array())

    h.announce('Nbody.get_default_spec_vars()')
    print(nb.get_default_spec_vars())

    h.announce('Nbody.get_excluded_extension()')
    print(nb.get_excluded_extension())

    # TODO: Bug in here
    #  h.announce('Nbody.get_format_file()')
    #  print(nb.get_format_file())

    h.announce('Nbody.get_ftype()')
    print(nb.get_ftype())

    h.announce('Nbody.find_format()')
    for f in formats:
        print(nb.find_format(f))

    h.announce('Nbody.get_list_of_arrays()')
    print(nb.get_list_of_arrays())

    h.announce('Nbody.get_list_of_methods()')
    print(nb.get_list_of_methods())

    h.announce('Nbody.get_list_of_vars()')
    print(nb.get_list_of_vars())

    h.announce('Nbody.get_mxntpe()')
    print(nb.get_mxntpe())

    h.announce('Nbody.get_nbody()')
    print(nb.get_nbody())

    h.announce('Nbody.get_nbody_tot()')
    print(nb.get_nbody_tot())

    h.announce('Nbody.get_npart()')
    print(nb.get_npart())

    h.announce('Nbody.get_npart_all()')
    for ntasks in range(1,100):
        h.hprint(nb.get_npart_all(nb.get_nbody(), ntasks)[0])
    print()

    # TODO: Possible bug in here
    #  h.announce('Nbody.get_npart_and_npart_all()')
    #  for ntasks in range(1,100):
    #      h.hprint(nb.get_npart_and_npart_all(nb.get_npart_all(nb.get_nbody(), ntasks)))
    #  print()
 
    h.announce('Nbody.get_npart_tot()')
    print(nb.get_npart_tot())

    h.announce('Nbody.get_npart()')
    print(nb.get_npart())

    h.announce('Nbody.get_ntype()')
    print(nb.get_ntype())

    h.announce('Nbody.get_read_fcts()')
    print(nb.get_read_fcts())

    h.announce('Nbody.get_write_fcts()')
    print(nb.get_write_fcts())

    h.announce('Nbody.has_array()')
    for arr in arraylist:
        h.hprint(nb.has_array(arr))
    print()

    h.announce('Nbody.has_var()')
    for var in varlist:
        h.hprint(nb.has_var(var))
    print()

    h.announce('Nbody.info()')
    nb.info()

    h.announce('Nbody.init()')
    nb.init()

    h.announce('Nbody.init_units()')
    nb.init_units()

    h.announce('Nbody.make_default_vars_global()')
    nb.make_default_vars_global()

    h.announce('Nbody.memory_info()')
    nb.memory_info()

    h.announce('Nbody.nodes_info()')
    nb.nodes_info()

    h.announce('Nbody.object_info()')
    nb.object_info()

    #  Is called by Nbody.read(), no need to test it twice.
    #  h.announce('Nbody.open_and_read()')
    #  nb.open_and_read()

    #  Is called by Nbody.write(), no need to test it twice.
    #  h.announce('Nbody.open_and_write()')
    #  nb.open_and_write()

    h.announce('Nbody.print_filenames()')
    nb.print_filenames()

    h.announce('Nbody.read()')
    nb.read()

    h.announce('Nbody.write_num()')
    nb.write_num(numfile)

    h.announce('Nbody.read_num()')
    nb.read_num(numfile)

    h.announce('Nbody.rename()')
    nb.rename()

    h.announce('Nbody.set_filenames()')
    nb.set_filenames(wt5)

    #  # TODO: bug here for rt4
    #  h.announce('Nbody.set_ftype()')
    #  # there is no format for 'None'
    #  for f in formats[1:]:
    #      nb.set_ftype(f)

    h.announce('Nbody.set_local_system_of_units()')
    for i in h.repeat:
        nb.set_local_system_of_units(
            UnitLength_in_cm=h.bigrand(),
            UnitMass_in_g=h.bigrand(),
            UnitVelocity_in_cm_per_s=h.bigrand()
        )
    # TODO: test params='default', unitparameterfile=None, gadgetparameterfile=None

    h.announce('Nbody.set_npart()')
    nb.set_npart(nb.get_npart())

    # TODO later
    #  h.announce('Nbody.set_parameters()')
    #  nb.set_parameters()

    #  h.announce('Nbody.set_pio()')
    #  nb.set_pio()

    h.announce('Nbody.set_tpe()')
    for i in range(nb.get_mxntpe()):
        nb.set_tpe(i)

    #  h.announce('Nbody.set_unitsparameters()')
    #  nb.set_unitsparameters()

    h.announce('Nbody.spec_info()')
    nb.spec_info()

    # TODO: bug here for rt4
    #  h.announce('Nbody.write()')
    #  nb.write()

    # already done before so you can read a num file in.
    #  h.announce('Nbody.write_num()')
    #  nb.write_num()



#===================================================
# Cleanup
#===================================================


if delfiles:
    for f in [wt1, wt2, wt3, wt4, wt5, wtT, numfile]:
        try:
            os.remove(f)
        except FileNotFoundError:
            pass
