#!/usr/bin/env python3

import os, shutil
import unittest

#AJOUTER UN FICHIER AVEC UNE UFDS SEUELEMTN
class TestIntegration(unittest.TestCase):

        def setUp(self):
            # Write dwarf coordinates to files
            #File with multiple dwarfs (ICRS)
            with open('coord_icrs.txt', 'w') as f:
                f.write("""#Name    Distance modulus           RA         DEC       PM_RA    PM_DEC     v_rad
Bootes1           19.1100    210.0225     14.50060       -0.39     -1.06    101.80
Hercules          20.6800    247.7722     12.78520       -0.04     -0.34     45.00
Tucana2           18.8000    342.9796    -58.56890        0.91     -1.27    129.10
""")

            #File with only one dwarf (ICRS)
            with open('coord_icrs_one.txt', 'w') as f:
                f.write("""#Name    Distance modulus           RA         DEC       PM_RA    PM_DEC     v_rad
Bootes3           18.3500    209.3000     26.80000       -1.16     -0.88    197.50""")

            #File with multiple dwarfs (carthesian)
            with open('coord_carth.txt', 'w') as f:
                        f.write("""#Name    x           y         z       v_x    v_y     v_z
Test1           10   12     15       1     5    1
Test2          25   78     21      10     4    5
""")

            #File with only one dwarf (carthesian)
            with open('coord_carth_one.txt', 'w') as f:
                f.write("""#Name    x           y         z       v_x    v_y     v_z
BTest1           10   12     15       1     5    1""")

        def tearDown(self):
            #Remove the files and folders created during the tests
            os.remove("coord_icrs.txt")
            os.remove("coord_icrs_one.txt")
            os.remove("coord_carth.txt")
            os.remove("coord_carth_one.txt")
            
            if os.path.isdir("output"):
                shutil.rmtree("output")
    
        def test_integration_icrs_coord_help(self):
            #Test that calling for help works
            out = os.system("python3 ../../../scripts/orbits/orbits_integration_MW --help")
            self.assertEqual(out, 0)
        
        def test_integration_icrs_coord_basic(self):
            #Basic case (just integrate orbits with ICRS coordinates and the default options)
            out = os.system("python3 ../../../scripts/orbits/orbits_integration_MW -i coord_icrs.txt -o output/")
            self.assertEqual(out, 0)
            
            out = os.system("python3 ../../../scripts/orbits/orbits_integration_MW -i coord_icrs_one.txt -o output/")
            self.assertEqual(out, 0)
    
        def test_integration_icrs_coord_t_forward(self):
            #Just change the forward integration time
            out = os.system("python3 ../../../scripts/orbits/orbits_integration_MW -i coord_icrs.txt -o output/ --t_forward 1")
            self.assertEqual(out, 0)
            
            out = os.system("python3 ../../../scripts/orbits/orbits_integration_MW -i coord_icrs_one.txt -o output/  --t_forward 1")
            self.assertEqual(out, 0)
            
        def test_integration_icrs_coord_t_backward(self):
            #Just change the the backward integration time
            out = os.system("python3 ../../../scripts/orbits/orbits_integration_MW -i  coord_icrs.txt -o output/ --t_backward 1")
            self.assertEqual(out, 0)
            
            out = os.system("python3 ../../../scripts/orbits/orbits_integration_MW  -i coord_icrs_one.txt -o output/  --t_backward 1")
            self.assertEqual(out, 0)
            
        def test_integration_icrs_coord_timestep(self):
            #Just change the timestep
            out = os.system("python3 ../../../scripts/orbits/orbits_integration_MW  -i coord_icrs.txt -o output/ --delta_t 0.1")
            self.assertEqual(out, 0)
            
            out = os.system("python3 ../../../scripts/orbits/orbits_integration_MW  -i coord_icrs_one.txt -o output/ --delta_t 0.1")
            self.assertEqual(out, 0)
            
        def test_integration_icrs_coord_MWpotential2014(self):
            #Change the potential (use the default, MWPotential2014)
            out = os.system("python3 ../../../scripts/orbits/orbits_integration_MW -i coord_icrs.txt -o output/ --potential MWPotential2014")
            self.assertEqual(out, 0)
            
            out = os.system("python3 ../../../scripts/orbits/orbits_integration_MW -i coord_icrs_one.txt -o output/ --potential MWPotential2014")
            self.assertEqual(out, 0)
            
        def test_integration_icrs_coord_MWpotential2014_time_evolved(self):
            #Change the potential (use MWPotential2014_time_evolved)
            out = os.system("python3 ../../../scripts/orbits/orbits_integration_MW -i coord_icrs.txt -o output/ --potential MWPotential2014_time_evolved")
            self.assertEqual(out, 0)
            
            out = os.system("python3 ../../../scripts/orbits/orbits_integration_MW -i coord_icrs_one.txt -o output/ --potential MWPotential2014_time_evolved")
            self.assertEqual(out, 0)
                    
        def test_integration_icrs_coord_forward_launch_option_initial(self):
            #Change the forward_launch_option to "initial"
            out = os.system("python3 ../../../scripts/orbits/orbits_integration_MW -i coord_icrs.txt -o output/ --forward_launch_option initial")
            self.assertEqual(out, 0)
            
            out = os.system("python3 ../../../scripts/orbits/orbits_integration_MW -i coord_icrs_one.txt -o output/ --forward_launch_option initial")
            self.assertEqual(out, 0)
            
        def test_integration_icrs_coord_forward_launch_option_apocenter(self):
                #Change the forward_launch_option to "initial"
                out = os.system("python3 ../../../scripts/orbits/orbits_integration_MW -i coord_icrs.txt -o output/ --forward_launch_option apocenter")
                self.assertEqual(out, 0)
                
                out = os.system("python3 ../../../scripts/orbits/orbits_integration_MW -i coord_icrs_one.txt -o output/ --forward_launch_option apocenter")
                self.assertEqual(out, 0)
        
        def test_integration_icrs_coord_forward_launch_option_custom(self):
            #Change the forward_launch_option to "initial"
            out = os.system("python3 ../../../scripts/orbits/orbits_integration_MW -i coord_icrs.txt -o orbit.csv --forward_launch_option custom --position 100 100 100 --velocity 10 10 10" )
            self.assertEqual(out, 0)
            
            out = os.system("python3 ../../../scripts/orbits/orbits_integration_MW -i coord_icrs_one.txt -o orbit.csv --forward_launch_option custom  --position 100 100 100 --velocity 10 10 10")
            self.assertEqual(out, 0)
            
        def test_integration_carth_coord_basic(self):
            #Basic case (just integrate orbits with carhtesian coordinates and the default options)
            out = os.system("python3 ../../../scripts/orbits/orbits_integration_MW -i coord_carth.txt -o output/ --coordinate_system carthesian")
            self.assertEqual(out, 0)
            
            out = os.system("python3 ../../../scripts/orbits/orbits_integration_MW -i coord_carth_one.txt -o output/ --coordinate_system carthesian")
            self.assertEqual(out, 0)

if __name__ == '__main__':
    unittest.main()
