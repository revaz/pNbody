#!/usr/bin/env python3

import unittest
from pNbody.Mockimgs import ccd
from pNbody.Mockimgs import instrument
from pNbody.Mockimgs.instrument import Instrument 
from pNbody.Mockimgs import telescope
from pNbody.Mockimgs import filters
from pNbody.Mockimgs.obs_object import Object
from pNbody import *
from pNbody import ic
from astropy import units as u
from astropy.io import fits
from pNbody import mapping

def getPlummer(n):
  
  # generate an nbody object
  e = 0.1
  rmax = 10

  nb = ic.plummer(n,1,1,1,e,rmax,M=1,irand=1,vel='no',ftype="swift")  
  
  # set particles to be stars
  md = nb.getParticleMatchingDict()
  nb.set_tpe(md["stars"])
  
  nb.mass  = np.ones(n)

  
  # define units
  u_Length   = 1* u.kpc
  u_Mass     = 10**10 * u.M_sun
  u_Velocity = 1* u.km/u.s
  u_Time     = u_Length/u_Velocity 
  toMsol     = u_Mass.to(u.M_sun).value
  
  
  # add units
  nb.UnitLength_in_cm         = u_Length.to(u.cm).value
  nb.UnitMass_in_g            = u_Mass.to(u.g).value
  nb.UnitVelocity_in_cm_per_s = u_Velocity.to(u.cm/u.s).value
  nb.Unit_time_in_cgs         = u_Time.to(u.s).value  
  
  # non cosmological #galaxy = Object(filename=opt.file,unitsfile=opt.unitsfile,adjust_coords=adjust_coords,distance=opt.distance*u.Mpc)
  nb.setComovingIntegrationOff()
  nb.cosmorun=0# add the object to the instrument
  nb.time    =0#instrument.add_object(galaxy)  
  
  return nb
  



  

class TestFilters(unittest.TestCase):

    def setUp(self):      
      pass
      

    def test_mapping_mkcic3dn(self):
      """
      test mapping_mkcic3dn
      """
      nb = getPlummer(1000)   
      nb.translate([10,10,10])

      # 1D : compute a cic matrix centered on 10 of size 5
      mat = mapping.mkcic1dn(nb.pos.astype(np.float32),nb.mass.astype(np.float32),(10,),(5,),(256,))
      print(np.sum(mat.ravel()))      
      
      #from matplotlib import pylab as plt
      #plt.plot(np.arange(mat.size),mat)
      #plt.show()
      
      # 2D : compute a cic matrix centered on 10,10 of size 5,5
      mat = mapping.mkcic2dn(nb.pos.astype(np.float32),nb.mass.astype(np.float32),(10,10),(5,5),(256,256))
      print(np.sum(mat.ravel()))
      
      #from matplotlib import pylab as plt
      #plt.imshow(mat)
      #plt.show()
      
      # 3D : compute a cic matrix centered on 10,10,10 of size 5,5,5
      mat = mapping.mkcic3dn(nb.pos.astype(np.float32),nb.mass.astype(np.float32),(10,10,10),(5,5,5),(256,256,256))
      print(np.sum(mat.ravel()))
      

if __name__ == '__main__':
    unittest.main()
