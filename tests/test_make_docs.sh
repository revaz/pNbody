#!/bin/bash
###########################################################################################
#  package:   pNbody
#  file:      test_make_docs.sh
#  brief:     Test to build sphinx documentation
#  copyright: GPLv3
#             Copyright (C) 2019 EPFL (Ecole Polytechnique Federale de Lausanne)
#             LASTRO - Laboratory of Astrophysics of EPFL
#  author:    Mladen Ivkovic <mladen.ivkovic@hotmail.com>
#
# This file is part of pNbody.
###########################################################################################

echo 'Running test to build documentation properly.'

wd=$PWD

cd ../Doc
make clean
isOk=$?
if [[ $isOk -ne 0 ]]; then
    echo 'make docs failed at make clean ?!'
    cd $wd
    exit 1
fi

make html
isOk=$?
if [[ $isOk -ne 0 ]]; then
    echo 'make docs failed at make html'
    cd $wd
    exit 1
fi

cd $wd
echo "finished test make docs successfully."
exit 0
