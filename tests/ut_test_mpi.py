#!/usr/bin/env python3
###########################################################################################
#  package:   pNbody
#  file:      test_mpi.py
#  brief:     unit tests for mpiwrapper.py
#  copyright: GPLv3
#             Copyright (C) 2019 EPFL (Ecole Polytechnique Federale de Lausanne)
#             LASTRO - Laboratory of Astrophysics of EPFL
#  author:    Mladen Ivkovic <mladen.ivkovic@hotmail.com>
#
# This file is part of pNbody.
###########################################################################################

import ut_helpers as h
import pNbody as pn
import pNbody.mpiwrapper as mpi
from mpi4py import MPI
import numpy as np
import os

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
ntasks = comm.Get_size()
taskloop = range(ntasks)

fname = 'mpi_testfile'  # filename to write stuff in
nb = pn.Nbody()
mxntpe = nb.get_mxntpe()


#-------------------------
def tprint(*obj):
#-------------------------
    """
    Autmatically print task rank before printing message
    """
    print("Task", rank, *obj)
    return


#-------------------------
def announce(*obj):
#-------------------------
    """
    Announce what you're testing, but let only rank 0 print
    """

    if rank==0:
        print("============================================================\n\n")
        print("   Testing ", *obj)
        print()
    return

#--------------------------------------------------
def getcleanarr(size=10, Rank=0, dtype='float64'):
#--------------------------------------------------
    """
    Create a simple array to work with.
    """
    return np.ones(size, dtype=dtype)*Rank





#-----------------------------------
# Function calls
#-----------------------------------

l = 5*ntasks                # arbitrary vector/array length. make it a multiple of ntasks
arr = getcleanarr(l,rank)

announce('mpi_AllGatherAndConcatArray')
#  print(mpi.mpi_AllgatherAndConcatArray(arr))
mpi.mpi_AllgatherAndConcatArray(arr)


# TODO
#  mpi.mpi_ExchangeFromTable()

announce('mpi_GatherAndWriteArray')
f = open(fname, 'wb')
mpi.mpi_GatherAndWriteArray(f, arr)
f.close()

announce('mpi_ReadAndSendArray')
f = open(fname, 'rb')
shape = np.array(arr.shape)
shape[0] = shape[0]*ntasks # array that has been written has been gathered first!
res = mpi.mpi_ReadAndSendArray(f, data_type=arr.dtype, shape=shape)
#  tprint(res)
f.close()



# TODO
#  mpi.mpi_GetExchangeTable(n_i)
#
#
# TODO: doesn't work this way?
#  announce('mpi_OldGatherAndWriteArray')
#  arr = getcleanarr(l, rank, np.int32)
#  f = open(fname, 'wb')
#  nlocal = np.ones([arr.shape[0], mxntpe], dtype=np.int32)*rank
#  comm.barrier()
#  mpi.mpi_OldGatherAndWriteArray(f, arr, nlocal=nlocal)
#  f.close()
#
#  announce('mpi_OldReadAndSendArray')
#  f = open(fname, 'rb')
#  nlocal = np.ones([arr.shape[0]*ntasks, mxntpe], dtype=np.int32)*rank
#  res = mpi.mpi_OldReadAndSendArray(f, data_type=arr.dtype, nlocal=nlocal)
#  tprint(res)
#  f.close()
#
#      nlocal : array NTask x Npart
#          array NTask
#
#
# TODO
#  announce('mpi_ReadAndSendBlock')
#  mpi.mpi_ReadAndSendBlock(f, data_type, shape=None, byteorder='little', split=None, htype=<class 'numpy.int32'>)





announce('mpi_allgather')
#  print(mpi.mpi_allgather(l+rank))
mpi.mpi_allgather(l)

announce('mpi_allreduce')
#  print(mpi.mpi_allreduce(l, op=MPI.SUM))
mpi.mpi_allreduce(l, op=MPI.SUM)

announce('mpi_arange')
#  print(mpi.mpi_arange(5*ntasks))
mpi.mpi_arange(5*ntasks)

announce('mpi_argmax')
#  print(mpi.mpi_argmax(arr))
mpi.mpi_argmax(arr)

announce('mpi_argmin')
#  print(mpi.mpi_argmin(arr))
mpi.mpi_argmin(arr)

announce('mpi_bcast')
for i in taskloop:
    arr = getcleanarr(l,rank)
    arr = mpi.mpi_bcast(arr, root=i)
    #  print(arr)

# TODO
#  mpi.mpi_find_a_toTask(begTask, fromTask, ex_table, delta_n)
#

announce('mpi_gather')
for i in taskloop:
    res = mpi.mpi_gather(l+rank, root=i)
    #  tprint("mpi_gather iter", i, res)


announce('mpi_getval')
arr = getcleanarr(l,rank)
for i in taskloop:
    res = mpi.mpi_getval(arr, (i,0))
    tprint("mpi_getval iter", i, res, arr)

announce('mpi_histogram')
arr2 = np.array(range(ntasks*2+1))
bins = np.array(range(0, ntasks*2+1, 3))
hist = mpi.mpi_histogram(arr2, bins)
#  tprint('arr:', arr2, "bins:", bins, "hist:", hist)

msg = 'mpi_iprint hello from rank '+str(rank)
announce('mpi_iprint')
mpi.mpi_iprint(msg)


arr = getcleanarr(l,rank)

announce('mpi_len')
#  tprint('mpi_len:', mpi.mpi_len(arr), 'local len:', arr.shape[0])
mpi.mpi_len(arr)

announce('mpi_max')
#  tprint('mpi max', mpi.mpi_max(arr))
mpi.mpi_max(arr)

announce('mpi_mean')
#  tprint('mpi mean', mpi.mpi_mean(arr))
mpi.mpi_mean(arr)

announce('mpi_min')
#  tprint('mpi min', mpi.mpi_min(arr))
mpi.mpi_min(arr)

announce('mpi_pprint')
msg='mpi_pprint from rank'+str(rank)
mpi.mpi_pprint(msg)


announce('mpi_send and mpi_recv')
if rank==ntasks - 1:
    dest = 0
else:
    dest = rank + 1
if rank==0:
    src = ntasks - 1
else:
    src = rank - 1
msg = 'data from rank '+str(rank)
sent = False
for i in taskloop:
    if rank == i:
        res = mpi.mpi_recv(src)
    else:
        if not sent:
            mpi.mpi_send(msg, dest=dest)
            sent = True
#  tprint("got", res)


announce('mpi_reduce')
for i in taskloop:
    arr = getcleanarr(l,rank)
    res = mpi.mpi_reduce(arr, root=i, op=MPI.SUM)
    #  tprint(res)

announce('mpi_rprint')
msg='hello from rank '+str(rank)
mpi.mpi_rprint(msg)

announce('mpi_sarange')
nb = pn.Nbody()
npart_all = np.empty((ntasks, mxntpe))
for i in taskloop:
    for j in range(mxntpe):
        npart_all[i, j] = 10*(i+1) + j
#  tprint(mpi.mpi_sarange(npart_all))
mpi.mpi_sarange(npart_all)


announce('mpi_sum')
#  tprint(mpi.mpi_sum(arr))
mpi.mpi_sum(arr)




#----------------------------------
# Cleanup
#----------------------------------

if rank==0:
    os.remove(fname)
