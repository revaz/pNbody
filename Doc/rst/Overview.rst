Overview
**********************

**pNbody** is a parallelized python module toolbox designed to manipulate and display interactively 
very large N-body systems.

Its oriented object approach allows to perform complicated manipulation with only very few commands.

As python is an interpreted language, the user can load an N-body system and explore it interactively 
using the python interpreter. However, **pNbody** can also be used in complex python scripts.

In particular **pNbody** is designed to:

  * extract many relevant physical quantities from n-body systems

  * ease the extraction of particles from an n-body system or merging different systems together

  * create realistic surface brightness images of galaxies  
    
  * create and display maps of physical values of the system, like density, temperature, 
    velocities, metallicity. Stereo rendering is also implemented.
    
  * generate initial conditions of idealized self-gravitating collision-less models through the Jeans equations 
    or by properly sampling distribution functions. 


**pNbody** is not limited by the file format. Users can redefine in a parameter file how to read its favorite format.

Its parallel (MPI/mpi4py) extension make it work on computer clusters without being limited by memory consumption. 
It has already been tested with hundreds of millions of particles.


.. image:: ../images/composition.png
