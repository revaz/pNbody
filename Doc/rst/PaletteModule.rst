the palette module
**********************

.. currentmodule:: pNbody.palette

.. automodule:: pNbody.palette
   :members:
