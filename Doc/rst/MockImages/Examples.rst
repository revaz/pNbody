
.. _mockimgs_examples-label:


Examples
========

A composit instrument
---------------------

This example show how to display the field of view of
two detectors.

::

  from pNbody import ic
  from pNbody.Mockimgs import *
  from astropy import units as u
  from matplotlib import pyplot as plt
  
  # define a CCD
  myCCD        = ccd.CCD(name="IFUWST",shape=[1024,1024],size=[1*u.cm,1*u.cm])
  
  # define a MOS (here we define only the field of view assumed to be a circle)
  myMOS        = mos.MOS(name="MOSWST",fov=1.25*u.degree)
  
  # define a telescope
  myTelescope  = telescope.Telescope(name="WST",focal=1145.91529937*u.cm)
  
  # define an object, here, a galaxy
  nb = ic.plummer(1000,1,1,1,0.1,rmax=10,M=1,irand=1,vel='no',name=None,ftype='swift')
  myGalaxy = obs_object.Object(nb=nb,distance=30*u.kpc)
  
  
  # define an instrument (use the CCD)
  WST_IFU =  instrument.Instrument(
    name        = "myInstrument",
    telescope   = myTelescope,
    ccd         = myCCD,
    obs_object  = myGalaxy,
    )
  
  # define an instrument (use the MOS)
  WST_MOS =  instrument.Instrument(
    name        = "myInstrument",
    telescope   = myTelescope,
    ccd         = myMOS,
    obs_object  = myGalaxy,
    )
  
  
  # open a matplotlib figure
  fig = plt.Figure()
  ax = plt.gca()
  
  # display the fields as well as the object (here in arcmin)  
  WST_IFU.draw(ax,mode="angle",unit=u.arcmin)
  WST_MOS.draw(ax,mode="angle",unit=u.arcmin)
  
  # set axis and show
  ax.axis('equal')
  plt.show()
  

Note that the display mode can be either 
``phys`` (physical size, usually in pc or kpc)
``angle`` (angle on the sky, arcsec, arcmin, degree)
``detector`` (size on the focal plane, mm, cm)::

  WST_IFU.draw(ax,mode="phys",unit=u.kpc)
  WST_IFU.draw(ax,mode="angle",unit=u.arcmin)
  WST_IFU.draw(ax,mode="detector",unit=u.mm)
  
  

