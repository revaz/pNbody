.. _mockimgs_instruments-label:

List of Instruments
===================


``Mockimgs`` contains pre-defined instruments.

.. program-output:: mockimgs_sb_compute_images --instruments_list

