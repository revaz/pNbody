
.. _mockimgs-label:


The ``Mockimgs`` module
=======================

.. currentmodule:: pNbody.Mockimgs


The ``Mockimgs`` modules provides tools to generate mock observation images from N-body models.


Designing an instrument
-----------------------

In its current form, an instrument is composed of a telescope, a ccd and a filter::

  from astropy import units as u
  from pNbody.Mockimgs import telescope, ccd, instruments, filters, obs_object

  # define a telescope
  mytelescope = telescope.Telescope(name="MyTelescope",focal=1500*u.mm)
  
  # define a ccd
  myccd = ccd.CCD(name="MyCCD",shape=[4096,4096],size=[41*u.mm,41*u.mm])
  
  # define a filter
  myfilter = filters.Filter("BastI_GAIA_G")
  
  # define an instrument
  myinstrument = instrument.Instrument(
  name        = "MyInstrument",
  telescope   = mytelescope,
  ccd         = myccd,
  filter_type = myfilter,
  )
  
  # get info
  myinstrument.info()
  

The last command will provide the following output::
  
  name               : MyInstrument
  fov                : [1.565987147680034 deg,1.565987147680034 deg]
  telescope name     : MyTelescope
  ccd name           : MyCCD
  ccd fov            : [1.565987147680034 deg,1.565987147680034 deg]
  ccd shape          : [4096,4096]
  ccd xmin xmax      : -0.013665815885465636 rad 0.013665815885465636 rad
  ccd ymin ymax      : -0.013665815885465636 rad 0.013665815885465636 rad
  ccd xmin xmax (kpc): 1.0 kpc 1.0 kpc
  ccd ymin ymax (kpc): 1.0 kpc 1.0 kpc
  filter name        : G
  filter module name : <module 'pNbody.Mockimgs.Mag_BastI' from '../python3.9/site-packages/pNbody/Mockimgs/Mag_BastI.py'>


Note that the focal is used here to convert physical distances into angles:

  .. math::
  
    \theta = \arctan( s / f)

where for example, :math:`\theta` is the field of view in radian, :math:`s` is the ccd size in some length units and
:math:`f` is the focal expressed in the same units.


Alternatively, pre-defined instruments (see the list below) can be directly loaded via ::

  myinstrument = instruments["arrakihs_vis_G"]
  myinstrument.info()

which gives::

   name               : arrakihs_vis_G
   fov                : [1.4997222222222222 deg,1.4997222222222222 deg]
   telescope name     : iSIM-170
   ccd name           : arrakihs_vis
   ccd fov            : [1.4997222222222222 deg,1.4997222222222222 deg]
   ccd shape          : [4096,4096]
   ccd xmin xmax      : -2699.5 arcsec 2699.5 arcsec
   ccd ymin ymax      : -2699.5 arcsec 2699.5 arcsec
   ccd xmin xmax (kpc): 1.0 kpc 1.0 kpc
   ccd ymin ymax (kpc): 1.0 kpc 1.0 kpc
   filter name        : G
   filter module name : <module 'pNbody.Mockimgs.Mag_BastI' from '/home/revaz/.local/lib/python3.9/site-packages/pNbody/Mockimgs/Mag_BastI.py'>
   


Attach and object
-----------------

To generate an image the instrument must be attached to an object (e.g. galaxy)::

  # create the object to observe ("snapshot.hdf5" is an N-body model) 
  galaxy = obs_object.Object(filename="snapshot.hdf5",distance=35*u.Mpc)
  
  # add the object to the instrument
  myinstrument.add_object(galaxy)  


Compute flux and surface brightness maps
----------------------------------------


If the N-body model ``snapshot.hdf5`` contains all necessary informations (see :ref:`mockimgs_preparation-label`),
flux and surface brightness maps can be computed::

  # set object for exposure
  myinstrument.set_object_for_exposure()
    
  # compute maps
  FluxMap = myinstrument.ComputeFluxMap()
  SBMap   = myinstrument.SurfaceBrightnessMap() 
  
  # save a fits image
  myinstrument.saveFitsImage(SBMap,"image.fits")
  


SSP Magnitudes in different filters
-----------------------------------

The ``Mockimgs`` modules provides stellar magnitudes for single stellar populations (SSP) in different filters.
Those are provided by the ``Mockimgs.filters``. 
Below is an example showing how to use it::

  from pNbody.Mockimgs import filters
  
  # get the list of all pNbody filters
  filters.List()
  
  # set a filter from the previous list
  Filter = filters.Filter("BastI_GAIA_G_RP")
  
  # or from a file
  Filter = filters.Filter("pNbody/config/opt/filters/BPASS230_EuclidNISPY.hdf")
  
  
  Mass = 1e11 # in Msol, the initial SSP mass
  Age  = 13   # in Gyr
  MH   = -2   # in log10([M/H])
  # compute the magnitude
  mag  = Filter.Magnitudes(Mass,Age,MH)

  print(mag)
  -21.824502416627993

A list of available filters is provided here: :ref:`mockimgs_filters-label`.


SSP luminosities
----------------

For a given metallicity or age, the corresponding luminosities in a given filter or the total bolometric luminosity
can be computed via the ``luminosities`` module. Note that even for the bolometric luminosity, a ``filter`` 
must be selected. This is because luminosities, as well as magnitudes are computed via population synthesis models 
stored in ``filter`` files. To compute luminosities, the procedure is the following::

  from pNbody.Mockimgs import luminosities
  
  # set a "filter"
  lum = luminosities.LuminosityModel("BPASS230_SDSS_r")
  
  # define SSP properties
  Mass = 1e11 # in Msol, the initial SSP mass
  Age  = 13   # in Gyr
  MH   = -2   # in log10([M/H])

  # compute the bolometric luminosity (in solar luminosity)
  L  = lum.Luminosities(Mass,Age,MH)

  print(L)
  39836268384.37495

  # compute the luminosity within the filter (in solar luminosity)
  L  = lum.Luminosities(Mass,Age,MH,bolometric=False)

  print(L)
  2302594538.516212
  
  
SSP mass ratio and Mass to Light ratio
--------------------------------------

The ``luminosities`` module also provides the possibility to retrieve 
the ratio between the SSP mass at a given age (after stellar mass loss) to the initial SSP mass::

  from pNbody.Mockimgs import luminosities
  
  # set a "filter"
  lum = luminosities.LuminosityModel("BPASS230_SDSS_r")
  
  # define SSP properties
  Age  = 13   # in Gyr
  MH   = -2   # in log10([M/H])

  # compute the SSP mass ratio
  MassRatio  = lum.SSPMassRatio(Age,MH)

  print(MassRatio)
  0.48918062943536766


The Mass to Light ratio is obtained using::

  # compute the SSP mass to light ratio
  ML  = lum.MassToLightRatio(Age,MH,bolometric=True)

  print(ML)
  1.2279780443171227
 
  # compute the SSP mass to light (through the filter) ratio
  ML  = lum.MassToLightRatio(Age,MH,bolometric=False)

  print(ML)
  21.244757652842992



.. note::

  In the case where the initial SSP mass is not available, it is possible to provide the current SSP mass
  and inform ``pNbody`` that it must apply a correction, using the ``current_mass`` option ::
  
    Mag  = Filter.Magnitudes(Mass,Age,MH,current_mass=True)  
    L    = LM.Luminosities(Mass,Age,MH,current_mass=True)
  
  The correction is done using a table stored in the hdf5 files which gives, for each age and metallicity the value
  of the current SSP mass compared to the initial value.
  

Computing a stellar mass from a magnitude
-----------------------------------------


The ``luminosities`` module also provides the possibility to extimate
the corresponding stellar mass at a given age (after stellar mass loss).

To illustrate this, lets start from a SSP of a given mass, age and metallicity::

  Mass = 1e6  # Msol
  Age  = 13   # in Gyr
  MH   = -2   # in log10([M/H])

Using the ``filters`` module, we can compute the SSP magnitude in a given filter::

  from pNbody.Mockimgs import filters
  Filter = filters.Filter("BPASS230_JKC_V")
  Mv  = Filter.Magnitudes(Mass,Age,MH)
  
  print(Mv)
  -8.922
  
To ease the understanding of the process, using the `luminosities`` module we can compute  
the mass of the SSP at the defined age::

  from pNbody.Mockimgs import luminosities
  lum = luminosities.LuminosityModel("BPASS230_JKC_V")

  print(lum.InitialMasses2EvolvedMasses(Mass,Age,MH))
  489180.62943536765
  
Now from the magnitude computed above, we can retrieve this same mass (at a few percent level)::

  print(lum.Magnitudes2Masses(Mv,Age,MH))
  472063.5411521999

To obtain the initial SSP mass, use::

  print(lum.Magnitudes2Masses(Mv,Age,MH,initial_mass_flag=True))
  965008.6547725224


Computing a luminosity from a magnitude
---------------------------------------

To compute the luminosity in a given filter knowing the magnitude, the following commands can be run::

  from pNbody.Mockimgs import luminosities
  lum = luminosities.LuminosityModel("BPASS230_JKC_V")
  
  # set a magnitude
  Mv  = -12
  
  # get the luminosity in solar luminosity in the corresponding filter:
  lum.Magnitudes2Luminosities(Mv)
  5267646.177915038



Pre-defined instruments
-----------------------

See :ref:`mockimgs_instruments-label`


List of available filters
-------------------------

See :ref:`mockimgs_filters-label`




``pNbody.Mockimgs`` classes
---------------------------

.. currentmodule:: pNbody.Mockimgs

The ``instrument`` class
........................

.. automodule:: pNbody.Mockimgs.instrument
   :members: 

The ``telescope`` class
.......................
   
.. automodule:: pNbody.Mockimgs.telescope
   :members: 

The ``ccd`` class
.................

.. automodule:: pNbody.Mockimgs.ccd
   :members: 

The ``filters`` module
......................


.. automodule:: pNbody.Mockimgs.filters
   :members: 

The ``luminosities`` module
...........................


.. automodule:: pNbody.Mockimgs.luminosities
   :members:   


The ``obs_object`` class
........................

.. automodule:: pNbody.Mockimgs.obs_object
   :members: 


