
.. _add_noise-label:

Adding noise on surface brightness maps
=======================================

.. currentmodule:: pNbody.Mockimgs

The ``Mockimg`` module offers the possibility to add noise on a surface brightness image using the command
``mockimgs_sb_add_noise``::

  mockimgs_sb_add_noise galaxy.fits.gz --SB_limit 31 --output-flux  -o galaxy_with_noise.fits

where the option ``--SB_limit`` sets the targeted surface brightness limit, by default, assuming a signal to noise of 3
and a standard deviation computed in 100 arcseconds square.
While by default the output will be a surface brightness map (magnitudes), it is highly recommended to add
the option ``--output-flux`` to store the output as a flux. This will prevent problems with pixels with 
a negative flux.
To display the output image, use ``mockimgs_sb_display_fits``, for example::

  mockimgs_sb_display_fits --add_axes --ax_unit kpc --colorbar --add_axes --colormap Greys --colormap_reverse galaxy_with_noise.fits.gz 




Scripts
-------


``mockimgs_sb_add_noise``
..............................

.. program-output:: mockimgs_sb_add_noise -h
