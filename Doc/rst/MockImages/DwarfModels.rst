.. _mockimgs_dwarfmodels-label:

Dwarf models
************

``mockimgs_generate_dwarf`` is a script that allows to generate spheroidal dwarf models.

In a fitrs step, from a given total quantity of stellar mass, in solar unit, formed (e.g. ``--M0 1e4``), 
the script generates a corresponding quantity of individual stars with some individual metallicityies and ages.

In a second step, the script uses an isochrone database to find the magnitude in some bands 
(e.g. ``--filters_keys VISmag Ymag Jmag``) and luminosities (``--logL 'logL'``)
of each individual stars. Only stars that did not exploded yet, according to the lifetimes stored in the
isochrone database, are considered.

If the option ``--filters_names MagVIS MagY MagJ`` is provided,
the final output will be an n-nbody file with magnitudes for each star stored as::

  nb.MagVIS
  nb.MagY
  nb.MagJ
  




``mockimgs_generate_dwarf``
...........................

.. program-output:: mockimgs_generate_dwarf -h



