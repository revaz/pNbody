
.. _mockimgs_filters-label:


List of filters
===============


Currently, the following filters are included:

.. program-output:: mockimgs_sb_compute_images --filters_list

