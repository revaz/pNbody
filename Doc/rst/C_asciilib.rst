the C asciilib module
**********************

.. currentmodule:: pNbody.asciilib

.. automodule:: pNbody.asciilib
   :members:
