the C peanolib module
**********************

.. currentmodule:: pNbody.peanolib

.. automodule:: pNbody.peanolib
   :members:
