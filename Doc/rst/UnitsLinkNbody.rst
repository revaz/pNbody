a

Link a unit system to a **pNbody** object
*****************************************

There is different way to link a system of units to a **pNbody** object.
In all cases, a system of unit is defined by providing three numbers, 
corresponding to the value of respectively centimeter (``UnitLength_in_cm``), 
gramm (``UnitMass_in_g``) and centimeter per second (``UnitVelocity_in_cm_per_s``)
in the desired system of unit. 

This may be done using the following possibilites.


Defining units explicitely
==========================

The first possibility to define units is to simply gives the explicit values
for the variables ``UnitLength_in_cm``, ``UnitMass_in_g``, ``UnitVelocity_in_cm_per_s``.
If  ``nb`` is your **pNbody** object, this is achieved with the following command::

  nb.set_local_system_of_units(UnitLength_in_cm=1,UnitVelocity_in_cm_per_s=1,UnitMass_in_g=1)

Another equivalent possibility is to use a dictionary::

  params = {}
  params['UnitLength_in_cm']	     = 1.
  params['UnitVelocity_in_cm_per_s'] = 1.
  params['UnitMass_in_g']	     = 1.
  nb.set_local_system_of_units(params)


Defining units using a units parameter file
===========================================

Here we use a units parameter file (here called ``./unitsparameters``), 
containing the following lines::

  UnitLength_in_cm
  UnitLength in cm
  Float
  3e+21

  UnitMass_in_g
  UnitMass in g
  Float
  3e+43

  UnitVelocity_in_cm_per_s
  UnitVelocity in cm per s
  Float
  20000000.

In this case, if  ``nb`` is your **pNbody** object, the following command
link a system of unit::

  nb.set_local_system_of_units(unitparameterfile='./unitsparameters')



Defining units using a gadget parameter file
============================================

`Gadget <http://www.mpa-garching.mpg.de/gadget/right.html>`_  parameter files contains
the definition for a system of units. In such files, we usually find
the following lines::

  UnitLength_in_cm				3.085678e+21
  UnitMass_in_g					1.989e+43
  UnitVelocity_in_cm_per_s 			100000.0


In this case, if  ``nb`` is your **pNbody** object, the following command
link a system of unit defined in the **Gadget** file ``params``::

  nb.set_local_system_of_units(gadgetparameterfile='./params')


Defining units when opening a file
==================================

**pNbody** offers also the possibility to define a unit system
when opening a file. In this case we directely pass either a units parameter::

  nb = Nbody("../snap.dat",ftype='gadget',unitsfile='unitsparameters')

or a gadget parameter file::

  nb = Nbody("../snap.dat",ftype='gadget',unitsfile='params')

to the class constructor.






