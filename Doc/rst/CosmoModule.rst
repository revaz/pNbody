the cosmo module
**********************

.. currentmodule:: pNbody.cosmo

.. automodule:: pNbody.cosmo
   :members:
