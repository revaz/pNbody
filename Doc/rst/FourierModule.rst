the fourier module
**********************

.. currentmodule:: pNbody.fourier

.. automodule:: pNbody.fourier
   :members:
