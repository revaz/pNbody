the profiles module
**********************

.. currentmodule:: pNbody.profiles

.. automodule:: pNbody.profiles
   :members:
