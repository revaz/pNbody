the DF module
=============

.. currentmodule:: pNbody.DF

.. automodule:: pNbody.DF
  :members: 

.. autoclass:: DistributionFunction
  :members: 
