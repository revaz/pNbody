Generating a multi-components galaxy
************************************

The script ``ic_makeGalaxy`` allows to create a multi-component galaxy model at equilibrium, combining 
a gas disk (Miyamoto-Nagai), a stellar disk (Exponential disk) a bulge (Plummer model) and a dark halo (generic 2-slope model).
The equilibrium is obtained by solving the Jean equation in both spherical and cylindrical coordinates.

By default the script create a Milky-Way like galaxy, assuming a set of parameters. The command::

  ic_makeGalaxy -o MW_galaxy.hdf5

will create the ``MW_galaxy.hdf5`` file containing all the particles.
The script can however take the parameters from a ``yaml`` file::

  ic_makeGalaxy -p params.yml

where the ``params.hdf5`` file contains for example the following parameters:

.. program-output:: ic_makeGalaxy --print-parameters


Plotting the corresponding rotation curve
.........................................

To check the rotation curve corresponding to the model generated, ``pNbody`` offers the following command::

  plotCylindricalProfile -y vcirc   --xmax 50 --rmax 50 --nr 64  --nr 64 --eps 0.1 --forceComovingIntegrationOff  MW_galaxy.hdf5


More examples
.............

Additional examples of dwarf galaxies or galaxies with cusp or core may be found in the 
`pNbody/examples/ic/ic_makeGalaxy <https://gitlab.com/revaz/pNbody/-/tree/master/pNbody/examples/ic/ic_makeGalaxy>`_
directory.

Scripts
-------


``ic_makeGalaxy``
................

.. program-output:: ic_makeGalaxy -h
