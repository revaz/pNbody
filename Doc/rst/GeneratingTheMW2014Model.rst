Generating a Milky Way galaxy based on the MW2014 Bovy model
************************************************************

The script ``ic_makeMW2014`` allows to create the Bovy MW model model at equilibrium, combining 
a stellar disk (Miyamoto disk) a bulge (trucated power law model) and a dark halo (NFW).
The equilibrium is obtained by solving the Jean equation in both spherical and cylindrical coordinates.

By default the script creates the exact MW2014 model. The command::

  ic_makeMW2014 -o MW_galaxy.hdf5

will create the ``MW_galaxy.hdf5`` file containing all the particles.
The script can however take the parameters from a ``yaml`` file::

  ic_makeGalaxy -p params.yml

where the ``params.hdf5`` file contains for example the following parameters:

.. program-output:: ic_makeMW2014 --print-parameters


Plotting the corresponding rotation curve
.........................................

To check the rotation curve corresponding to the model generated, ``pNbody`` offers the following command::

  plotCylindricalProfile -y vcirc   --xmax 50 --rmax 50 --nr 64  --nr 64 --eps 0.1 --forceComovingIntegrationOff  MW_galaxy.hdf5



Scripts
-------


``ic_makeMW2014``
.................

.. program-output:: ic_makeMW2014 -h
