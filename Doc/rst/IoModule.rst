the iofunc module
*****************

.. currentmodule:: pNbody.iofunc

.. automodule:: pNbody.iofunc
   :members:
   
..  .. autofunction:: 


.. .. autofunction:: checkfile
.. .. autofunction:: end_of_file
.. .. autofunction:: write_array
.. .. autofunction:: read_ascii
.. .. autofunction:: write_dmp
.. .. autofunction:: read_dmp



