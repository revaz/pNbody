the Mass models module
======================


.. automodule:: pNbody.mass_models.einasto
   :members: 

.. automodule:: pNbody.mass_models.exponential
   :members: 

.. automodule:: pNbody.mass_models.hernquist
   :members: 
   
.. automodule:: pNbody.mass_models.homosphere
   :members: 
   
.. automodule:: pNbody.mass_models.isochrone
   :members:      

.. automodule:: pNbody.mass_models.isothermal
   :members: 

.. automodule:: pNbody.mass_models.jaffe
   :members: 

.. automodule:: pNbody.mass_models.kuzmin
   :members: 
   
.. automodule:: pNbody.mass_models.logarithmic
   :members: 
   
.. automodule:: pNbody.mass_models.mestel
   :members:  
   
.. automodule:: pNbody.mass_models.miyamoto
   :members: 

.. automodule:: pNbody.mass_models.nfw
   :members: 

.. automodule:: pNbody.mass_models.plummer
   :members: 
   
.. automodule:: pNbody.mass_models.pm
   :members: 
   
.. automodule:: pNbody.mass_models.pseudoisothermal
   :members:      

.. automodule:: pNbody.mass_models.pseudomestel
   :members: 

.. automodule:: pNbody.mass_models.ring
   :members: 

.. automodule:: pNbody.mass_models.shell
   :members: 
   
.. automodule:: pNbody.mass_models.slabexp
   :members: 
   
.. automodule:: pNbody.mass_models.slab
   :members:     
