Converting units
****************

Example 1:
==========
::

  >> from pNbody import *
  >> 
  >> # define a new system of units : kpc,Msol,Myr
  >> system_of_units = units.UnitSystem('local',[units.Unit_kpc,  10**10*units.Unit_Ms, units.Unit_Myr])
  >> 
  >> # trivial conversion factor into kpc,10**10Msol,Myr 
  >> print system_of_units.convertionFactorTo(units.Unit_kpc)
  >> print system_of_units.convertionFactorTo(10**10*units.Unit_Ms)
  >> print system_of_units.convertionFactorTo(units.Unit_Myr)
  >> 
  >> # length
  >> # 
  >> print system_of_units.convertionFactorTo(units.Unit_cm)
  >> print system_of_units.convertionFactorTo(units.Unit_m)
  >> print system_of_units.convertionFactorTo(units.Unit_km)
  >> 
  >> # velocity
  >> print system_of_units.convertionFactorTo(units.Unit_km/units.Unit_s)
  >> print system_of_units.convertionFactorTo(units.Unit_kms)
  >> 
  >> 
  >> # energy
  >> print system_of_units.convertionFactorTo(units.Unit_kg*units.Unit_ms**2)
  >> print system_of_units.convertionFactorTo(units.Unit_J)
  >> 
  >> # density
  >> print system_of_units.convertionFactorTo(units.Unit_g/units.Unit_cm**3)


Example 2:
==========
::

  >> from pNbody import *
  >> 
  >> nb = Nbody("dsph.dat",ftype='gadget',unitsfile='params.dsph')
  >> 
  >> nb.localsystem_of_units.info() 
  >> 
  >> 
  >> # trivial conversion factor into kpc,10**10Msol,Myr 
  >> print "to km/s    : *",nb.localsystem_of_units.convertionFactorTo(units.Unit_kms)
  >> print "to g/cm**3 : *",nb.localsystem_of_units.convertionFactorTo(units.Unit_g/units.Unit_cm**3)
  >> print "to Msol    : *",nb.localsystem_of_units.convertionFactorTo(units.Unit_Msol)
  >> print "to Msol/yr : *",nb.localsystem_of_units.convertionFactorTo(units.Unit_Msol/units.Unit_yr)


Example 3:
==========
::

  >> from pNbody import *
  >> 
  >> ##############################
  >> # define some system of units
  >> ##############################
  >> 
  >> # output system of units (cgs)
  >> cgs_units = units.UnitSystem('cgs',[units.Unit_cm,units.Unit_g,units.Unit_s,units.Unit_K])
  >> 
  >> # gear system of units
  >> params = {}
  >> params['UnitVelocity_in_cm_per_s']      = 20725573.785998672
  >> params['UnitMass_in_g']                 = 1.989e+43
  >> params['UnitLength_in_cm']              = 3.085678e+21
  >> gear_units = units.Set_SystemUnits_From_Params(params)
  >> 
  >> 
  >> ##############################
  >> # compute constants
  >> ##############################
  >> 
  >> # compute mu
  >> Xi=0.76
  >> mu = 4./(8.-5.*(1.-Xi))
  >> 
  >> 
  >> # get k (boltzmann) in cgs_units
  >> k = ctes.BOLTZMANN.into(cgs_units)
  >> 
  >> # get mh (hydrogen mass) in cgs_units
  >> mh = ctes.PROTONMASS.into(cgs_units)
  >> 
  >> 
  >> ##############################
  >> # specific thermal energy
  >> ##############################
  >> T=1e4
  >> ui = 3/2.*k/(mu*mh) *T
  >> 
  >> 
  >> ##############################
  >> # mass
  >> ##############################
  >> 
  >> mi = 5e-08 # gear_units (500Msol)
  >> mi = mi*gear_units.convertionFactorTo(cgs_units.UnitMass)
  >> 
  >> 
  >> ##############################
  >> # energy
  >> ##############################
  >> 
  >> Nsn = 5
  >> Nngb= 10.
  >> 
  >> Esn  = Nsn * 1e51 * 0.03 	# already in cgs
  >> Esni = Esn/Nngb			# already in cgs
  >> 
  >> 
  >> 
  >> ##############################
  >> # now compute, all in cgs
  >> ##############################
  >> 
  >> ei = mi*ui
  >> eip= ei + Esni
  >> 
  >> print "ratio",eip/ei
  >> 
  >> 
  >> 
  >> ##############################
  >> # now compute, all in gear
  >> ##############################
  >> 
  >> ui  = ui*cgs_units.convertionFactorTo(gear_units.UnitSpecEnergy)
  >> mi  = mi*cgs_units.convertionFactorTo(gear_units.UnitMass)
  >> Esni= Esni*cgs_units.convertionFactorTo(gear_units.UnitEnergy)
  >> 
  >> print "------------"
  >> print ui
  >> print mi
  >> print Esni


Example 3:
==========
Convert gear density units to atom/cm 3::
  
  >> from pNbody import *
  >> ##############################
  >> # define some system of units
  >> ##############################
  >> 
  >> # cgs units (exits in pNbody)
  >> units.cgs
  >> 
  >> # output system of units (the mass units is the hydrogen mass)
  >> Unit_atom = ctes.PROTONMASS.into(units.cgs)*units.Unit_g
  >> Unit_atom.set_symbol('atom') 
  >> out_units = units.UnitSystem('local',[units.Unit_cm,Unit_atom,units.Unit_s,units.Unit_K])
  >> 
  >> 
  >> # gear system of units
  >> params = {}
  >> params['UnitVelocity_in_cm_per_s']      = 20725573.785998672
  >> params['UnitMass_in_g']                 = 1.989e+43
  >> params['UnitLength_in_cm']              = 3.085678e+21
  >> gear_units = units.Set_SystemUnits_From_Params(params)
  >> 
  >> 
  >> ##############################
  >> # now, use to convert
  >> ##############################
  >> 
  >> print gear_units.convertionFactorTo(out_units.UnitDensity)		# gear to atom/cm^3
  >> print gear_units.convertionFactorTo(units.cgs.UnitDensity)		# gear to g/cm^3
  >> print 0.1*out_units.convertionFactorTo(units.cgs.UnitDensity)		# atom/cm^3 to g/cm^3
  >> print 1.67e-25*units.cgs.convertionFactorTo(out_units.UnitDensity)



Example 4:
==========
::


  >> from pNbody import units,io,ctes
  >> from numpy import *
  >> 
  >> gadgetparameterfile="params.dSphCZ"
  >> params = io.read_params(gadgetparameterfile) 
  >> 
  >> #system_of_units = units.Set_SystemUnits_From_Params(params)
  >> system_of_units = units.Set_SystemUnits_From_File(gadgetparameterfile)
  >> 
  >> G=ctes.GRAVITY.into(system_of_units)
  >> H = ctes.HUBBLE.into(system_of_units)
  >> HubbleParam = params['HubbleParam']
  >> 
  >> rhoc = pow(HubbleParam*H,2)*3/(8*pi*G)
  >> print rhoc,rhoc*80,"free of h"
  >> 
  >> rhoc = pow(H,2)*3/(8*pi*G)
  >> print rhoc,rhoc*80,"in code units, including h"
  >> 
  >> H=0.1
  >> G=43000.1
  >> 
  >> print pow(H,2)*3/(8*pi*G)
















