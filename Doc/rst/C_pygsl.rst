the C pygsl module
**********************

.. currentmodule:: pNbody.pygsl

.. automodule:: pNbody.pygsl
   :members:
