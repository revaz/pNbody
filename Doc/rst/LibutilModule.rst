the libutil module
**********************

.. currentmodule:: pNbody.libutil

.. automodule:: pNbody.libutil
   :members:
