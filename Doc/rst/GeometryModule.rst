the geometry module
**********************

.. currentmodule:: pNbody.geometry

.. automodule:: pNbody.geometry
   :members:
