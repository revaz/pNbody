the param module
**********************

.. currentmodule:: pNbody.param

.. automodule:: pNbody.param
   :members:
