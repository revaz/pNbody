.. _physical_quantities-label: 

Computing physical quantities
*****************************

To run the examples proposed hereafter, you need first to follow the instructions given in 
the section :ref:`generate_examples-label` .

We also suppose that the following model has been loaded, i.e, in your python interpreter you typed::

  >>> from pNbody import Nbody
  >>> nb = Nbody("MW_galaxy.hdf5")



From brute arrays
-----------------

Any array attached to an ``Nbody`` object contains physical quantities. They corresponds precisely to the values
stored in the snapshot without any conversion. In some cases working with those array can be useful. However, by
default, no units are attached to those arrays. They can be used the following way::

  # coordinates of particles
  >>> nb.pos
  
  array([[ 0.59459096, -6.849886  ,  0.83441144],
         [ 1.1637805 ,  9.026595  ,  0.10752326],
         [ 3.0834882 , -0.19680543, -0.28759104],
         ...,
         [ 4.139927  ,  1.5572525 ,  0.14975849],
         [-2.6131186 , -2.20977   , -0.05962646],
         [-3.0051165 , -5.1332235 ,  0.5773136 ]], dtype=float32)
  
  # initial specific energy
  >>> nb.u_init

  array([1.0152912, 1.0152912, 1.0152912, ..., 0.       , 0.       ,
       0.       ], dtype=float32)


A large number of methods works directly on these brute arrays::

  >>> nb.cm()
  
  array([-0.3414196 ,  0.1023279 ,  0.59247654])
  
  >>> nb.x()
  
  array([ 0.59459096,  1.1637805 ,  3.0834882 , ...,  4.139927  ,
         -2.6131186 , -3.0051165 ], dtype=float32)
         
  >>> nb.vx()
  
  array([ 219.8732  , -206.42065 ,   11.346481, ...,  -13.726294,
          111.58997 ,  187.3271  ], dtype=float32)
  

By convention, the name of those methods stars with a lower case character.


With a proper conversion
------------------------

If the ``Nbody`` object contains units or information if it comes from a cosmological simulation or not,
a proper conversion can be performed using methods. 
By convention, the name of those methods stars with an upper case character.
Those methods can be supplemented with units. 
How quantities are converted in different situations is described in section :ref:`units_comovingnoncomoving-label` .

For example, to get the position of particles in kpc::

  >>> nb.Pos(units="kpc")

  array([[ 0.59459096, -6.849886  ,  0.83441144],
         [ 1.1637805 ,  9.026595  ,  0.10752326],
         [ 3.0834882 , -0.19680543, -0.28759104],
         ...,
         [ 4.139927  ,  1.5572525 ,  0.14975849],
         [-2.6131186 , -2.20977   , -0.05962646],
         [-3.0051165 , -5.1332235 ,  0.5773136 ]], dtype=float32)


For example, to get the velocities of particles in km/s::

  >>> nb.Vel(units="km/s")
  
  array([[ 219.8732   ,    9.581228 ,   11.225813 ],
         [-206.42065  ,   22.783592 ,    1.2769856],
         [  11.346481 ,  182.89082  ,   20.033907 ],
         ...,
         [ -13.726294 ,  175.35052  ,    8.549284 ],
         [ 111.58997  , -112.51938  ,   63.499905 ],
         [ 187.3271   , -109.07539  ,  -16.536251 ]], dtype=float32)


The following table profile a list of usefull methods.

============================ ===================================================== =============
method name                  meaning                                               status
============================ ===================================================== =============
nb.Pos()                     Coordinates                                           ok
nb.Vel()                     Velocities                                            ok
nb.Mass()                    Mass                                                  ok
nb.TotalMass()               Total mass                                            ok
nb.InitialMass()             Initial mass (before stellar mass loss)               ok
nb.Rxyz()                    3D distance to the center                             ok
nb.Rxy()                     2D distance to the center                             ok
nb.sphRadius()               SPH smoothing value                                   ok
nb.Rho()                     Gas density                                           ok
nb.FormationGasDensity()     Time/scale factor at which the gas particle formed    ok
nb.InternalEnergy()          Internal energy                                       ok
nb.GasMeanWeight()           Gas mean molecular weight                             to be checked
nb.GasMeanWeightDD()         Gas mean molecular weight                             to be checked             
nb.T()                       Temperature                                           ok
nb.Time()                    Time/scale factor                                     ok 
nb.CosmicTime()              Formation time/scale factor for each stellar particle ok      
nb.StellarFormationTime()    Time at which a stellar particle formed               ok  
nb.StellarAge()              Age of a stellar particle                             ok
nb.ScaleFactor()             Scale factor                                          ok
nb.Redshift()                Redshift                                              ok
nb.LuminositySpec()          Specific luminosity of a stellar particle             ok
nb.Luminosity()              Luminosity of a stellar particle                      ?
nb.Magnitudes()              Magnitude of a stellar particle                       ?
nb.TotalMagnitude()          Total magnitude of a model                            ?
nb.TotalKineticEnergy()      Total kinetic energy                                  ?
nb.TotalPotentialEnergy()    Total potential energy                                ?
nb.TJeans()                  Jeans temperature                                     ?
nb.Tff()                     Free-fall time                                        ?
============================ ===================================================== =============
