Default configuration
**********************

**pNbody** uses a set of parameters files, color tables and formats files. 
These files are provided by the installation and are by default stored in 
the directory ``site-packages/pNbody/config``.
To display where these files are taken from, you can use the command::

  pNbody_show-path

It is recommanded that the user uses its own configuration files. To be automatically
recongnized by **pNbody**, the latter must be in the user ``~/.pNbody`` directory.
**pNbody** provides a simple command to copy all parameters in this directory. Simply
type::

  pNbody_copy-defaultconfig

and check the values of the new paths::

  pNbody_show-path

You can now freely modify the files contains in the configuratio directory.

By default, the content of the configuration directory is:

+------------------------+------------+--------------------------------------------------------------------------------+
| name                   | type       | Content                                                                        |
+========================+============+================================================================================+
| defaultparameters      | file       | the default graphical parameters used by **pNbody**                            |
+------------------------+------------+--------------------------------------------------------------------------------+
| unitsparameters        | file       | the default units parameters used by **pNbody**                                |
+------------------------+------------+--------------------------------------------------------------------------------+
| formats                | directory  | specific class definition files used to read different file formats            |
+------------------------+------------+--------------------------------------------------------------------------------+
| extensions             | directory  | extent the default pNbody class with new methods (see my_default_extension.py) |
+------------------------+------------+--------------------------------------------------------------------------------+
| rgb_tables             | directory  | color tables                                                                   |
+------------------------+------------+--------------------------------------------------------------------------------+
| plugins                | directory  | optional plugins                                                               |
+------------------------+------------+--------------------------------------------------------------------------------+
| opt                    | directory  | optional files                                                                 |
+------------------------+------------+--------------------------------------------------------------------------------+

