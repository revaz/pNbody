Install
*******

While **pNbody** depends on several libraries, the missing dependencies should be automatically installed if
**pNbody** is installed with ``pip``. So, you should not care about installing anything manually.


Installing from PYPI
--------------------

If you do not need the last version or a specific one, you can directly install **pNbody** with pip, without any
preliminary download::

  pip install pNbody
    


Installing from source, in an existing python environment
---------------------------------------------------------

If you cloned  **pNbody** from gitlab in a folder named ``pNbody``, just type::

  pip install ./pNbody


Installing **pNbody** in a specific directory is done with the
``--prefix`` option::

  pip install --prefix=/where/you/want/to/install/it ./pNbody 



Installing using a fresh Python Virtual Environment
---------------------------------------------------

Make sure that the **PYTHONPATH** variable is not set::

  unset PYTHONPATH

Create a python virtual environment and activate it::

  python3 -m venv ~local/python-venv/pNbody
  source ~local/python-venv/pNbody/bin/activate



Install **pNbody**, from the cloned directroy ``pNbody``::

  pip install ./pNbody

or by downloading a version from PYPI::

  pip install pNbody


Install additional python modules for display purposes::

  pip install PyQt5
  



Installing using a Conda Environment
------------------------------------

Make sure that the **PYTHONPATH** variable is not set::

  unset PYTHONPATH

Create a new conda environment and activate it::

  conda create -n pNbody-env
  conda activate pNbody-env


Install **pNbody**, from the cloned directroy ``pNbody``::

  pip install ./pNbody

or by downloading a version from PYPI::

  pip install pNbody


Install additional python modules for display purposes::

  pip install PyQt5

Troubleshooting
---------------

In some rare cases some libraries can be missing, leading to a crash of the installation.
In this case it can be useful to edit the ``setup.py`` file before running ``pip install``.


