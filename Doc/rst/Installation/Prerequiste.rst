Prerequisite
************

**pNbody** is fully based on standards tools. Most of them are
installed by default on a linux system.


The basic module of **pNbody** needs :

1) Python 3.10.x:

   http://www.python.org
   
   
2) pip:

   https://pypi.org/project/pip/
   
   
3) a C compiler like gcc:

   http://gcc.gnu.org/
   
   
4) the Gnu Scientific Library (GSL):

   https://www.gnu.org/software/gsl/
   
   

The core of **pNbody** uses the following python modules (if missing, those modules will be automatically installed).
It is however recommended to have at least **numpy** installed first (``pip install 'numpy<2.0'``).

1) numpy-1.0.4 or higher, but smaller than 2.0.0:
   
   http://numpy.scipy.org/
   

2) Pillow 8.4.0 or higher:
   
   https://pypi.org/project/Pillow/
   

3) scipy 1.4.1 or higher:
   
   http://www.scipy.org/
   
   
4) astropy:
   
   https://www.astropy.org/ 
   
   
5) tqdm:
   
   https://github.com/tqdm/tqdm  
   
6) setuptools:
   
   https://pypi.org/project/setuptools/
   
7) wheel:

  https://pypi.org/project/wheel/   
   
8) versioneer:

  https://pypi.org/project/versioneer/

9) IPython:

  https://ipython.org/

For display purposes, those additional modules will be required:

10) matplotlib

  https://matplotlib.org/


11) PyQt5 

  https://pypi.org/project/PyQt5/

12) tkinter (could replace PyQt5)

  https://docs.python.org/3/library/tkinter.html
        
   
For the parallel capabilities, an mpi distribution is needed (ex. openmpi) 
as well as the additional python mpi wrapping:

13) mpi4py:

   http://cheeseshop.python.org/pypi/mpi4py
   
 
In order to convert movies in standard format (gif or mpeg), the two following applications are needed :

1) convert (imagemagick)
   
   http://www.imagemagick.org/script/index.php
   
   
2) mencoder (mplayer)
   
   http://www.mplayerhq.hu/design7/news.html
   





