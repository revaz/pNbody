''gpy'': the shortcut to open a snapshot 
****************************************

**pNbody** provides a script called ``gpy`` that ease the loading of a snapshot and the creation of a **pNbody** object.
From the example directory, you can invoke the ``gpy`` command ::


  $ gpy MW_galaxy.hdf5
  Python 3.10.12 (main, Sep 11 2024, 15:47:36) [GCC 11.4.0]
  Type 'copyright', 'credits' or 'license' for more information
  IPython 7.31.1 -- An enhanced Interactive Python. Type '?' for help.
  

This command launches the ``IPython`` interpreter, load the  ``pNbody`` and ``numpy`` 
modules and creates a **pNbody** object from the ``MW_galaxy.hdf5`` file. It is thus equivalent to run::

  >>> from pNbody import Nbody
  >>> import numpy as np
  >>> nb = Nbody("MW_galaxy.hdf5")


In the ``IPython`` environment you can then directly access the **pNbody** object via the ``nb`` variable::

  In [1]: nb.nbody
  Out[1]: 20235


Note that ``gpy`` comes with a bunch of options. See :ref:`gpy_scripts-label` for the full documentation.

The ``--arrays`` options informs **pNbody** on the arrays to load::

  $ gpy --arrays pos,vel MW_galaxy.hdf5
  
In this examples, only the coordinates and velocities will be loaded.  

The ``--ptypes`` options informs **pNbody** on the type of particles to load::

  $ gpy --ptypes 0 MW_galaxy.hdf5

In this case, only gas particles will be loaded.


Finally, an important one is ``--verbose`` which sets the verbosity of **pNbody**. Putting it to ``10`` sets the highest level 
of verbosity which can be essential for debugging purposes::

  $ gpy --verbose 10 MW_galaxy.hdf5
  find_format ("/home/revaz/local/local2023/soft/python-3.10/local/lib/python3.10/dist-packages/pNbody/main.py", line 407): the format file is not gh5
  find_format ("/home/revaz/local/local2023/soft/python-3.10/local/lib/python3.10/dist-packages/pNbody/main.py", line 410): The format is swift
  read_particles ("/home/revaz/local/local2023/soft/python-3.10/local/lib/python3.10/dist-packages/pNbody/config/formats/swift.py", line 856): reading header...
  read_particles ("/home/revaz/local/local2023/soft/python-3.10/local/lib/python3.10/dist-packages/pNbody/config/formats/swift.py", line 890): Header BoxSize >> boxsize [1500.32945615]
  read_particles ("/home/revaz/local/local2023/soft/python-3.10/local/lib/python3.10/dist-packages/pNbody/config/formats/swift.py", line 890): Header Command_line >> command_line [b'/home/revaz/local/local2023/soft/python-3.10/local/bin/gpy MW_galaxy.hdf5']
  read_particles ("/home/revaz/local/local2023/soft/python-3.10/local/lib/python3.10/dist-packages/pNbody/config/formats/swift.py", line 890): Header Date >> date [b'Sat Nov 16 17:48:23 2024']
  read_particles ("/home/revaz/local/local2023/soft/python-3.10/local/lib/python3.10/dist-packages/pNbody/config/formats/swift.py", line 890): Header Flag_Entropy_ICs >> flag_entr_ics [0]
  read_particles ("/home/revaz/local/local2023/soft/python-3.10/local/lib/python3.10/dist-packages/pNbody/config/formats/swift.py", line 890): Header GitTag >> gittag [b'40dfb622619fd9e69678c1ffa6dbf6601bc22348']
  read_particles ("/home/revaz/local/local2023/soft/python-3.10/local/lib/python3.10/dist-packages/pNbody/config/formats/swift.py", line 890): Header MassTable >> massarr [     0      0 100000      0      0      0]
  read_particles ("/home/revaz/local/local2023/soft/python-3.10/local/lib/python3.10/dist-packages/pNbody/config/formats/swift.py", line 890): Header NumFilesPerSnapshot >> num_files [1]
  read_particles ("/home/revaz/local/local2023/soft/python-3.10/local/lib/python3.10/dist-packages/pNbody/config/formats/swift.py", line 890): Header NumPart_ThisFile >> npart [  391 17969     0     0  1875     0]
  read_particles ("/home/revaz/local/local2023/soft/python-3.10/local/lib/python3.10/dist-packages/pNbody/config/formats/swift.py", line 890): Header NumPart_Total >> npart_tot [  391 17969     0     0  1875     0]
  read_particles ("/home/revaz/local/local2023/soft/python-3.10/local/lib/python3.10/dist-packages/pNbody/config/formats/swift.py", line 890): Header NumPart_Total_HighWord >> nallhw [0 0 0 0 0 0]
  read_particles ("/home/revaz/local/local2023/soft/python-3.10/local/lib/python3.10/dist-packages/pNbody/config/formats/swift.py", line 890): Header Redshift >> redshift [0.]
  read_particles ("/home/revaz/local/local2023/soft/python-3.10/local/lib/python3.10/dist-packages/pNbody/config/formats/swift.py", line 890): Header Scale-factor >> atime [0.]
  read_particles ("/home/revaz/local/local2023/soft/python-3.10/local/lib/python3.10/dist-packages/pNbody/config/formats/swift.py", line 890): Header UserName >> username [b'revaz']
  read_particles ("/home/revaz/local/local2023/soft/python-3.10/local/lib/python3.10/dist-packages/pNbody/config/formats/swift.py", line 890): Cosmology Omega_lambda >> omegalambda [0.]
  read_particles ("/home/revaz/local/local2023/soft/python-3.10/local/lib/python3.10/dist-packages/pNbody/config/formats/swift.py", line 890): Cosmology Omega_m >> omega0 [0.]
  read_particles ("/home/revaz/local/local2023/soft/python-3.10/local/lib/python3.10/dist-packages/pNbody/config/formats/swift.py", line 890): Cosmology h >> hubbleparam [0.]
  read_particles ("/home/revaz/local/local2023/soft/python-3.10/local/lib/python3.10/dist-packages/pNbody/config/formats/swift.py", line 890): Units Unit current in cgs (U_I) >> Unit_current_in_cgs [1.]
  read_particles ("/home/revaz/local/local2023/soft/python-3.10/local/lib/python3.10/dist-packages/pNbody/config/formats/swift.py", line 890): Units Unit length in cgs (U_L) >> UnitLength_in_cm [3.085e+21]
  read_particles ("/home/revaz/local/local2023/soft/python-3.10/local/lib/python3.10/dist-packages/pNbody/config/formats/swift.py", line 890): Units Unit mass in cgs (U_M) >> UnitMass_in_g [1.98848e+43]
  read_particles ("/home/revaz/local/local2023/soft/python-3.10/local/lib/python3.10/dist-packages/pNbody/config/formats/swift.py", line 890): Units Unit temperature in cgs (U_T) >> Unit_temp_in_cgs [1.]
  read_particles ("/home/revaz/local/local2023/soft/python-3.10/local/lib/python3.10/dist-packages/pNbody/config/formats/swift.py", line 890): Units Unit time in cgs (U_t) >> Unit_time_in_cgs [3.085e+16]
  read_particles ("/home/revaz/local/local2023/soft/python-3.10/local/lib/python3.10/dist-packages/pNbody/config/formats/swift.py", line 969): reading units...
  read_particles ("/home/revaz/local/local2023/soft/python-3.10/local/lib/python3.10/dist-packages/pNbody/config/formats/swift.py", line 999): reading particles...
  loadFromHDF5 ("/home/revaz/local/local2023/soft/python-3.10/local/lib/python3.10/dist-packages/pNbody/main.py", line 1805): loading pos ptype=0 dtype=<class 'numpy.float32'> dim=3
  loadFromHDF5 ("/home/revaz/local/local2023/soft/python-3.10/local/lib/python3.10/dist-packages/pNbody/main.py", line 1805): loading pos ptype=1 dtype=<class 'numpy.float32'> dim=3
  loadFromHDF5 ("/home/revaz/local/local2023/soft/python-3.10/local/lib/python3.10/dist-packages/pNbody/main.py", line 1805): loading pos ptype=4 dtype=<class 'numpy.float32'> dim=3
  loadFromHDF5 ("/home/revaz/local/local2023/soft/python-3.10/local/lib/python3.10/dist-packages/pNbody/main.py", line 1805): loading vel ptype=0 dtype=<class 'numpy.float32'> dim=3
  loadFromHDF5 ("/home/revaz/local/local2023/soft/python-3.10/local/lib/python3.10/dist-packages/pNbody/main.py", line 1805): loading vel ptype=1 dtype=<class 'numpy.float32'> dim=3
  loadFromHDF5 ("/home/revaz/local/local2023/soft/python-3.10/local/lib/python3.10/dist-packages/pNbody/main.py", line 1805): loading vel ptype=4 dtype=<class 'numpy.float32'> dim=3
  loadFromHDF5 ("/home/revaz/local/local2023/soft/python-3.10/local/lib/python3.10/dist-packages/pNbody/main.py", line 1805): loading mass ptype=0 dtype=<class 'numpy.float32'> dim=1
  loadFromHDF5 ("/home/revaz/local/local2023/soft/python-3.10/local/lib/python3.10/dist-packages/pNbody/main.py", line 1805): loading mass ptype=1 dtype=<class 'numpy.float32'> dim=1
  loadFromHDF5 ("/home/revaz/local/local2023/soft/python-3.10/local/lib/python3.10/dist-packages/pNbody/main.py", line 1805): loading mass ptype=4 dtype=<class 'numpy.float32'> dim=1
  loadFromHDF5 ("/home/revaz/local/local2023/soft/python-3.10/local/lib/python3.10/dist-packages/pNbody/main.py", line 1805): loading num ptype=0 dtype=<class 'numpy.uint32'> dim=1
  loadFromHDF5 ("/home/revaz/local/local2023/soft/python-3.10/local/lib/python3.10/dist-packages/pNbody/main.py", line 1805): loading num ptype=1 dtype=<class 'numpy.uint32'> dim=1
  loadFromHDF5 ("/home/revaz/local/local2023/soft/python-3.10/local/lib/python3.10/dist-packages/pNbody/main.py", line 1805): loading num ptype=4 dtype=<class 'numpy.uint32'> dim=1
  loadFromHDF5 ("/home/revaz/local/local2023/soft/python-3.10/local/lib/python3.10/dist-packages/pNbody/main.py", line 1805): loading rsp_init ptype=0 dtype=<class 'numpy.float32'> dim=1
  loadFromHDF5 ("/home/revaz/local/local2023/soft/python-3.10/local/lib/python3.10/dist-packages/pNbody/main.py", line 1805): loading rsp_init ptype=4 dtype=<class 'numpy.float32'> dim=1
  loadFromHDF5 ("/home/revaz/local/local2023/soft/python-3.10/local/lib/python3.10/dist-packages/pNbody/main.py", line 1805): loading u_init ptype=0 dtype=<class 'numpy.float32'> dim=1
  loadFromHDF5 ("/home/revaz/local/local2023/soft/python-3.10/local/lib/python3.10/dist-packages/pNbody/main.py", line 1805): loading tstar_init ptype=4 dtype=<class 'numpy.float32'> dim=1
  loadFromHDF5 ("/home/revaz/local/local2023/soft/python-3.10/local/lib/python3.10/dist-packages/pNbody/main.py", line 1805): loading sp_type ptype=4 dtype=<class 'numpy.int32'> dim=1
  read_particles ("/home/revaz/local/local2023/soft/python-3.10/local/lib/python3.10/dist-packages/pNbody/config/formats/swift.py", line 1024): Massarr is not supported! Please specify the mass of all the particles!
  Python 3.10.12 (main, Sep 11 2024, 15:47:36) [GCC 11.4.0]
  Type 'copyright', 'credits' or 'license' for more information
  IPython 7.31.1 -- An enhanced Interactive Python. Type '?' for help.
  
  In [1]:



.. _gpy_scripts-label:

Scripts
-------


``gpy``
.......

.. program-output:: gpy -h


