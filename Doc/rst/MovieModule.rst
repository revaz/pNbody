the Movie module
**********************

.. currentmodule:: pNbody.Movie

.. automodule:: pNbody.Movie
   :members:
