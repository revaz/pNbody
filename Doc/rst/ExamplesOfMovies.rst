How to generate movies
**********************

**pNbody** provides several examples showing how to generate movies from snapshot files.
The examples are accessible from the ``examples`` directory provided in the **pNbody** distribution.
To run those examples, follow the ``README`` files.
