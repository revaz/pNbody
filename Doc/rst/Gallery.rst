Gallery
**********************

.. toctree::
   :maxdepth: 5
  
Cosmological simulations
========================

Density map of a comobile cube (20Mpc^3/h^3) of the univers at z=40.
268 millions of particles have been used to create this map.

   .. image:: ../gallery/ci.gif
     :width: 800 px

Density map of a comobile cube (20Mpc^3/h^3) of the univers at z=6.

   .. image:: ../gallery/gaslcdm256.jpg
     :width: 800 px


Sterescopic view of the univers at z=0.
(blue: gas, green:dark matter, red:stars)

   .. image:: ../gallery/univers.jpg
     :width: 800 px


Sterescopic zoom of a cluster in a cosmological simulation.
(blue: gas, green:dark matter, red:stars)


   .. image:: ../gallery/amas.jpg
     :width: 800 px


Evolution of a makino univers.

   .. image:: ../gallery/univers_makino.gif
     :width: 800 px



  
Isolated and interacting galaxies
=================================

3D model of the MilkyWay warp

   .. image:: ../gallery/milkywaywarp.gif 
     :width: 800 px

Bending instabilities in spiral galaxies
(edge-on density map, face-on density map, z map, araki ratio map)

   .. image:: ../gallery/bending.gif 
     :width: 800 px

Winding of the line of node of a warped disk
(z map, edge-on density map)

   .. image:: ../gallery/winding.gif 
     :width: 800 px

Velocity map of disks subjet to the bending instabilities

   .. image:: ../gallery/warp.gif 
     :width: 800 px

Edge on and Face on surface density maps of an evolving spiral galaxy 

   .. image:: ../gallery/spiral.gif 
     :width: 800 px

Edge on and Face on surface density maps with contours of a spiral galaxy 

   .. image:: ../gallery/gal.gif 
     :width: 800 px
   
Edge on and Face on surface density maps with contours of a spiral galaxy 
   
   .. image:: ../gallery/multiphase.gif 
     :width: 800 px
   
Stereoscopic view of interacting galaxies

   .. image:: ../gallery/galaxies.jpg
     :width: 800 px
   
Sample of interacting galaxies    

   .. image:: ../gallery/merger.gif
     :width: 800 px
   
Sample of interacting galaxies    
   
   

Cluster of galaxies
=================================


Evolution of buoyantly plasma bubbles in a cooling flow cluster
(temperature map, density map, entropy map)

   .. image:: ../gallery/cluster.gif
     :width: 800 px

Temperature evolution of buoyantly plasma bubbles in a cooling flow cluster

   .. image:: ../gallery/cluster2.gif
     :width: 800 px
   
Formation of cold gas in a cooling flow cluser perturbed by plasma bubbles

   .. image:: ../gallery/coldgas.gif
     :width: 800 px
   
   
   
   
   














