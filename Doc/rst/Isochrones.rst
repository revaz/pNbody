Handling isochrones : the Isochrones module
===========================================

.. currentmodule:: pNbody.Isochrones


The ``Isochrones`` module provide tools to handle isochrones libraries and turn them
into stellar fluxes or stellar magnitudes in different filters.

Currently ``Isochrones`` uses isochrones from the 
`MIST <https://waps.cfa.harvard.edu/MIST/>`_
from the  
`CMD <http://stev.oapd.inaf.it/cgi-bin/cmd>`_
projects and ``BastI`` isochrones.


MIST isochrones
---------------

``MIST`` isochrones come as a set of files in a directory.
To download the ``MIST`` databases for a given filter set,
move to ``workspace/Isochrones/MIST`` in the ``pNbody`` distribution. Informations are provided in a ``README`` file.

Isochrones can be displayed with the ``isochrones_plot`` command (see :ref:`isochrones_scripts-label` for mode
details), for example:

.. code-block::
   
   isochrones_plot -d MIST_v1.2_vvcrit0.0_UBVRIplus --Age 13000 --x log_Teff  --y 'log_L' --invertx

where ``MIST_v1.2_vvcrit0.0_UBVRIplus`` is a ``MIST`` directory.

The list of keys stored in the database can be obtained from the ``--info`` option::
   
   isochrones_plot -d MIST_v1.2_vvcrit0.0_UBVRIplus --info


BastI isochrones
----------------

``BastI`` isochrones come as a set of files in a directory. To download the ``BastI`` databases for a given filter set,
move to ``workspace/Isochrones/BastI`` in the ``pNbody`` distribution. Informations are provided in a ``README`` file.
Once the database (directory) is downloaded, isochrones can be displayed with the 
``isochrones_plot`` command (see :ref:`isochrones_scripts-label` for mode
details), for example:

.. code-block::
   
   isochrones_plot -d P04O1D1E1Y247 --Age 13000 --x G_BP-G_RP --y G --inverty --xmin -1 --xmax 3 --ymin -5 --ymax 13 --MH -2

where ``P04O1D1E1Y247`` is a ``BastI`` directory.
The list of available ``BastI`` isochrones:

.. list-table:: ``BastI`` isochrones
   :widths: 20 50 20
   :header-rows: 1

   * - name
     - description
     - release
   * - P00O1D1E1Y247   
     - Scaled solar models & transformations  
     - GAIA-DR3
   * - P04O1D1E1Y247   
     - Alpha-enhanced=+0.4 models & transformations  
     - GAIA-DR3
   * - P04O1D1E1Y275   
     - Alpha-enhanced=+0.4 models & transformations  
     - GAIA-DR3
   * - P04O1D1E1Y300   
     - Alpha-enhanced=+0.4 models & transformations  
     - GAIA-DR3
   * - P04O1D1E1Y320    
     - Alpha-enhanced=+0.4 models & transformations  
     - GAIA-DR3 



CMD isochrones
--------------

``CMD`` isochrones are stored in one single file. 
To download the ``CMD`` file, move to ``workspace/Isochrones/CMD`` in the ``pNbody`` distribution. 
Informations are provided in a ``README`` file.

Isochrones can be displayed with the ``isochrones_plot`` command (see :ref:`isochrones_scripts-label` for mode
details), by directly pointing towards the CMD file, for example:

.. code-block::
   
   isochrones_plot -f CMD_Euclid.dat --Age 13000 --x Bluemag-Redmag  --y VISmag --inverty 


Retrieving info from the Isochrones database
--------------------------------------------

Computing magnitudes
....................

The following lines provides an example how to get

.. code-block::
   
   from pNbody import Isochrones
   
   # load the database
   DB = Isochrones.Isochrones(dirname="P04O1D1E1Y247")

   # get the keys
   keys = DB.getKeys().keys()

   # check that one specific key exists
   DB.checkKeyExists("G")  
   
   # define an age (in Myr)
   age = 13000
   
   # define a metallicity (log10(M/H))
   mh = -2
   
   # define a star mass (in solar mass)
   mass = 0.65
   
   # get the maximal stellar mass (in solar mass) for a given age and metallicity
   DB.getMaxStellarMass(age,mh)   
   
   # get the corresponding magnitude in a given filter
   DB.get(age,mh,1,key="G")



Generate a SSP magnitude grid
-----------------------------

As it allows to retrieve a magnitude for a given stellar mass, a given age and a given
metalliity, the ``Isochrones`` module can be use to create magnitudes lookup tables for a
given single stellar population, i.e., a given age an metallicity, assuming some IMF. 
Such table is created with the ``isochrones_generate_magnitudes_grid`` command, for example (see :ref:`isochrones_scripts-label` for mode
details):

.. code-block::

  isochrones_generate_magnitudes_grid  --rebinAges --M0 1e6 --filter_key VIS             -d  P04O1D1E1Y247_euclid -o Euclid_VIS_BastI_1e6.hdf5
    
or:

.. code-block::
   
   isochrones_generate_magnitudes_grid  --rebinAges --M0 1e6 --filter_key VISmag   -o EuclidVISmag_CMD_1e6.pkl  CMD_Euclid.dat


By default the script uses a Kroupa IMF which is hardcoded in the script. It is possible to change the IMF by providing a file
(using the ``--imf`` parameter) containing for example:

.. code-block::

   params = {}
   params["Mmax"] = 50.
   params["Mmin"] = 0.05
   params["as"] = [0.7,-0.8,-1.7,-1.3]
   params["ms"] = [0.08,0.5,1.0]


where ``as`` is the slope of the IMF in the mass intervals ``ms`` given is solar masses:

The output of ``isochrones_generate_magnitudes_grid`` is either a python pickle file or an hdf5 file which contains a 2D magnitude table and two vectors providing the binning in age and metallicity. Ages are provided in Myrs.

The pickle file can be read as:

.. code-block::
   
   import pickle
   f = open(filename,"rb")
   Mags = pickle.load(f)
   binsAge = pickle.load(f)
   binsMH  = pickle.load(f)
   f.close()


The script ``isochrones_plot_magnitudes_grid``
(see :ref:`isochrones_scripts-label` for mode details) 
allows to plot the previously generated magnitudes grid::

  isochrones_plot_magnitudes_grid EuclidVISmag_CMD_1e6.pkl


The info on the content of the hdf5 files can be obtained with::

  isochrones_ssp_grid_info Euclid_VIS_BastI_1e6.hdf5

Check the content of ``isochrones_ssp_grid_info`` to see how to read this file format.


Generate a SSP luminosity grid
------------------------------

If the isocrhones tables provide a luminosity field (log of the luminosity)
it is possible to generate a luminosity instead of a magnitude grid (note the ``--luminosity_key``)::

  isochrones_generate_magnitudes_grid  --rebinAges --M0 1e6 --luminosity_key 'log(L/Lo)' -d P04O1D1E1Y247_euclid  -o BolLum_BastI_1e6.hdf5
  

If the output is stored as a pkl file, the table may be displayed with::

  isochrones_plot_magnitudes_grid --luminosity Euclid_logL_CMD_1e6.pkl 


SSP Grids
---------

Single stellar population grids (SSP) are stored in the ``pNbody` directory ::

  config/opt/filters/

or on the user directory ::

  ~/.pNbody/opt/filters/

Those tables can be re-created using the script::

  tools/Isochrones/generateSSPGrids.sh


.. _isochrones_scripts-label:  

Isochrones Scripts
------------------



``isochrones_plot``
-------------------

.. program-output:: isochrones_plot -h


``isochrones_py``
-----------------

.. program-output:: isochrones_py -h


``isochrones_generate_magnitudes_grid``
---------------------------------------

.. program-output:: isochrones_generate_magnitudes_grid -h

``isochrones_plot_magnitudes_grid``
-----------------------------------

.. program-output:: isochrones_plot_magnitudes_grid -h


``isochrones_ssp_grid_info``
----------------------------

.. program-output:: isochrones_ssp_grid_info -h


The IsochronesBlock class
-------------------------

.. currentmodule:: pNbody.Isochrones.main

.. autoclass:: IsochroneBlock
   :members:

The IsochronesGrid class
------------------------

.. currentmodule:: pNbody.Isochrones.main

.. autoclass:: IsochronesGrid
   :members:






