Selecting particles
*******************

List of selection functions
---------------------------


**pNbody** provides several fonction to select particles:

=============== =================================
function name   description
=============== =================================
selectc         select particles from a condition
selecti         select particles from their index
selectp         select particles from their id
sub             select particles from their index
reduc           select a fraction of particles
=============== =================================

Examples
--------


To run the examples of this section, you must first move to the ``~/pNbody_examples`` directory.
If the latter does not exists, you can first create it with::

  pNbody-examples-copy

and move inside the created directory.


Open a file containing particles and display it ::

  >>> from pNbody import Nbody
  >>> import numpy as np
  >>> nb = Nbody("disk.dat",ftype='gadget')
  >>> nb.cmcenter()
  >>> nb.display(size=(100,100))


Select particles with a positive x coordinate ::
  
  >>> nb = nb.selectc(nb.x()>0)
  >>> nb.display(size=(100,100))  
  
  
Select the first half of the particles, from their indices ::
  
  >>> indices = np.arange(nb.nbody/2,dtype=int)
  >>> nb = nb.selecti(indices)
  >>> nb.display(size=(100,100))  


Select particles based on their IDs ::
  
  >>> nb = nb.selectp(lst=[161,171,191,201,211,221,241,261,301])
  >>> print(nb.num)
  

Select a fraction of particles, reduce the number of particles by a factor 8, keeping 1 particles very 8 ::
  
  >>> nb = Nbody("disk.dat",ftype='gadget')
  >>> nb.cmcenter()
  >>> nb.display(size=(100,100))
  >>> nb = nb.reduc(8)
  >>> nb.display(size=(100,100))  
  

Select a subset of particles with indices from 8 to 16 ::
  
  >>> nb = nb.sub(8,16)







