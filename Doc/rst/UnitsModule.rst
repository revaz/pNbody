the units module
**********************

.. currentmodule:: pNbody.units

.. automodule:: pNbody.units
   :members:
