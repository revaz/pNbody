Generating initial conditions
*******************************

**pNbody** provides several methods and script
to generate initial conditions for N-body evolution codes. 
No method exists yet to generate cosmological initial conditions.

The generation of initial conditions is devided into
two parts. First, the generation of a mass profile by
distributing particles in space. Secondly, the determination
of the velocities for each of these particles. 

A series of script is also provided to ease the generation of initial conditions.
The first set of scripts distribute particles in the phase-space, properly sampling a distribution function.
The second set solve the Jeans equations in spherical and cylindrical coordinates to assemble a multi-component galaxy.



.. toctree::
   :maxdepth: 2
   
   GeneratingMassProfiles
   GeneratingVelocities
   GeneratingFromDistributionFunction
   GeneratingAMulitComponentsGalaxy
   GeneratingTheMW2014Model
