Mock images
***********


This section describe how ``pNbody`` and its sub-module ``pNbody.Mockimgs`` (:ref:`mockimgs-label` ) 
provides tools to generate mock observation images from N-body models.


Contents:

.. toctree::
   :maxdepth: 2
   
   MockImages/ModelPreparation
   MockImages/DwarfModels
   MockImages/Mockimgs
   MockImages/SurfaceBrightnessMaps  
   MockImages/AddNoise 
   MockImages/Instruments     
   MockImages/Filters    
   MockImages/Examples
