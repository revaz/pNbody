###########################################################################################
#  package:   pNbody
#  file:      moria.py
#  brief:     Moria format (binary)
#  copyright: GPLv3
#             Copyright (C) 2019 EPFL (Ecole Polytechnique Federale de Lausanne)
#             LASTRO - Laboratory of Astrophysics of EPFL
#  author:    Yves Revaz <yves.revaz@epfl.ch>
#
# This file is part of pNbody.
###########################################################################################

########################################################################
#
# MORIA CLASS
#
########################################################################

import sys
import collections
from pNbody import mpi, error, libutil
from pNbody import units, ctes
from pNbody import iofunc as pnio

import numpy as np
# define some units

stars = 4
dark_matter = 1
gas = 0
all_particles = None
nber_tpe = 6

try:                            # all this is usefull to read files
    from mpi4py import MPI
except BaseException:
    MPI = None


class Nbody_moria:

    def _init_spec(self):
        # define some units
        Unit_atom = ctes.PROTONMASS.into(units.cgs) * units.Unit_g
        Unit_atom.set_symbol('atom')
        out_units = units.UnitSystem('local', [units.Unit_cm, Unit_atom,
                                               units.Unit_s, units.Unit_K])
        self.DensityUnit_in_AtomPerCC = out_units.UnitDensity

    def get_excluded_extension(self):
        """
        Return a list of file to avoid when extending the default class.
        """
        return []

    def compulsory_arrays(self, init=False):
        """
        Set arrays to none and return dictionary containing all
        required data to read
        """
        if init:
            self.pos = None
            self.vel = None
            self.num = None
            self.mass = None

        # field_name, data type, shape, particle type
        fields = collections.OrderedDict([
            ("POS ", ["pos", np.float32, 3, all_particles]),
            ("VEL ", ["vel", np.float32, 3, all_particles]),
            ("ID  ", ["num", np.int32, 1, all_particles]),
            ("MASS", ["mass", np.float32, 1, all_particles])
        ])

        return fields

    def optional_arrays(self, init=False):
        """
        Set arrays to none and return dictionary containing all
        required data to read
        """
        if init:
            self.u = None
            self.rho = None
            self.rsp = None
            self.pot = None
            self.acc = None
            self.endt = None
            self.tstp = None
            self.zsph = None
            self.zsta = None
            self.fesp = None
            self.fest = None
            self.mgsp = None
            self.mgst = None
            self.inma = None
            self.myp = None
            self.tfor = None
            self.temp = None
            self.pres = None
            self.dvel = None
            self.tlfb = None
            self.artc = None
            self.er0x = None
            self.er0y = None
            self.er0z = None

        # field_name, data type, shape, particle type
        fields = collections.OrderedDict([
            ("U   ", ["u", np.float32, 1, gas]),
            ("RHO ", ["rho", np.float32, 1, gas]),
            ("HSML", ["rsp", np.float32, 1, gas]),
            ("POT ", ["pot", np.float32, 1, all_particles]),
            ("ACCE", ["acc", np.float32, 3, all_particles]),
            ("ENDT", ["endt", np.float32, 1, gas]),
            ("TSTP", ["tstp", np.float32, 1, all_particles]),
            ("ZSPH", ["zsph", np.float32, 1, gas]),
            ("ZSTA", ["zsta", np.float32, 1, stars]),
            ("FESP", ["fesp", np.float32, 1, gas]),
            ("FEST", ["fest", np.float32, 1, stars]),
            ("MGSP", ["mgsp", np.float32, 1, gas]),
            ("MGST", ["mgst", np.float32, 1, stars]),
            ("INMA", ["inma", np.float32, 1, stars]),
            ("MYP ", ["myp", np.int32, 1, stars]),
            ("TFOR", ["tfor", np.float32, 1, stars]),
            ("TEMP", ["temp", np.float32, 1, gas]),
            ("PRES", ["pres", np.float32, 1, gas]),
            ("DVEL", ["dvel", np.float32, 1, gas]),
            ("TLFB", ["tlfb", np.float32, 1, all_particles]),
            ("ARTC", ["artc", np.float32, 1, all_particles]),
            ("ER0X", ["er0x", np.float32, 1, gas]),
            ("ER0Y", ["er0y", np.float32, 1, gas]),
            ("ER0Z", ["er0z", np.float32, 1, gas]),
        ])

        return fields

    def check_spec_ftype(self):

        for name in self.p_name:
            # read the first 4 bites and check that they are equal to 256
            mpi_test = mpi.mpi_NTask() > 1 and self.pio == "no"
            if mpi.mpi_IsMaster() or self.pio == "yes":
                f = open(name, 'rb')
                try:
                    htype = np.int32
                    nb1 = np.fromstring(f.read(4), htype)
                    if sys.byteorder != sys.byteorder:
                        nb1.byteswap(True)
                    nb1 = nb1[0]
                except BaseException:
                    if mpi_test:
                        mpi.mpi_bcast(False)
                    raise error.FormatError("moria")

                if nb1 != 8:
                    if mpi_test:
                        mpi.mpi_bcast(False)
                    raise error.FormatError("moria")
                elif mpi_test:
                    mpi.mpi_bcast(True)
            else:
                test = mpi.mpi_bcast(None)
                if test is False:
                    raise error.FormatError("moria")

    def get_read_fcts(self):
        return [self.read_particles]

    def get_write_fcts(self):
        return [self.write_particles]

    def get_mxntpe(self):
        return 6

    def get_default_spec_vars(self):
        """
        return specific variables default values for the class
        """
        return {'massarr': np.array([0, 0, self.nbody, 0, 0, 0]),
                'atime': 0.,
                'redshift': 0.,
                'flag_sfr': 0,
                'flag_feedback': 0,
                'nall': np.array([0, 0, self.nbody, 0, 0, 0]),
                'flag_cooling': 0,
                'num_files': 1,
                'boxsize': 0.,
                'omega0': 0.,
                'omegalambda': 0.,
                'hubbleparam': 0.,
                'flag_age': 0.,
                'hubbleparam': 0.,
                'flag_metals': 0.,
                'nallhw': np.array([0, 0, 0, 0, 0, 0]),
                'flag_entr_ic': 0,
                'flag_chimie_extraheader': 0,
                'flag_thermaltime': 0,
                'critical_energy_spec': 0.,
                'empty': 44 * '',
                'comovingintegration': None
                }

    def initComovingIntegration(self):
        """
        set true if the file has been runned using
        the comoving integration scheme
        """

        # do nothing, if the value is already set
        if self.comovingintegration is not None:
            return

        flag = True

        # this is not very good, however, there is not other choice...
        if self.omega0 == 0 and self.omegalambda == 0:
            flag = False

        self.comovingintegration = flag

    def get_massarr_and_nzero(self):
        """
        return massarr and nzero

        !!! when used in //, if only a proc has a star particle,
        !!! nzero is set to 1 for all cpu, while massarr has a length of zero !!!
        """

        if self.has_var('massarr') and self.has_var('nzero'):
            if self.massarr is not None and self.nzero is not None:
                if mpi.mpi_IsMaster():
                    print(
                        "warning : get_massarr_and_nzero : here we use massarr and nzero",
                        self.massarr,
                        self.nzero)
                return self.massarr, self.nzero

        massarr = np.zeros(len(self.npart), float)
        nzero = 0

        # for each particle type, see if masses are equal
        for i in range(len(self.npart)):
            first_elt = sum((np.arange(len(self.npart)) < i) * self.npart)
            last_elt = first_elt + self.npart[i]

            if first_elt != last_elt:
                c = (self.mass[first_elt] ==
                     self.mass[first_elt:last_elt]).astype(int)
                if sum(c) == len(c):
                    massarr[i] = self.mass[first_elt]
                else:
                    nzero = nzero + len(c)

        return massarr.tolist(), nzero

    def read_particles(self, f):
        """
        read moria file
        """
        ##########################################
        # read the header and send it to each proc
        ##########################################
        tpl = (4, np.int32)
        header = pnpnio.ReadBlock(f, tpl, byteorder=self.byteorder, pio=self.pio)
        if header[0] != "HEAD":
            raise IOError("Unable to find 'HEAD', got %s" % header[0])

        tpl = (24, 48, float, float, np.int32, np.int32, 24, np.int32,
               np.int32, float, float, float, float, np.int32, np.int32, 24,
               np.int32, 60)
        header = pnpnio.ReadBlock(f, tpl, byteorder=self.byteorder, pio=self.pio)
        npart, massarr, atime, redshift, flag_sfr, flag_feedback, \
            nall, flag_cooling, num_files, boxsize, omega0, omegalambda, \
            hubbleparam, flag_age, flag_metals, nallhw, flag_entr_ic, empty = header

        npart = np.fromstring(npart, np.int32)
        massarr = np.fromstring(massarr, float)
        nall = np.fromstring(nall, np.int32)
        nallhw = np.fromstring(nallhw, np.int32)

        if sys.byteorder != self.byteorder:
            npart.byteswap(True)
            massarr.byteswap(True)
            nall.byteswap(True)
            nallhw.byteswap(True)

        if flag_metals:             # gas metal properties
            NELEMENTS = flag_metals
            self.NELEMENTS = NELEMENTS
        else:
            NELEMENTS = 0
            self.NELEMENTS = NELEMENTS

        ##########################################
        # computes nzero
        ##########################################
        # count number of particles that have non constant masses and then
        # have masses storded further

        if self.pio == 'no':

            npart_tot = npart
            npart_all = libutil.get_npart_all(npart, mpi.mpi_NTask())
            npart = npart_all[mpi.mpi_ThisTask()]                      # local
            npart_read = npart_tot
            nbody_read = sum(npart_read)

            npart_m_read = npart_tot * (massarr == 0)

            ngas = npart[0]
            ngas_read = npart_tot[0]
            nstars = npart[1]
            nstars_read = npart_tot[1]

            # compute nzero
            nzero = 0
            mass = np.array([])
            for i in range(len(npart_tot)):
                if massarr[i] == 0:
                    nzero = nzero + npart_tot[i]
                else:
                    mass = np.concatenate(
                        (mass, np.ones(npart[i]) * massarr[i]))

        else:

            npart_tot = mpi.mpi_allreduce(npart)
            # each proc read for himself
            npart_all = None
            npart = npart                                              # local
            # each proc read for himself
            npart_read = None
            nbody_read = sum(npart)

            # each proc read for himself
            npart_m_read = None

            ngas = npart[0]
            ngas_read = ngas
            nstars = npart[1]
            nstars_read = nstars

            # compute nzero
            nzero = 0
            mass = np.array([])
            for i in range(len(npart)):
                if massarr[i] == 0:
                    nzero = nzero + npart[i]
                else:
                    mass = np.concatenate(
                        (mass, np.ones(npart[i]) * massarr[i]))

        nbody = sum(npart)
        nbody_tot = sum(npart_tot)

        if npart_m_read is not None:
            if sum(npart_m_read) != nzero:
                raise Exception(
                    "sum(npart_m) (%d) != nzero (%d)" %
                    (sum(npart_m_read), nzero), npart_m_read)

        ##########################################
        # read and send particles attribute
        ##########################################

        fields = self.compulsory_arrays(init=True)

        N = len(fields)
        for i in range(N):
            tpl = (4, np.int32)
            block_name = pnio.ReadBlock(f, tpl, byteorder=self.byteorder,
                                      pio=self.pio)
            name = block_name[0]

            # check if fields is known
            if name not in list(fields.keys()):
                raise IOError("Got %s but is undefined" % name)

            # get required data
            data = fields[name]
            obj = data[0]
            dtype = data[1]
            shape = data[2]

            # check if implemented
            if data[3] != all_particles:
                raise Exception("Not implemented")

            # compute real shape
            shape = [nbody_read, shape]

            # check if should skip block
            skip = False
            if self.skip_io_block(name):
                skip = True
                print(("skipping %s..." % name))

            # read block
            elif mpi.mpi_IsMaster():
                print(("reading %s..." % name))

            tmp = pnio.ReadDataBlock(f, dtype, shape=shape,
                                   byteorder=self.byteorder,
                                   pio=self.pio, npart=npart_read,
                                   skip=skip)

            if skip:
                continue

            # ensure right size
            if data[2] == 1:
                tmp = tmp.flatten()
            setattr(self, obj, tmp)

        # extentions

        fields = self.optional_arrays(init=True)

        while not pnio.end_of_file(f, pio=self.pio, MPI=MPI):
            tpl = (4, np.int32)
            block_name = pnio.ReadBlock(f, tpl, byteorder=self.byteorder,
                                      pio=self.pio)
            name = block_name[0]

            # check if fields is known
            if name not in list(fields.keys()):
                raise IOError("Got %s but is undefined" % name)

            # get required data
            data = fields[name]
            obj = data[0]
            dtype = data[1]
            shape = data[2]
            part_type = data[3]

            if part_type != all_particles:
                shape = [npart[part_type], shape]
            else:
                shape = [nbody_read, shape]

            # check if should skip block
            skip = False
            if self.skip_io_block(name):
                skip = True
                print(("skipping %s..." % name))

            # read block
            elif mpi.mpi_IsMaster():
                print(("reading %s..." % name))
            tmp = pnio.ReadDataBlock(f, dtype, shape=shape,
                                   byteorder=self.byteorder,
                                   pio=self.pio, npart=npart_read,
                                   skip=skip)

            if skip:
                continue

            if part_type != all_particles:
                before = nbody - sum(npart[part_type:])
                before = np.zeros((before, data[2]))
                after = nbody - sum(npart[:part_type + 1])
                after = np.zeros((after, data[2]))
                tmp = np.concatenate((before, tmp, after))
            if data[2] == 1:
                tmp = tmp.flatten()
            setattr(self, obj, tmp.astype(dtype))

        # make global
        self.npart = npart
        self.massarr = massarr

        self.atime = atime
        self.redshift = redshift
        self.flag_sfr = flag_sfr
        self.flag_feedback = flag_feedback
        self.nall = nall
        self.flag_cooling = flag_cooling
        self.num_files = num_files
        self.boxsize = boxsize
        self.omega0 = omega0
        self.omegalambda = omegalambda
        self.hubbleparam = hubbleparam
        self.flag_age = flag_age
        self.flag_metals = flag_metals
        self.nallhw = nallhw
        self.flag_entr_ic = flag_entr_ic
        self.empty = empty
        self.nbody = nbody

        self.tpe = np.array([], np.int32)
        for i in range(len(npart)):
            self.tpe = np.concatenate((self.tpe, np.ones(npart[i]) * i))

        self.nzero = nzero
        if isinstance(self.massarr, np.ndarray):
            self.massarr = self.massarr.tolist()
        if isinstance(self.nall, np.ndarray):
            self.nall = self.nall.tolist()
        if isinstance(self.nallhw, np.ndarray):
            self.nallhw = self.nallhw.tolist()

        self.comovingintegration = None
        self.initComovingIntegration()

    def write_particles(self, f):
        """
        specific format for particle file
        """

        # here, we must let the user decide if we creates
        # the mass block or not, event if all particles have the same mass
        massarr, nzero = self.get_massarr_and_nzero()

        if self.pio == 'yes':

            """
            here, we have to compute also
            mass for each proc
            """

            npart = self.npart
            nall = self.npart_tot
            num_files = mpi.NTask

            npart_write = None
            npart_m_write = None

        else:

            npart = self.npart
            nall = self.npart_tot
            npart_all = np.array(mpi.mpi_allgather(npart))
            num_files = 1

            npart_write = self.npart
            npart_m_write = np.array(self.npart) * \
                (np.array(self.massarr) == 0)

        massarr = np.zeros(nber_tpe)

        # header
        if sys.byteorder == self.byteorder:
            npart = np.array(npart, np.int32).tostring()
            massarr = np.array(massarr, float).tostring()
        else:
            npart = np.array(npart, np.int32).byteswap().tostring()
            massarr = np.array(massarr, float).byteswap().tostring()

        atime = self.atime
        redshift = self.redshift
        flag_sfr = self.flag_sfr
        flag_feedback = self.flag_feedback

        if sys.byteorder == self.byteorder:
            nall = np.array(nall, np.int32).tostring()
        else:
            nall = np.array(nall, np.int32).byteswap().tostring()

        flag_cooling = self.flag_cooling
        num_files = num_files
        boxsize = self.boxsize
        omega0 = self.omega0
        omegalambda = self.omegalambda
        hubbleparam = self.hubbleparam
        flag_age = self.flag_age
        flag_metals = self.flag_metals

        if sys.byteorder == self.byteorder:
            nallhw = np.array(self.nallhw, float).tostring()
        else:
            nallhw = np.array(self.nallhw, float).byteswap().tostring()

        flag_entr_ic = self.flag_entr_ic
        empty = self.empty

        # header
        tpl = (("HEAD", 4), (256 + 8, np.int32))
        pnio.WriteBlock(f, tpl, byteorder=self.byteorder)

        tpl = ((npart, 24), (massarr, 48), (atime, float), (redshift, float),
               (flag_sfr, np.int32), (flag_feedback, np.int32), (nall, 24),
               (flag_cooling, np.int32), (num_files, np.int32), (boxsize, float),
               (omega0, float), (omegalambda, float), (hubbleparam, float),
               (flag_age, np.int32), (flag_metals, np.int32), (nallhw, 24),
               (flag_entr_ic, np.int32), (empty, 60))
        pnio.WriteBlock(f, tpl, byteorder=self.byteorder)

        def write_fields(self, fields, optional):
            for (name, (obj, dtype, shape, part_type)) in list(fields.items()):
                field_exists = getattr(self, obj, None) is not None
                if self.skip_io_block(name):
                    print(("skipping %s..." % name))
                    continue

                if not field_exists and not optional:
                    raise Exception("Cannot find field:", obj)
                elif not field_exists:
                    continue

                if part_type != all_particles:
                    tmp = self.select(part_type)
                else:
                    tmp = self

                tmp = getattr(tmp, obj).astype(dtype)

                bytes_length = tmp.size * tmp.itemsize
                print(
                    ("writing block: {}  - {:4} -  length (bytes): {}".format(name, part_type, bytes_length)))
                tpl = ((name, 4), ((bytes_length + 8), np.int32))
                pnio.WriteBlock(f, tpl, byteorder=self.byteorder)

                pnio.WriteArray(f, tmp, byteorder=self.byteorder,
                              pio=self.pio, npart=npart_write)

        fields = self.compulsory_arrays()
        write_fields(self, fields, optional=False)

        tmp = self.optional_arrays()
        write_fields(self, tmp, optional=True)
