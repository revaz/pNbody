###########################################################################################
#  package:   pNbody
#  file:      bnbf.py
#  brief:
#  copyright: GPLv3
#             Copyright (C) 2019 EPFL (Ecole Polytechnique Federale de Lausanne)
#             LASTRO - Laboratory of Astrophysics of EPFL
#  author:    Yves Revaz <yves.revaz@epfl.ch>
#
# This file is part of pNbody.
###########################################################################################

##########################################################################
#
# BINARY CLASS
#
##########################################################################

import numpy as np
from pNbody import iofunc as pnio
from pNbody import mpiwrapper as mpi

class Nbody_bnbf:

    def _init_spec(self):
        pass

    def get_excluded_extension(self):
        """
        Return a list of file to avoid when extending the default class.
        """
        return []

    def get_read_fcts(self):
        return [self.read_particles]

    def get_write_fcts(self):
        return [self.write_particles]

    def get_mxntpe(self):
        return 6

    def get_default_spec_vars(self):
        """
        return specific variables default values for the class
        """
        return {'label': "0               ",
                'nbody': 0,
                'atime': 0.,
                'empty': 232 * ''}

    def read_particles(self, f):
        """
        read binary particle file
        """

        ##########################################
        # read the header and send it to each proc
        ##########################################

        tpl = (16, np.int32, np.float32, 232)
        header = pnio.ReadBlock(f, tpl, byteorder=self.byteorder, pio=self.pio)
        label, nbody, atime, empty = header

        ##########################################
        # read and send particles attribute
        ##########################################

        pos = pnio.ReadDataBlock(
            f,
            np.float32,
            shape=(
                nbody,
                3),
            byteorder=self.byteorder,
            pio=self.pio)
        vel = pnio.ReadDataBlock(
            f,
            np.float32,
            shape=(
                nbody,
                3),
            byteorder=self.byteorder,
            pio=self.pio)
        mass = pnio.ReadDataBlock(
            f,
            np.float32,
            shape=(
                nbody,
            ),
            byteorder=self.byteorder,
            pio=self.pio)

        ##########################################
        # make global
        ##########################################

        self.pos = pos
        self.vel = vel
        self.mass = mass

    def write_particles(self, f):
        """
        specific format for particle file
        """

        if self.pio == 'yes':
            nbody = self.nbody
        else:
            nbody = mpi.mpi_reduce(self.nbody)

        # header
        tpl = ((self.label, 16), (nbody, np.int32),
               (self.atime, np.np.float32), (self.empty, 232))
        pnio.WriteBlock(f, tpl, byteorder=self.byteorder)

        # positions
        pnio.WriteDataBlock(
            f,
            self.pos.astype(np.float32),
            byteorder=self.byteorder,
            pio=self.pio)
        # velocities
        pnio.WriteDataBlock(
            f,
            self.vel.astype(np.float32),
            byteorder=self.byteorder,
            pio=self.pio)
        # mass
        pnio.WriteArray(
            f,
            self.mass.astype(np.float32),
            byteorder=self.byteorder,
            pio=self.pio)

    def spec_info(self):
        """
        Write spec info
        """
        infolist = []
        infolist.append("")
        infolist.append("label               : %s" % self.label)
        infolist.append("atime               : %s" % self.atime)

        return infolist
