###########################################################################################
#  package:   pNbody
#  file:      simpleformat.py
#  brief:     a template for format files
#  copyright: GPLv3
#             Copyright (C) 2019 EPFL (Ecole Polytechnique Federale de Lausanne)
#             LASTRO - Laboratory of Astrophysics of EPFL
#  author:    Yves Revaz <yves.revaz@epfl.ch>
#
# This file is part of pNbody.
###########################################################################################


import numpy as np
from pNbody import iofunc as pnio
from pNbody import mpiwrapper as mpi
from pNbody import libutil


class Nbody_simpleformat:

    def _init_spec(self):
        pass

    def get_excluded_extension(self):
        """
        Return a list of file to avoid when extending the default class.
        """
        return []

    def get_read_fcts(self):
        return [self.read_particles]

    def get_write_fcts(self):
        return [self.write_particles]

    def get_mxntpe(self):
        return 6

    def get_default_spec_vars(self):
        """
        return specific variables default values for the class
        """
        return {'tnow': 0.,
                }

    def get_default_spec_array(self):
        """
        return specific array default values for the class
        """
        return {'meta': (0, np.float32),
                }

    def read_particles(self, f):
        """
        read binary particle file
        """

        ##########################################
        # read the header and send it to each proc
        ##########################################

        tpl = ('float32', np.int32, np.int32)
        header = pnio.ReadBlock(f, tpl, byteorder=self.byteorder, pio=self.pio)
        tnow, ngas, nstar = header

        nbody = ngas + nstar
        npart = [ngas, nstar, 0, 0, 0, 0]

        ##########################################
        # set nbody and npart
        ##########################################

        if self.pio == 'no':

            npart_tot = npart
            npart_all = libutil.get_npart_all(npart, mpi.mpi_NTask())
            npart = npart_all[mpi.mpi_ThisTask()]			# local
            npart_read = npart_tot
            nbody_read = sum(npart_read)
            ngas_read = npart_tot[0]

        else:

            npart_tot = mpi.mpi_reduce(npart)
            npart_all = None						# each proc read for himself
            npart = npart						# local
            npart_read = None						# each proc read for himself
            nbody_read = sum(npart)
            ngas_read = npart[0]

        #######################################################################
        # read and send particles position/velocities/mass/metallicity
        #######################################################################

        self.pos = pnio.ReadDataBlock(
            f,
            'float32',
            shape=(
                nbody_read,
                3),
            byteorder=self.byteorder,
            pio=self.pio,
            npart=npart_read)
        self.vel = pnio.ReadDataBlock(
            f,
            'float32',
            shape=(
                nbody_read,
                3),
            byteorder=self.byteorder,
            pio=self.pio,
            npart=npart_read)
        self.mass = pnio.ReadDataBlock(
            f,
            'float32',
            shape=(
                nbody_read,
            ),
            byteorder=self.byteorder,
            pio=self.pio,
            npart=npart_read)
        self.meta = pnio.ReadDataBlock(
            f,
            'float32',
            shape=(
                ngas_read,
            ),
            byteorder=self.byteorder,
            pio=self.pio,
            npart=None)
        self.meta = np.concatenate(
            (self.meta, np.zeros(nbody_read - ngas_read).astype(np.float32)))

        #self.pos = pnio.ReadArray(f,'float32',shape=(nbody,3),byteorder=self.byteorder,pio=self.pio,nlocal=npart_all)
        #self.vel = pnio.ReadArray(f,'float32',shape=(nbody,3),byteorder=self.byteorder,pio=self.pio,nlocal=npart_all)
        #self.mass= pnio.ReadArray(f,'float32',                byteorder=self.byteorder,pio=self.pio,nlocal=npart_all)
        #self.meta= pnio.ReadArray(f,'float32',                byteorder=self.byteorder,pio=self.pio,nlocal=npartgas_all)
        #self.meta  = concatenate((self.meta,zeros(nbody-npart[0]).astype(np.float32)))

        self.tpe = np.array([], np.int32)
        for i in range(len(npart)):
            self.tpe = np.concatenate((self.tpe, np.ones(npart[i]) * i))

    def write_particles(self, f):
        """
        specific format for particle file
        """
        npart_all = np.array(mpi.mpi_allgather(self.npart))

        if self.pio == 'yes':
            ngas = self.npart[0]
            nstar = self.npart[1]
        else:
            ngas = mpi.mpi_reduce(self.npart[0])
            nstar = mpi.mpi_reduce(self.npart[1])

        # header
        tpl = ((self.tnow, np.float32), (ngas, np.int32), (nstar, np.int32))
        pnio.WriteBlock(f, tpl, byteorder=self.byteorder)

        # positions
        pnio.WriteArray(
            f,
            self.pos.astype(np.float32),
            byteorder=self.byteorder,
            pio=self.pio,
            npart=None)
        pnio.WriteArray(
            f,
            self.vel.astype(np.float32),
            byteorder=self.byteorder,
            pio=self.pio,
            npart=None)
        pnio.WriteArray(
            f,
            self.mass.astype(np.float32),
            byteorder=self.byteorder,
            pio=self.pio,
            npart=None)
        pnio.WriteArray(f, self.meta[:self.npart[0]].astype(
            np.float32), byteorder=self.byteorder, pio=self.pio, npart=None)

    def spec_info(self):
        """
        Write spec info
        """
        infolist = []
        infolist.append("")
        infolist.append("tnow                : %s" % self.tnow)
        return infolist

    def Metallicity(self):
        """
        Return the metallicity
        """
        return self.meta
