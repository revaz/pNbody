#!/usr/bin/env python3

###########################################################################################
#  package:   pNbody
#  file:      setunits_2.py
#  brief:     Units conversion
#  copyright: GPLv3
#             Copyright (C) 2019 EPFL (Ecole Polytechnique Federale de Lausanne)
#             LASTRO - Laboratory of Astrophysics of EPFL
#  author:    Yves Revaz <yves.revaz@epfl.ch>
#
# 
#  This file is part of pNbody.
###########################################################################################


from pNbody import *

nb = Nbody("../snap.dat", ftype='gadget', unitsfile='params')
nb = Nbody("../snap.dat", ftype='gadget', unitsfile='unitsparameters')
