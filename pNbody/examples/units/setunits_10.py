#!/usr/bin/env python3

###########################################################################################
#  package:   pNbody
#  file:      setunits_10.py
#  brief:     Units conversion
#  copyright: GPLv3
#             Copyright (C) 2019 EPFL (Ecole Polytechnique Federale de Lausanne)
#             LASTRO - Laboratory of Astrophysics of EPFL
#  author:    Yves Revaz <yves.revaz@epfl.ch>
#
# 
#  This file is part of pNbody.
###########################################################################################

from pNbody import units, ctes
import numpy as np


# set the units
Unit_atom = ctes.PROTONMASS.into(units.cgs) * units.Unit_g
Unit_atom.set_symbol('atom')
SN_units = units.UnitSystem(
    'local', [units.Unit_cm, Unit_atom, units.Unit_Myr, units.Unit_K])

SN_units = units.UnitSystem(
    'local', [units.Unit_cm, Unit_atom, units.Unit_Myr, units.Unit_K])
cgs_units = units.cgs

E_51 = 1.


print((E_51 * cgs_units.convertionFactorTo(SN_units.UnitEnergy) * 1e51))
