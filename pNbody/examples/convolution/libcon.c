#include <Python.h>
#include <math.h>
#include <numpy/arrayobject.h>
#include <fftw3.h>


#include "libcon.h"


static PyObject *
      libcon_fftw_dft_r2c_1d(PyObject *self,PyObject *args)
      {
	  
          PyArrayObject *A = NULL;
	  
	  int N;
	  int i;
	  
          if (!PyArg_ParseTuple(args,"O",&A))
              return NULL;
	  
	  
	  N=A->dimensions[0];

 	  /* create the output */
	  npy_intp   ld[1];
	  PyArrayObject *Cr, *Ci;
	  ld[0] = A->dimensions[0];
	  Cr = (PyArrayObject *) PyArray_SimpleNew(1,ld,NPY_DOUBLE);
	  Ci = (PyArrayObject *) PyArray_SimpleNew(1,ld,NPY_DOUBLE);


          fftw_complex *out;
          fftw_plan p;
	  double rea,img;

          out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * (N/2+1));
          	  	    
	    
	  p = fftw_plan_dft_r2c_1d(N, (double *)A->data, out,FFTW_ESTIMATE|FFTW_DESTROY_INPUT);
	  fftw_execute(p);
	  
	  	 
	  /* get */
          for (i = 0; i < N; i++)
	    {
	      *(double*)(Cr->data + i*(Cr->strides[0])) = (double) out[i][0] ;
	      *(double*)(Ci->data + i*(Ci->strides[0])) = (double) out[i][1] ;
	    }
	  
          fftw_destroy_plan(p);
	  fftw_free(out);

	  
	  Py_BuildValue("(OO)",Cr,Ci);
      }               





static PyObject *
      libcon_convolution_1d(PyObject *self,PyObject *args)
      {
	  
          PyArrayObject *A = NULL;
	  PyArrayObject *B = NULL;
	  
	  int N;
	  int i;
	  
          if (!PyArg_ParseTuple(args,"OO",&A,&B))
              return NULL;
	  
	  /* check max size of matrix */
	  if (A->dimensions[0] != B->dimensions[0]){
	    PyErr_SetString(PyExc_ValueError,"size of arguement 1 must be the same than argument 2.");
	    return NULL;	  
	  }
	  
	  N=A->dimensions[0];

 	  /* create the output */
	  npy_intp   ld[1];
	  PyArrayObject *C;
	  ld[0] = A->dimensions[0];
	  C = (PyArrayObject *) PyArray_SimpleNew(1,ld,NPY_DOUBLE);


          //double *in1,*in2;
          fftw_complex *out1,*out2;
          fftw_plan p;
	  double rea,img;

          //in1  = (double *) malloc(sizeof(double) * N);
	  //in2  = (double *) malloc(sizeof(double) * N);
          out1 = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * (N/2+1));
	  out2 = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * (N/2+1));
          	  
	  /* put */
          //for (i = 0; i < N; i++)
	  //  {
	  //    in1[i] = *(double*)(A->data + i*(A->strides[0]));
	  //    in2[i] = *(double*)(B->data + i*(B->strides[0]));
          //  }
	    
	    

          //p = fftw_plan_dft_r2c_1d(N, in1, out1,FFTW_ESTIMATE|FFTW_DESTROY_INPUT);
	  p = fftw_plan_dft_r2c_1d(N, (double *)A->data, out1,FFTW_ESTIMATE|FFTW_DESTROY_INPUT);
	  fftw_execute(p);
	  
          //p = fftw_plan_dft_r2c_1d(N, in2, out1,FFTW_ESTIMATE|FFTW_DESTROY_INPUT);
          p = fftw_plan_dft_r2c_1d(N, (double *)B->data, out2,FFTW_ESTIMATE|FFTW_DESTROY_INPUT);
          fftw_execute(p);


	  /* multiply */
	  for (i = 0; i < N/2+1; i++)
	    {	   
	      rea = out1[i][0]*out2[i][0] - out1[i][1]*out2[i][1];
	      img = out1[i][0]*out2[i][1] + out1[i][1]*out2[i][0];  
	      out1[i][0] = rea/N;	/* we put here the factor */
	      out1[i][1] = img/N;	/* we put here the factor */	      	      
	    }
	  

          //p = fftw_plan_dft_c2r_1d(N, out1, in1,FFTW_ESTIMATE|FFTW_DESTROY_INPUT);
	  p = fftw_plan_dft_c2r_1d(N, out1, (double *)C->data,FFTW_ESTIMATE|FFTW_DESTROY_INPUT);
          fftw_execute(p);


	  	 
	  /* get */
          //for (i = 0; i < N; i++)
	  //  {
	  //    *(float*)(C->data + i*(C->strides[0])) = (float) in1[i]/N ;
	  //  }
	  
          fftw_destroy_plan(p);
          //free(in1); 
	  //free(in2); 
	  fftw_free(out1);
	  fftw_free(out2);

	  
	  
	  return PyArray_Return(C);
      }               



/*////////////////////////////////////////////////////

CUDA PART

////////////////////////////////////////////////////*/





static PyObject *
      libcon_fftw_dft_r2c_1dcu(PyObject *self,PyObject *args)
      {
          PyArrayObject *A = NULL;
	  
	  int N;
	  int i;
	  
          if (!PyArg_ParseTuple(args,"O",&A))
              return NULL;
	  	  
	  N=A->dimensions[0];

 	  /* create the output */
	  npy_intp   ld[1];
	  PyArrayObject *Cr,*Ci;
	  ld[0] = N;
	  Ci = (PyArrayObject *) PyArray_SimpleNew(1,ld,NPY_DOUBLE);
	  Cr = (PyArrayObject *) PyArray_SimpleNew(1,ld,NPY_DOUBLE);
          dev_fft_1dcu(N,(double*)A->data,(double*)Cr->data,(double*)Ci->data);

	  Py_BuildValue("(OO)",Cr,Ci);
      }               





static PyObject *
      libcon_convolution_1dcu(PyObject *self,PyObject *args)
      {
          PyArrayObject *A = NULL;
	  PyArrayObject *B = NULL;
	  
	  int N;
	  int i;
	  
          if (!PyArg_ParseTuple(args,"OO",&A,&B))
              return NULL;
	  
	  /* check max size of matrix */
	  if (A->dimensions[0] != B->dimensions[0]){
	    PyErr_SetString(PyExc_ValueError,"size of arguement 1 must be the same than argument 2.");
	    return NULL;	  
	  }
	  
	  N=A->dimensions[0];

 	  /* create the output */
	  npy_intp   ld[1];
	  PyArrayObject *C;
	  ld[0] = N;
	  C = (PyArrayObject *) PyArray_SimpleNew(1,ld,NPY_DOUBLE);

          dev_convolution_1dcu(N,(double*)A->data,(double*)B->data,(double*)C->data);
	 	  
	  return PyArray_Return(C);
      }               





static PyMethodDef libconMethods[] = {

          {"fftw_dft_r2c_1d",   libcon_fftw_dft_r2c_1d, METH_VARARGS,
           "fft 1d real to complex."},

          {"convolution_1d",   libcon_convolution_1d, METH_VARARGS,
           "performe convolution."},




          {"fftw_dft_r2c_1dcu",   libcon_fftw_dft_r2c_1dcu, METH_VARARGS,
           "fft 1d real to complex (cuda)."},

          {"convolution_1dcu",   libcon_convolution_1dcu, METH_VARARGS,
          "performe convolution (cuda)."},


          {NULL, NULL, 0, NULL}        /* Sentinel */
      };   
      
      
extern "C" void initlibcon(void)
      {    
          (void) Py_InitModule("libcon", libconMethods);	
	  
	  import_array();
      }      
      
