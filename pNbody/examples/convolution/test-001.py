#!/usr/bin/env python3

###########################################################################################
#  package:   pNbody
#  file:      test-001.py
#  brief:     Test 1D c convolution
#  copyright: GPLv3
#             Copyright (C) 2019 EPFL (Ecole Polytechnique Federale de Lausanne)
#             LASTRO - Laboratory of Astrophysics of EPFL
#  author:    Yves Revaz <yves.revaz@epfl.ch>
#
# This file is part of pNbody.
###########################################################################################

from numpy import *
from numpy import fft

import sys
import libcon


# do the fourrier transform
N = 18
random.seed(0)
G2 = random.random(N)
M2 = random.random(N)  # ones(N,float)

print('-->')
print(G2)
print(M2)
out = libcon.convolution_1d(G2, M2)
print('<--')
print(out)


##############
# print  fft.fft(G2)
