#!/usr/bin/env python3

###########################################################################################
#  package:   pNbody
#  file:      memory_info.py
#  brief:     Print the memory usage of pNbody
#  copyright: GPLv3
#             Copyright (C) 2019 EPFL (Ecole Polytechnique Federale de Lausanne)
#             LASTRO - Laboratory of Astrophysics of EPFL
#  author:    Yves Revaz <yves.revaz@epfl.ch>
#
#  This file is part of pNbody.
###########################################################################################

from numpy import *
from pNbody import *
from pNbody import ic

n = 1e6
nb = ic.box(n, 1, 1, 1)
nb.memory_info()


x = random.random(int(n))
y = random.random(int(n))
z = random.random(int(n))
pos = transpose(array([x, y, z]))

nb = Nbody(status='new', p_name='snap.dat', pos=pos)
nb.memory_info()
