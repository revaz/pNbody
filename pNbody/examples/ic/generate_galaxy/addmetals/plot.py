#!/usr/bin/env python3

###########################################################################################
#  package:   pNbody
#  file:      plot.py
#  brief:     Do a plot
#  copyright: GPLv3
#             Copyright (C) 2019 EPFL (Ecole Polytechnique Federale de Lausanne)
#             LASTRO - Laboratory of Astrophysics of EPFL
#  author:    Yves Revaz <yves.revaz@epfl.ch>
#
# This file is part of pNbody.
###########################################################################################



import Ptools as pt
import numpy as np
t = np.arange(-14, 0, 0.1)

#t0 = -5
#ts = 1.0
#x = np.exp( -(t-t0)**2 /ts**2 )


x = np.random.normal(-3, 1, 10000)
x = np.clip(x, -5, 0)


pt.hist(x)
pt.show()
