#!/usr/bin/env python3

###########################################################################################
#  package:   pNbody
#  file:      addmetals_for_gas.py
#  brief:     Add metals for gas
#  copyright: GPLv3
#             Copyright (C) 2019 EPFL (Ecole Polytechnique Federale de Lausanne)
#             LASTRO - Laboratory of Astrophysics of EPFL
#  author:    Yves Revaz <yves.revaz@epfl.ch>
#
# This file is part of pNbody.
###########################################################################################

from pNbody import *
from optparse import OptionParser


########################################
#
# parser
#
########################################


def parse_options():

    usage = "usage: %prog [options] file"
    parser = OptionParser(usage=usage)

    parser.add_option("-t",
                      action="store",
                      dest="ftype",
                      type="string",
                      default='gadget',
                      help="type of the file",
                      metavar=" TYPE")

    parser.add_option("-o",
                      action="store",
                      dest="outputfile",
                      type="string",
                      default=None,
                      help="outputfile name",
                      metavar=" STRING")

    parser.add_option("--exec",
                      action="store",
                      dest="execline",
                      type="string",
                      default=None,
                      help="give command to execute before",
                      metavar=" STRING")

    (options, args) = parser.parse_args()

    if len(args) == 0:
        print("you must specify a filename")
        sys.exit(0)
        file = None
    else:
        file = args

    return file, options


##########################################################################
#
#                                    MAIN
#
##########################################################################


file, opt = parse_options()

nb = Nbody(file, ftype=opt.ftype)

if opt.execline is not None:
    exec(opt.execline)


NELEMENTS = 5
nb.flag_metals = NELEMENTS
nb.rho = np.ones(nb.nbody).astype(np.float32)
nb.rsp = np.zeros(nb.nbody).astype(np.float32)
nb.metals = np.zeros((nb.nbody, NELEMENTS)).astype(np.float32)


# Fe
Fe = -1 * np.ones(nb.nbody).astype(np.float32)
nb.metals[:, 0] = nb.ChimieSolarMassAbundances['Fe'] * (10**Fe - 1.0e-20)


if opt.outputfile is not None:
    nb.rename(opt.outputfile)
    nb.write()
