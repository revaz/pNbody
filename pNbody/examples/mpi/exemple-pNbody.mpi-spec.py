#!/usr/bin/env python3

# -*- coding: iso-8859-1 -*-
###########################################################################################
#  package:   pNbody
#  file:      exemple-pNbody.mpi-spec.py
#  brief:     
#  copyright: GPLv3
#             Copyright (C) 2019 EPFL (Ecole Polytechnique Federale de Lausanne)
#             LASTRO - Laboratory of Astrophysics of EPFL
#  author:    Yves Revaz <yves.revaz@epfl.ch>
#
# 
#  This file is part of pNbody.
###########################################################################################


from pNbody import mpi
from numpy import *


######################
# sum/min/max/mean/len
######################

tag = "sum/min/max/mean/len"

n = 10
data = ones(n) * mpi.ThisTask

data_sum = mpi.mpi_sum(data)
data_min = mpi.mpi_min(data)
data_max = mpi.mpi_max(data)
data_mean = mpi.mpi_mean(data)
data_len = mpi.mpi_len(data)

print(tag, mpi.ThisTask, data_sum, data_min, data_max, data_mean, data_len)
mpi.mpi_barrier()

######################
# arange
######################

data = mpi.mpi_arange(5)

print(tag, mpi.ThisTask, data)
