
##########
# dm
##########
args[0]['scale'] = 'log'
args[0]['mn'] = 0
args[0]['mx'] = 0.00669977
args[0]['cd'] = 8.02918e-05

args[0]['palette'] = 'light'
args[0]['fc'] = 0.0   # amplitude : remove DM here


##########
# gas
##########
args[1]['scale'] = 'log'
args[1]['mn'] = 0
args[1]['mx'] = 0.000250947
args[1]['cd'] = 5.63937e-07

args[1]['palette'] = 'ocean'
args[1]['fc'] = 1.0
# args[1]['smode'] = "add"
# args[1]['qs2'] = 10
args[1]['smode'] = "alpha_gamma"
# Lower values makes the gas more transparent. A value of 7.5 to 10 is good.
args[1]['qs2'] = 7.5
args[1]['gamma'] = 1.0

##########
# stars
##########
args[2]['scale'] = 'log'
args[2]['mn'] = 0
args[2]['mx'] = 0.00437984
args[2]['cd'] = 2.60407e-06

args[2]['palette'] = 'sunburst'
args[2]['fc'] = 1.0
args[2]['smode'] = "alpha_gamma"
args[2]['qs2'] = -0.5
args[2]['gamma'] = 1.03
