################################
# film parameters
################################

#global film

film = {}
film['ftype']  = "swift"
film['time']   = "nb.atime"
film['exec']   = None
film['macro']  = None

film['format'] = 'png'
film['frames'] = []


#######################################################################
#
# first frame
#
#######################################################################

frame = {}
frame['width']       = 1280
frame['height']      = 720
frame['size']        = (88.88,50)
frame['tdir']        = None
frame['pfile']       = None
frame['exec']        = None
frame['macro']       = None
frame['components']  = []
frame['cargs']       = []
frame['view']        = "xy"

#######################################################################
#
# gaseous disk surface density (fo)
#
#######################################################################

component = {}
component['id']          = "gas"
component['exec']        = "nbfc.translate(-nbfc.boxsize/2)"
component['frsp']        = 2
component['rendering']   = 'map'

# add component to frame
frame['components'].append(component)


# add frame to the film
film['frames'].append(frame)



