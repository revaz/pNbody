#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import matplotlib.patches as patches
import argparse
import numpy as np
import os
import glob
from pNbody import *
from tqdm import tqdm

# %% Functions

description = """"
Add some text to images"""

epilog = """
Examples:
pnbmov_to_mpl  -o pngs_final --time_file times.npz  kpc pngs/*

--------

"""
parser = argparse.ArgumentParser(description=description, epilog=epilog)

parser.add_argument("files",
                    nargs="+",
                    type=str,
                    help="File name(s).")

parser.add_argument("-o",
                    action="store",
                    type=str,
                    dest="output_folder",
                    default="png_images_processed",
                    help="Name of the output folder.")

parser.add_argument("--time-file",
                    action="store",
                    type=str,
                    default=None,
                    help="File containing the simulation times.")


parser.add_argument("--scale",
                    action="store",
                    type=float,
                    dest="scale_length",
                    help="Scale length to display")

parser.add_argument("--scale_text",
                    action="store",
                    type=str,
                    dest="scale_text",
                    help="Scale text to display")


opt = parser.parse_args()






###########################
# Files and units
###########################

# Create the output directory
os.makedirs(opt.output_folder, exist_ok=True)

n_images = len(opt.files)

###########################
# Images
###########################

img = mpimg.imread(opt.files[0])

img_width  = img.shape[1]
img_height = img.shape[0]

dpi        = 100
fig_height = img_height / dpi
fig_width  = img_width  / dpi

figsize    = [fig_width,fig_height]

params = {
  "axes.labelsize": 14,
  "axes.titlesize": 18,
  "font.size": 12,
  "legend.fontsize": 12,
  "xtick.labelsize": 14,
  "ytick.labelsize": 14,
  "text.usetex": True,
  "figure.subplot.left": 0.0,
  "figure.subplot.right": 1.0,
  "figure.subplot.bottom": 0.0,
  "figure.subplot.top": 1.0,
  "figure.subplot.wspace": 0.,
  "figure.subplot.hspace": 0.,
  "figure.figsize" : figsize,
  "lines.markersize": 6,
  "lines.linewidth": 2.0,
}
plt.rcParams.update(params)

# read data time if provided
if opt.time_file:
  data_time = np.load(opt.time_file)
    
###########################
# Now compose your image
###########################
for i in tqdm(range(n_images)):

  filename = opt.files[i]  
  basename = os.path.basename(filename)
    
  img = mpimg.imread(filename)
  
  plt.figure(figsize=figsize) 
  ax  = plt.gca()  
  fig = plt.gcf()  
    
  #im = ax.imshow(img,  zorder=1, extent=[x_min, x_max, y_min, y_max], aspect='auto')
  im = ax.imshow(img, aspect='auto',extent=[0,img_width,0,img_height],zorder=1,alpha=1)

  # Remove the axis labels
  ax.axis('off')  # Hide axis and labels

  ##############
  # add timing
  ##############
  
  if opt.time_file:
    time = data_time["Time"][i]
    # position relative to the axes
    ax.text(0.90, 0.90, "t = {:.2f} Gyr".format(time),transform=ax.transAxes, fontsize=28, color='white',ha='center')



  opt.logo="pNbody-logo.png"
  if opt.logo is not None:
      logo = mpimg.imread(opt.logo)

      ax2 = fig.add_axes([0.825, 0, 0.15, 0.15], transform=ax.transAxes)  # (x, y, width, height)
      ax2.imshow(logo,alpha=0.9,zorder=10)
      ax2.axis('off')
      
      # ok, but hard to rescale
      #fig.figimage(logo, 0, 0, zorder=3, alpha=1)



  ax.text(0.25, 0.15, "dark matter", fontsize=28, color='white',ha='center',transform=ax.transAxes)
  ax.text(0.75, 0.15, "stars",       fontsize=28, color='white',ha='center',transform=ax.transAxes)


  # Finally save the image
  output_path = os.path.join(opt.output_folder, f"{basename}")
  plt.savefig(output_path)

  # close current figure   
  plt.close()
