################################
# film parameters
################################

#global film

film = {}
film['ftype']  = "swift"
film['time']   = "nb.atime"
film['exec']   = None
film['macro']  = None

film['format'] = 'png'
film['frames'] = []


#######################################################################
#
# first frame
#
#######################################################################

frame = {}
frame['width']       = 640
frame['height']      = 720
frame['size']        = (355,400)
frame['tdir']        = None
frame['pfile']       = None
frame['exec']        = None
frame['macro']       = None
frame['components']  = []
frame['cargs']       = []
frame['view']        = "xz"
frame['obs']         = None
frame['x0']          = None
frame['palette']     = 'blulut'

#######################################################################
#
# stars
#
#######################################################################

component = {}
component['id']          = "stars"
component['exec']        = "nbfc.translate(-nbfc.boxsize/2)"
component['frsp']        = 0
component['rendering']   = 'map'
component['palette']     = 'grey'

# add component to frame
frame['components'].append(component)


#######################################################################
#
# dm
#
#######################################################################

component = {}
component['id']          = "halo"
component['exec']        = "nbfc.translate(-nbfc.boxsize/2)"
component['frsp']        = 0
component['rendering']   = 'map'

# add component to frame
frame['components'].append(component)


# add frame to the film
film['frames'].append(frame)



