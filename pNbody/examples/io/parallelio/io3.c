/* example of parallel MPI write into separate files */ 
#include "mpi.h" 
#include <stdio.h> 
#define BUFSIZE 100 
 
int main(int argc, char *argv[]) 
{ 
    int i, myrank, buf[BUFSIZE]; 
    char filename[128]; 
    MPI_File myfile; 
 
    MPI_Init(&argc, &argv); 
    MPI_Comm_rank(MPI_COMM_WORLD, &myrank); 
    for (i=0; i<BUFSIZE; i++) 
        buf[i] = myrank * BUFSIZE + i; 
    sprintf(filename, "testfile.%d", myrank); 
    MPI_File_open(MPI_COMM_SELF, filename, 
		  MPI_MODE_WRONLY | MPI_MODE_CREATE, 
		  MPI_INFO_NULL, &myfile); 
    MPI_File_write(myfile, buf, BUFSIZE, MPI_INT, 
		   MPI_STATUS_IGNORE); 
    MPI_File_close(&myfile); 
    MPI_Finalize(); 
    return 0; 
} 
