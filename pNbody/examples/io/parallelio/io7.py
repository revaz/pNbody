#!/usr/bin/env python3


###########################################################################################
#  package:   pNbody
#  file:      io7.py
#  brief:     IO example
#  copyright: GPLv3
#             Copyright (C) 2019 EPFL (Ecole Polytechnique Federale de Lausanne)
#             LASTRO - Laboratory of Astrophysics of EPFL
#  author:    Yves Revaz <yves.revaz@epfl.ch>
#
# 
#  This file is part of pNbody.
###########################################################################################

from mpi4py import MPI
from numpy import *
import time


from optparse import OptionParser


def parse_options():
    usage = "usage: %prog [options] file"
    parser = OptionParser(usage=usage)

    parser.add_option("-o", "--outputfile",
                      action="store",
                      type="string",
                      dest="outputfile",
                      default="pytest-io7.dat",
                      help="output file name")

    parser.add_option("--DataSize",
                      action="store",
                      type="int",
                      dest="DataSize",
                      default=10,
                      help="Data size in Mb")

    (options, args) = parser.parse_args()

    files = args
    return files, options


files, opt = parse_options()


comm = MPI.COMM_WORLD
ThisTask = comm.Get_rank()
NTask = comm.Get_size()
Procnm = MPI.Get_processor_name()
Rank = ThisTask


print("ThisTask=%d" % ThisTask)

# set data
print(opt.DataSize)
DataSize = 1024 * 1024 * opt.DataSize  # 10 Mega
Nnumbers = int(DataSize / 8)
random.seed(ThisTask)
data = random.random(Nnumbers)
nbytes = data.nbytes

if ThisTask == 0:
    t1 = time.time()

filename = opt.outputfile

if ThisTask == 0:
    myfile = open(filename, 'w')
    savetxt(myfile, data)

    for Task in range(1, NTask):
        sdata = comm.recv(source=Task)
        savetxt(myfile, sdata)

    myfile.close()

else:
    comm.send(data, dest=0)

if ThisTask == 0:
    t2 = time.time()
    print("elapsed time : ", t2 - t1)
