#include "mpi.h" 
#include <stdio.h> 
 
/* example of parallel MPI write into a single file */ 
#define BUFSIZE 100 
int main(int argc, char *argv[]) 
{ 
    int i, myrank, buf[BUFSIZE]; 
    char filename[128]; 
    MPI_File thefile; 
    MPI_Offset offset; 
 
    MPI_Init(&argc, &argv); 
    MPI_Comm_rank(MPI_COMM_WORLD, &myrank); 
    sprintf(filename, "testfile"); 
    for (i = 0; i < BUFSIZE; i++) 
        buf[i] = myrank * BUFSIZE + i; 
    MPI_File_open(MPI_COMM_WORLD, filename, 
		  (MPI_MODE_WRONLY | MPI_MODE_CREATE), 
		  MPI_INFO_NULL, &thefile); 
    MPI_File_set_view(thefile, 0, MPI_INT, MPI_INT, "native", MPI_INFO_NULL); 
    offset = myrank * BUFSIZE; 
    MPI_File_write_at(thefile, offset, buf, BUFSIZE, MPI_INT, 
		      MPI_STATUS_IGNORE); 
    MPI_File_close(&thefile); 
    MPI_Finalize(); 
    return 0; 
} 
