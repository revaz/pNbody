Moving the observer around a fixed snapshot using gmkgmov
---------------------------------------------------------

In this exemple, the track followed by the camera is set using glups.
First, lunch glups to visualize one snapshot :

# glups ../data/treo4000.000.gad

Check that the projection mode is 1.
Set the desired view, and then, in the menu, set 
Menu >> GL Mode >> Auto-motion on

You can now guide the motion of the ovserver using the mouse and the + and - keys.
Record the track by pressing the F1 key. Once you have finished, press
again the F1 key. You can now quit glups. 

A direcory named track, containing a record of the track followed by
the observer has been created. 
We must now interpolated these files in as a function of the desired frame
needed for the movies. This is achived by the command :


../bin/gsplinetrk -s 0.1 -n 200 track/*.trk --dirname=newtrack00 --basename=trk 

The options -n sets the number of frames.
A new directory "newtrack00" containing the new 200 interpolated track files 
is created. 
You can eventually change the smoothing factor using the option -s.
You chan check the result of the interpolation by using the command :

glups --trk "newtrack00/*"  ../data/treo4000.000.gad


These files must now be converted into pNbody parameter files :

../bin/gtrk2nbd newtrack00/*.trk  --size "(512,288)" --eye='both'    --snapfiles="../data/treo4000.000.gad"

It is important here to give the name of the snapshot that will be used for the movie. 
Two directory "right" and "left" are created with the corresponding files.

If you need only one eye, simply type :
../bin/gtrk2nbd newtrack00/*.trk  --eye='default'    --snapfiles="../data/treo4000.000.gad"

The files are then stored in a directory called "center".

Now, it is time to setup the movie parameter file "filmparameter.py".
The movie is composed of 2 horizontal sub-movies, one for each eye.
The position of the observer corresponding to each eye, 
is now defind entirely by the files stored in the "right" and "left" directories. 
(param[1]['tdir'] = 'right',param[2]['tdir'] = 'left').
The other parameters may be chosen freely.




# create the polygons

../bin/mkpolygon.py --type sphere --cmd="""nb.pos = nb.pos*100""" sphere.dat
../bin/mkpolygon.py --type grid   --cmd="""nb.pos = nb.pos*400;nb.translate([-400,0,0])""" grid.dat

# create the fits

rm -rf fits png_*
gmkgmov -p filmparam.py --shape "(1024,576)" -d ~/pgm/python/pNbody-4.0/examples/movies/data/
../bin/gfits2png.py --dir png_right --params=fitsparam.py --comp1="fits/*0000-gas.fits" --comp2="fits/*0000-gas.fits" --comp3="fits/*0000-gas.fits" --comp4="fits/*0000-gas.fits"  --comp5="fits/*0002-grid.dat.fits"   --comp6="fits/*0000-gas.fits"
../bin/gfits2png.py --dir png_left  --params=fitsparam.py --comp1="fits/*0001-gas.fits" --comp2="fits/*0001-gas.fits" --comp3="fits/*0001-gas.fits" --comp4="fits/*0001-gas.fits"  --comp5="fits/*0003-grid.dat.fits"   --comp6="fits/*0000-gas.fits"

# combine left and right eyes

../bin/gwrapimages.py   png_left png_right -d png_g

# create the movie
gmov2mov --dir png_g  --only_film  -o film.avi






