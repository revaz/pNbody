#!/usr/bin/env python3

###########################################################################################
#  package:   pNbody
#  file:      filmparam.py
#  brief:     Parameter for a movie
#  copyright: GPLv3
#             Copyright (C) 2019 EPFL (Ecole Polytechnique Federale de Lausanne)
#             LASTRO - Laboratory of Astrophysics of EPFL
#  author:    Yves Revaz <yves.revaz@epfl.ch>
#
# 
#  This file is part of pNbody.
###########################################################################################
# nombre de sous film horiz et vertical
nh = 2  # horizontal
nw = 1  # vertical
# size of subfilms
width = 512
height = 512
# size of the film
numByte = width * nw
numLine = height * nh
# init parameters
param = initparams(nh, nw)


# image 1
param[1]['ftype'] = 'gadget'
param[1]['time'] = 'nb.atime'
param[1]['tdir'] = 'right'
param[1]['filter_name'] = None
param[1]['filter_opts'] = [10, 10, 2, 2]
param[1]['scale'] = 'log'
param[1]['mn'] = 0
param[1]['mx'] = 0
param[1]['cd'] = 0
param[1]['frsp'] = 0.

# image 2
param[2]['ftype'] = 'gadget'
param[2]['time'] = 'nb.atime'
param[2]['tdir'] = 'left'
param[2]['filter_name'] = None
param[2]['filter_opts'] = [10, 10, 2, 2]
param[2]['scale'] = 'log'
param[2]['mn'] = 0
param[2]['mx'] = 0
param[2]['cd'] = 0
param[2]['frsp'] = 0.
