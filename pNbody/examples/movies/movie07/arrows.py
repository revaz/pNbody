#!/usr/bin/env python3

###########################################################################################
#  package:   pNbody
#  file:      arrows.py
#  brief:
#  copyright: GPLv3
#             Copyright (C) 2019 EPFL (Ecole Polytechnique Federale de Lausanne)
#             LASTRO - Laboratory of Astrophysics of EPFL
#  author:    Yves Revaz <yves.revaz@epfl.ch>
#
# 
#  This file is part of pNbody.
###########################################################################################

nb = nb.reduc(128)


pos = zeros((2 * nb.nbody, 3))

k = 0
for i in range(nb.nbody):

    vnorm = sqrt(nb.vel[i, 0]**2 + nb.vel[i, 1]**2 + nb.vel[i, 2]**2)

    pos[k] = nb.pos[i]
    k = k + 1
    pos[k] = nb.pos[i] + nb.vel[i] / vnorm * 10
    k = k + 1


tnow = nb.tnow
nb = Nbody(status='new', pos=pos, ftype='gadget')
nb.tnow = tnow
