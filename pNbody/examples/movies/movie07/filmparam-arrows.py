#!/usr/bin/env python3

# -*- coding: utf-8 -*-
###########################################################################################
#  package:   pNbody
#  file:      filmparam-arrows.py
#  brief:     Parameter for a movie
#  copyright: GPLv3
#             Copyright (C) 2019 EPFL (Ecole Polytechnique Federale de Lausanne)
#             LASTRO - Laboratory of Astrophysics of EPFL
#  author:    Yves Revaz <yves.revaz@epfl.ch>
#
# 
#  This file is part of pNbody.
###########################################################################################

################################
# film parameters
################################

film = {}
film['ftype'] = "gadget"
film['time'] = "nb.atime"
film['exec'] = None
film['macro'] = 'arrows.py'
film['frames'] = []

######################
# a frame
######################

frame = {}
frame['width'] = 1000
frame['height'] = 625
frame['tdir'] = None
frame['pfile'] = "glparameters_right.nbd"
frame['exec'] = None
frame['macro'] = None
frame['components'] = []


# component 'total'
component = {}
component['id'] = 'all'
component['rendering'] = 'polygon2'
component['exec'] = None
component['macro'] = None
component['frsp'] = 0.
component['filter_name'] = None
component['filter_opts'] = None
frame['components'].append(component)


film['frames'].append(frame)

######################
# a frame
######################

frame = {}
frame['width'] = 1000
frame['height'] = 625
frame['tdir'] = None
frame['pfile'] = "glparameters_left.nbd"
frame['exec'] = None
frame['macro'] = None
frame['components'] = []


# component 'total'
component = {}
component['id'] = 'all'
component['rendering'] = 'polygon2'
component['exec'] = None
component['macro'] = None
component['frsp'] = 0.
component['filter_name'] = None
component['filter_opts'] = None
frame['components'].append(component)


film['frames'].append(frame)
