#!/usr/bin/env python3


###########################################################################################
#  package:   pNbody
#  file:      ImageModule.py
#  brief:     Describe the class ImageMaker
#  copyright: GPLv3
#             Copyright (C) 2019 EPFL (Ecole Polytechnique Federale de Lausanne)
#             LASTRO - Laboratory of Astrophysics of EPFL
#  author:    Yves Revaz <yves.revaz@epfl.ch>
#
# 
#  This file is part of pNbody.
###########################################################################################

from pNbody import ic
from pNbody import Nbody

import os


class ImageMaker():

    def __init__(self, directory='fits', bname='img', fmt='png'):
        self.directory = directory
        self.bname = bname
        self.counter = 0
        self.fmt = fmt

    # def dump(nb):
    #  nb = createnbodyfromvec(nb)
    #  nb.dumpimage(nb,self.directory,self.bname,self.conter)
    #  self.counter=self.counter+1

    def info(self):
        # print self.counter
        print("INFO INFO INFO INFO")
        print((self.counter))
        print("INFO INFO INFO INFO")

    def dump(self, dict):

        # set output file name
        file = "%s/%s%06d.%s" % (self.directory,
                                 self.bname, self.counter, self.fmt)
        print(("\nsaving %s\n" % file))

        # exctract dict
        pos = dict['pos']

        # create nbody object
        nb = Nbody(pos=pos, ftype='gadget')

        # create and save image
        nb.display(save=file, size=(1, 1))

        # increment counter
        self.counter = self.counter + 1
