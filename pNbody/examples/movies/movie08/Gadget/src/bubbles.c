#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <mpi.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_spline.h>

#include "allvars.h"
#include "proto.h"


#ifdef BUBBLES


/*! \file bubble.c
 *  \brief Init bubble
 *
*/


/*! compute bubble
 *
 */
void init_bubble()
{
  FILE *fd;
  int n,i;
  char line[58];
  float Time,D,R,E,A,B;
  
    
  fd = fopen(All.BubblesInitFile,"r");
  fscanf(fd, "# %6d\n", &n);

  /* allocate memory */
  All.BubblesTime = malloc(n * sizeof(double));
  All.BubblesD    = malloc(n * sizeof(double));
  All.BubblesR    = malloc(n * sizeof(double));
  All.BubblesE    = malloc(n * sizeof(double));
  All.BubblesA    = malloc(n * sizeof(double));
  All.BubblesB    = malloc(n * sizeof(double));
  
  /* read empty line */
  fgets(line, sizeof(line), fd);


  for (i=0;i<n;i++){
  
    fscanf(fd, "%f %f %f %f %f %f\n",&Time,&D,&R,&E,&A,&B);
        
    All.BubblesTime[i]  = (double)Time;
    All.BubblesD[i]	= (double)D; 
    All.BubblesR[i]	= (double)R; 
    All.BubblesE[i]	= (double)E; 
    All.BubblesA[i]	= (double)A; 
    All.BubblesB[i]	= (double)B;
    
    if(ThisTask == 0)
      printf("Time = %8.3f D=%8.3f R=%8.3f E=%8.3f A=%8.3f B=%8.3f\n",All.BubblesTime[i],All.BubblesD[i],All.BubblesR[i],All.BubblesE[i],All.BubblesA[i],All.BubblesB[i]);
    
  }
    
  fclose(fd);
  
  All.BubblesIndex = 0;

  /* set the right index */  
  for (i=0;i<n;i++){
    
    if (All.BubblesTime[All.BubblesIndex]<All.Time)
      All.BubblesIndex++;
    else
      break;
  
  }
  
  
#ifdef AGN_FEEDBACK	  
  All.BubblesTime[All.BubblesIndex]=1e30;
  if(ThisTask == 0)
    printf("Time for next bubble is not defined\n"); 
#else  
  if(ThisTask == 0)
    printf("Time for next bubble = %f (Tnow = %f)\n",All.BubblesTime[All.BubblesIndex],All.Time); 
#endif	    
  
 
  
}

/*! compute bubble
 *
 */
void make_bubble()
{


  if(All.Time>All.BubblesTime[All.BubblesIndex])
    {

      if(ThisTask == 0)
	{
	  printf("Its time to make a bubble.\n");
	}


      /* first, compute potential energy for all particles */										     
      compute_potential();	

      /* make first bubble */
      create_bubble(+1);
  
      /* make second bubble */
      create_bubble(-1);

      if(ThisTask == 0)
          printf("Two bubbles have been created.\n");
	
      /* move to the next bubble */
      All.BubblesIndex++;

#ifdef AGN_FEEDBACK
      /* ensure that the next bubble will not be lunched without agn_feedback authorisation */
      All.BubblesTime[All.BubblesIndex]= 1e30;
#else
      if(ThisTask == 0)
	  printf("Next bubble at t=%g.\n",All.BubblesTime[All.BubblesIndex]);
#endif


  
    }

}


/*! create bubble
 *
 */
void create_bubble(int sign)
{
  
  int i; 
  FLOAT x,y,z,r,ee;
  double D,R,A,B;
  double u_old=0,u_new=0;
  double alpha,delta;
  double xs,cs,ss;
  double UiInt,UiInt_sum,UeInt,UeInt_sum;
  double EiPot,EiPot_sum,EePot,EePot_sum;
  double Mi,Me,Mi_sum,Me_sum;
  double dEgyPot,dEgyInt;
  
  double a3inv=1.;			/* !!!!!!!!!!!! */

  {													     
      																	     
      /* 																     
         compute total energies and masses of particles that will becomes bubbles or not						     
      */																     
      																	     
      A = All.BubblesA[All.BubblesIndex];												     
      B = All.BubblesB[All.BubblesIndex];
      D = All.BubblesD[All.BubblesIndex];											     
      R = All.BubblesR[All.BubblesIndex]; 		
      																	     
      UiInt = UiInt_sum = UeInt = UeInt_sum = 0;											     
      EiPot = EiPot_sum = EePot = EePot_sum = 0;											     
      Mi    = Mi_sum    = Me    = Me_sum    = 0;											     
      																	     
      for(i = 0; i < NumPart; i++)													     
        {																     

#ifdef MULTIPHASE
          if((P[i].Type == 0) && (SphP[i].Phase == GAS_SPH))
#else	    
          if(P[i].Type == 0)	
#endif	    													     
            {   															     
              																     
              x = P[i].Pos[0];														     
              y = P[i].Pos[1];														     
              z = P[i].Pos[2];														     
              
              r = sqrt(x*x+y*y+z*z);
              
              /* (1) rotate the point */
        																     
              /* Rz(B) */														     
              cs = cos(B);														     
              ss = sin(B);														     
              xs = cs*x - ss*y; 													     
              y  = ss*x + cs*y; 													     
              x  = xs;															     
          																     
              /* Ry(A) */														     
              cs = cos(A);														     
              ss = sin(A);														     
              xs =  cs*x + ss*z;													     
              z  = -ss*x + cs*z;													     
              x  = xs;  	 													     
            																     
              /* (2) translate+- */													     
            																     											     
            																     
	      /* sign */
	      z = z-D*sign;	
	      r = sqrt(x*x+y*y+z*z);
        		      
#ifdef MULTIPHASE
              switch (SphP[i].Phase)													     
        	{															     
        	  case GAS_SPH: 													     
        	    /* egyspec from entropy */												     
        	    u_old = (1./GAMMA_MINUS1) * SphP[i].Entropy *pow(SphP[i].Density * a3inv, GAMMA_MINUS1);				     
        	    break;														     
        	  															     
        	  case GAS_STICKY:
		  case GAS_DARK:			    										     
        	    /* egyspec from specific energy */											     
        	    u_old = SphP[i].Entropy;												     
        	    break;														     
        	}															     

#else   																     
              u_old = (1./GAMMA_MINUS1) * SphP[i].Entropy *pow(SphP[i].Density * a3inv, GAMMA_MINUS1);					     
#endif        																   
        		    
              if (r<R)  		    /* the point is in the bubble */								     
        	{															     
        	  UiInt += u_old*P[i].Mass;												     
        	  EiPot += P[i].Potential*P[i].Mass;
        	  Mi	+= P[i].Mass;		
        	}   															     
              else			    /* the point is outside the bubble */
        	{
        	  ee = exp( (R-r)/(R*All.BubblesRadiusFactor) );
        	  UeInt += u_old*P[i].Mass*ee;												     
        	  EePot += P[i].Potential*P[i].Mass*ee;
        	  Me	+= P[i].Mass*ee;		  
        	}  
        	
        	   
            }																     
        																     
        																     
        }																     
        																     
        /* share results */														     
        MPI_Allreduce(&UiInt, &UiInt_sum,  1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);							     
        MPI_Allreduce(&EiPot, &EiPot_sum,  1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);							     
        MPI_Allreduce(&UeInt, &UeInt_sum,  1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);							     
        MPI_Allreduce(&EePot, &EePot_sum,  1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
        MPI_Allreduce(&Mi, &Mi_sum,  1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);							     
        MPI_Allreduce(&Me, &Me_sum,  1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);								     


        /* if Alpha and Delta are given */
        if ((All.BubblesAlpha!=0)&&(All.BubblesDelta!=0))
	  {
	    All.BubblesBeta = (1-All.BubblesDelta)* Mi_sum/Me_sum ; 
	    All.BubblesE[All.BubblesIndex] = -(All.BubblesDelta-1)*((Mi_sum/Me_sum)*EePot_sum -  EiPot_sum) + (All.BubblesAlpha-1)*(UiInt_sum + UeInt_sum);
          }
	else
	  {  
	    /*if (All.BubblesAlpha==0)*/	    /* compute BubblesAlpha from BubblesDelta */
	    if (1)				    /* !!! if alpha is computed once, it is no longer 0 !!! */
              {
	    	/* we can compute beta */												 
	    	All.BubblesBeta = (1-All.BubblesDelta)* Mi_sum/Me_sum ; 											 
        
            	/* now, we can compute alpha */ 											 
      	    	All.BubblesAlpha = (All.BubblesE[All.BubblesIndex] - (All.BubblesDelta-1)*EiPot_sum - All.BubblesBeta*EePot_sum)/(UiInt_sum + UeInt_sum) + 1; 
              }
            else				    /* compute BubblesDelta from BubblesAlpha */
	      {
	    	/* we can compute delta */
	    	All.BubblesDelta = ( (All.BubblesAlpha-1)*(UiInt_sum + UeInt_sum)- All.BubblesE[All.BubblesIndex])
	    			 / ( (Mi_sum/Me_sum)*EePot_sum -  EiPot_sum) + 1;
	    	
	    	/* now we can compute beta */													 
	    	All.BubblesBeta = (1-All.BubblesDelta)* Mi_sum/Me_sum ; 											 
              }
        
	    /*  
	    if(ThisTask == 0)															 
	      printf("==> UInt=%g; EiPot=%g; EePot=%g; Mi=%g; Me=%g\n",(UiInt_sum + UeInt_sum),EiPot_sum,EePot_sum,Mi_sum,Me_sum);  
            */
        
	  }
        																     
        /* count energy */
	All.EnergyBubbles -= All.BubblesE[All.BubblesIndex];																     
        																     
        if(ThisTask == 0)														     
          {
	    dEgyInt = (All.BubblesAlpha-1)*(UiInt_sum + UeInt_sum);
	    dEgyPot = (1-All.BubblesDelta)*((Mi_sum/Me_sum)*EePot_sum -  EiPot_sum);															     
            fprintf(FdBubble,"Step: %d, Time: %g, EgyBubble: %g, dEgyInt: %g, dEgyPot: %g, EgyInt: %g, Alpha: %g, Beta: %g, Delta: %g, D: %g, R: %g, A: %g, B: %g \n",
            All.NumCurrentTiStep,All.Time,All.BubblesE[All.BubblesIndex],dEgyInt,dEgyPot,(UiInt_sum + UeInt_sum),
	    All.BubblesAlpha,All.BubblesBeta,All.BubblesDelta,D,R,A,B);
            fflush(FdBubble);	   
          }																     
      
	  
      /* 																     
         now, apply Alpha,Beta,Delta						     
      */


      for(i = 0; i < NumPart; i++)
        {
#ifdef MULTIPHASE
          if((P[i].Type == 0) && (SphP[i].Phase == GAS_SPH))
#else          
	  if(P[i].Type == 0)	
#endif	  	      
            {  
	    
	      x = P[i].Pos[0];
	      y = P[i].Pos[1];
	      z = P[i].Pos[2];
	      
	      /* (1) rotate the point */
	    
	      /* Rz(B) */
              cs = cos(B);
              ss = sin(B);
	      xs = cs*x - ss*y;
	      y  = ss*x + cs*y;
	      x  = xs;
	      
	      /* Ry(A) */
              cs = cos(A);
              ss = sin(A);
	      xs =  cs*x + ss*z;
	      z  = -ss*x + cs*z;
	      x  = xs;	         
	      	    
	      /* (2) translate+- */
	    
	      D = All.BubblesD[All.BubblesIndex];
	      R = All.BubblesR[All.BubblesIndex]; 
	    
	      /* sign */
	      z = z-D*sign;	
	      r = sqrt(x*x+y*y+z*z);
	      	   

#ifdef MULTIPHASE
              switch (SphP[i].Phase)
                {
                  case GAS_SPH:
                    /* egyspec from entropy */
		    u_old = (1./GAMMA_MINUS1) * SphP[i].Entropy *pow(SphP[i].Density * a3inv, GAMMA_MINUS1);
                    break;
                  
                  case GAS_STICKY:
		  case GAS_DARK:			    
                    /* egyspec from specific energy */
	            u_old = SphP[i].Entropy;
                    break;
                }


#else	      
	      u_old = (1./GAMMA_MINUS1) * SphP[i].Entropy *pow(SphP[i].Density * a3inv, GAMMA_MINUS1);
#endif	      
	      
	      
	      if (r<R)			/* the point is in the bubble */
	        {
		  alpha = All.BubblesAlpha;
		  delta = All.BubblesDelta;		
	        }
	      else			/* the point is outside the bubble */
	        {
		  ee = exp( (R-r)/(R*All.BubblesRadiusFactor) );
		  alpha = (All.BubblesAlpha-1)*ee + 1;
		  delta =      All.BubblesBeta*ee + 1;	
		}	      

              u_new		    = (alpha/delta)  *u_old;
	      P[i].Mass 	   *= delta;
	      SphP[i].Density	   *= delta;



#ifdef MULTIPHASE
              switch (SphP[i].Phase)
                {
                  case GAS_SPH:
                    /* entropy from energy */
		    SphP[i].Entropy = GAMMA_MINUS1 * u_new /pow(SphP[i].Density * a3inv, GAMMA_MINUS1) ;
                    break;
                  
                  case GAS_STICKY:
		  case GAS_DARK:			    
                    /* egyspec from energy */
	            SphP[i].Entropy = u_new;
                    break;
                }

#else	      
	      SphP[i].Entropy = GAMMA_MINUS1 * u_new /pow(SphP[i].Density * a3inv, GAMMA_MINUS1) ;
#endif	      

	        	
	
            }

        }
	




  }

    	    	
}










#endif






