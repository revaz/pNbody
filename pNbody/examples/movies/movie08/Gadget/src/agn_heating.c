#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <mpi.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_spline.h>

#include "allvars.h"
#include "proto.h"


#ifdef AGN_HEATING


/*! \file agn_heating.c
 *  \brief Compute gas heating due to agn
 *
*/

static double hubble_a, a3inv;



void agn_heating()
  {
    

  int i;
  double SpecPower;
  double r,fm,fm_sum,pn,pn_sum;
    
  
  if(All.ComovingIntegrationOn)
    {
    
      hubble_a = All.Omega0 / (All.Time * All.Time * All.Time)
	+ (1 - All.Omega0 - All.OmegaLambda) / (All.Time * All.Time) + All.OmegaLambda;

      hubble_a = All.Hubble * sqrt(hubble_a);
    
      a3inv = 1 / (All.Time * All.Time * All.Time);
    }
  else
    a3inv = hubble_a = 1;
    
    
  /* here, we want to inject a power P */  
    
  /************************************/  
  /* first loop : determine SpecPower */  		/* here, we should take into account non active part */
  /************************************/  
    
  fm = 0;
  pn = 0;
    
  for(i = 0; i < NumPart; i++)
    {
      if(P[i].Ti_endstep == All.Ti_Current)
	{    

	  if(P[i].Type == 0)	/* gas */
	    {
	      r = sqrt( P[i].Pos[0]*P[i].Pos[0] + P[i].Pos[1]*P[i].Pos[1] + P[i].Pos[2]*P[i].Pos[2] );

	      if (r < All.AGNHeatingRmax)
                fm += HeatingRadialDependency(r)*P[i].Mass;
    
            }
	} 
      else
        {
	  if(P[i].Type == 0)	/* gas */
	    {
	      pn += -SphP[i].DtEgySpecAGNHeat*P[i].Mass;	/* power of non active particles */
	    }	  
	}	   
	    
    }  
    
    

  /* share results */
  MPI_Allreduce(&fm, &fm_sum,  1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  MPI_Allreduce(&pn, &pn_sum,  1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    
#ifdef USE_BONDI_POWER
  All.AGNHeatingPower = All.BondiPower;
#endif
  
  SpecPower = (All.AGNHeatingPower-pn_sum)/fm_sum ;
  
    
  /***************/    
  /* second loop */ 
  /***************/  
    
  for(i = 0; i < NumPart; i++)
    {
      if(P[i].Ti_endstep == All.Ti_Current)
	{    
	  if(P[i].Type == 0)	/* gas */
	    {
             
	      r = sqrt( P[i].Pos[0]*P[i].Pos[0] + P[i].Pos[1]*P[i].Pos[1] + P[i].Pos[2]*P[i].Pos[2] );
	      
	      if (r < All.AGNHeatingRmax)
	        {
		
		  /* first reset dtentropy */
		  SphP[i].DtEntropyAGNHeat = 0;
		  
                  /* first, we simply stop the radiative cooling */
		  //SphP[i].DtEntropyAGNHeat = -SphP[i].DtEntropyRadSph;
		  //SphP[i].DtEntropyRadSph = 0;
		
		  /* then, heat with agn*/
		  SphP[i].DtEntropyAGNHeat += GAMMA_MINUS1*pow(SphP[i].Density * a3inv,-GAMMA)*gamma_fct(SphP[i].Density *a3inv,r,SpecPower)/hubble_a;
		  SphP[i].DtEgySpecAGNHeat = - 1/GAMMA_MINUS1 * pow(SphP[i].Density * a3inv,GAMMA_MINUS1) * (SphP[i].DtEntropyAGNHeat);
		  		  
		  
#ifdef MULTIPHASE		
                  switch(SphP[i].Phase)
		    {
		      case GAS_SPH:		/* here we increase the entropy */
		        SphP[i].DtEntropy += SphP[i].DtEntropyAGNHeat;
		        break;
		      case GAS_STICKY:		/* here we increase the energy */
		      case GAS:DARK:
		      	SphP[i].DtEntropy += -SphP[i].DtEgySpecAGNHeat;	
			break;
		    }
#else
		  SphP[i].DtEntropy += SphP[i].DtEntropyAGNHeat;
#endif		  
		  
		}
	      else /* no heating */
	        {
                  SphP[i].DtEntropyAGNHeat = 0;		/* if the part is inactive ??? */
		}	        	  
		
            }
	}  
    }  
      
  
  }


/*! heating function
 *
 */
double gamma_fct(FLOAT density,double r, double SpecPower)
{
  double g;
  g = SpecPower * HeatingRadialDependency(r) * density; 
  return g;   
}




/*! heating radial dependency
 *
 */
double HeatingRadialDependency(double r)
{
  double f;
  /* radius dependency */
  //f = (1 - r/All.AGNHeatingRmax);
  //f = f*f; 
  
  if (r<All.AGNHeatingRmax)
    f = 1;
  else
    f = 0;   
  
  return f;   
}




#endif






