#!/usr/bin/env python3

###########################################################################################
#  package:   pNbody
#  file:      filmparam12.py
#  brief:     Parameter for a movie
#  copyright: GPLv3
#             Copyright (C) 2019 EPFL (Ecole Polytechnique Federale de Lausanne)
#             LASTRO - Laboratory of Astrophysics of EPFL
#  author:    Yves Revaz <yves.revaz@epfl.ch>
#
# 
#  This file is part of pNbody.
###########################################################################################
################################
# film parameters
################################

#global film

film = {}
film['ftype'] = "gadget"
film['time'] = "nb.atime"
film['exec'] = None
film['macro'] = None
film['frames'] = []


######################
# a frame
######################

frame = {}
frame['width'] = 512
frame['height'] = 512
frame['tdir'] = None
frame['pfile'] = 'glparameters-1_right.nbd'
frame['exec'] = None
frame['macro'] = None
frame['components'] = []


# component 'total'
component = {}
component['id'] = 'gas'
component['rendering'] = 'map'
component['exec'] = None
component['macro'] = None
component['frsp'] = 0.
frame['components'].append(component)

component = {}
component['id'] = 'halo'
component['rendering'] = 'map'
component['exec'] = None
component['macro'] = None
frame['components'].append(component)

component = {}
component['id'] = 'stars'
component['rendering'] = 'map'
component['exec'] = None
component['macro'] = None
frame['components'].append(component)
film['frames'].append(frame)


######################
# a frame
######################

frame = {}
frame['width'] = 512
frame['height'] = 512
frame['tdir'] = None
frame['pfile'] = 'glparameters-1_left.nbd'
frame['exec'] = None
frame['macro'] = None
frame['components'] = []


# component 'total'
component = {}
component['id'] = 'gas'
component['rendering'] = 'map'
component['exec'] = None
component['macro'] = None
component['frsp'] = 0.
frame['components'].append(component)

component = {}
component['id'] = 'halo'
component['rendering'] = 'map'
component['exec'] = None
component['macro'] = None
frame['components'].append(component)

component = {}
component['id'] = 'stars'
component['rendering'] = 'map'
component['exec'] = None
component['macro'] = None
frame['components'].append(component)
film['frames'].append(frame)
