#!/usr/bin/env python3

###########################################################################################
#  package:   pNbody
#  file:      filmparam31.py
#  brief:     Parameter for a movie
#  copyright: GPLv3
#             Copyright (C) 2019 EPFL (Ecole Polytechnique Federale de Lausanne)
#             LASTRO - Laboratory of Astrophysics of EPFL
#  author:    Yves Revaz <yves.revaz@epfl.ch>
#
# 
#  This file is part of pNbody.
###########################################################################################
################################
# film parameters
################################

#global film

film = {}
film['ftype'] = "gadget"
film['time'] = "nb.atime"
film['exec'] = None
film['macro'] = None
film['frames'] = []

######################
# other parameters
######################

film['timesteps'] = (0, 1, 0.1)
film['imdir'] = 'png3.1'
film['format'] = "png"


######################
# a frame
######################

frame = {}
frame['width'] = 512
frame['height'] = 512
frame['tdir'] = None
frame['pfile'] = None
frame['exec'] = None
frame['macro'] = None
frame['components'] = []


# component 'total'
component = {}
component['id'] = 'all'
component['rendering'] = 'map'
component['exec'] = None
component['macro'] = None
component['frsp'] = 0.
component['filter_name'] = 'gaussian'
component['filter_opts'] = [2, 2]
component['size'] = (2, 2)
frame['components'].append(component)


film['frames'].append(frame)
