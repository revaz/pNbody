#include <Python.h>
#include <numpy/arrayobject.h>
#include <math.h>


/* gadget stuff */

#define FLOAT float

int NumPart;
double Time=0;
double dTime=0.1;
double NextTime;


struct particle_data
{
  FLOAT Pos[3];
}
 *P;


void init_gadget()
  {
  
    size_t bytes;
    NumPart = 10000;
  
    
    if(!(P = malloc(bytes = NumPart * sizeof(struct particle_data))))
      {
    	printf("failed to allocate memory for `P' (%g MB).\n", bytes / (1024.0 * 1024.0));
      }
    
  }


void set_gadget()
  {
     int i;
     
     for (i=0;i<NumPart;i++)
       {
	 P[i].Pos[0] = (float)random()/(float)RAND_MAX ;
	 P[i].Pos[1] = (float)random()/(float)RAND_MAX ;
	 P[i].Pos[2] = (float)random()/(float)RAND_MAX ;	 
       }
       
  }





/* gadget stuff */



PyObject *ImageModule;
PyObject *ImageDict;
PyObject *ImageMaker;
PyObject *pyCreator;
PyObject *pyArgs;
PyObject *pyFile;
PyObject *pyNextTime;

PyArrayObject *Pos;	

 
void init_pnbody()
{

  /* initialize python and numpy */
  Py_Initialize();
  import_array();

  ImageModule = PyImport_ImportModule("Mkgmov");


  /* create the object */
  pyCreator = PyObject_GetAttrString(ImageModule, "Movie");
  
  /* file */
  pyFile = PyString_FromString("filmparam40.py");
  pyArgs = PyTuple_New(1);
  PyTuple_SetItem(pyArgs, 0, pyFile);
  
  //ImageMaker = PyObject_CallObject(pyCreator, NULL);
  ImageMaker = PyObject_CallObject(pyCreator, pyArgs);


  /* deallocate */
  Py_DECREF(pyCreator);
  Py_DECREF(pyArgs);
  Py_DECREF(pyFile);

  /* dictionary */
  ImageDict =PyDict_New();


  /* give some info */
  PyObject_CallMethod(ImageMaker, "info",NULL);

}




/*! finalize pNbody 
 *
 */
void finalize_pnbody()
{

 
  /* clean */
  Py_DECREF(ImageDict);
  Py_DECREF(ImageMaker);

  Py_Finalize();  
    
}






void compute_pnbody()
{


  /****************************************/
  /* check if it is time for the next image
  /****************************************/
   
  pyNextTime = PyObject_CallMethod(ImageMaker, "get_next_time",NULL);  

  if (!PyFloat_Check(pyNextTime))
    {
      printf("no more time left.\n");
      Py_DECREF(pyNextTime);
      return;
    }
      
  NextTime = PyFloat_AsDouble(pyNextTime);    
    
  printf("  Time=%g NextTime=%g\n",Time,NextTime);
  
  
  if (NextTime<=Time)
    {

      /* set next time */ 
      PyObject_CallMethod(ImageMaker, "set_next_time",NULL);

      /***********************************/
      /* create pos
      /***********************************/


      PyArrayObject *Pos;    

      int i;
      npy_intp ld[2];	
  
       
  
      ld[0]=NumPart;
      ld[1]=3;

      Pos = (PyArrayObject *) PyArray_SimpleNew(2,ld,NPY_FLOAT);
  
  
      for (i=0;i<NumPart;i++)
        {
          
          *(float*)(Pos->data + i*(Pos->strides[0]) + 0*Pos->strides[1]) = P[i].Pos[0] ;
          *(float*)(Pos->data + i*(Pos->strides[0]) + 1*Pos->strides[1]) = P[i].Pos[1] ;
          *(float*)(Pos->data + i*(Pos->strides[0]) + 2*Pos->strides[1]) = P[i].Pos[2] ;
        }      
  
  
  
      /***********************************/
      /* create image
      /***********************************/

  

      /* fill the dictionnary */
      PyDict_Clear(ImageDict);
      PyDict_SetItemString(ImageDict,"pos", (PyObject*)Pos);
      PyDict_SetItemString(ImageDict,"atime", PyFloat_FromDouble((double)Time));

      /* call the dump method */
      PyObject_CallMethod(ImageMaker, "dump","O",ImageDict);
  
  
      Py_DECREF(Pos);
  
  }
  
  Py_DECREF(pyNextTime);

}






int main(int argc, char *argv[])
  {
    int i;
    
    printf("init pNbody...\n");
    init_pnbody();
    printf("init pNbody done.\n");
    
    
    printf("init Gadget...\n");
    init_gadget();
    printf("init Gadget done.\n");   
    
    Time =0;
    dTime=0.1; 
    
    
    for (i=0;i<50;i++)
      {
        printf("\n* * * Time = %g * * *\n",Time);    
        
	//printf("  set_gadget...\n");
        set_gadget();
	//printf("  set_gadget done.\n");
      
        //printf("  start compute_pnbody...\n");
        compute_pnbody();
        //printf("  compute_pnbody done.\n");
	
	Time+=dTime;
      }
    
    printf("finalize pNbody...\n");
    finalize_pnbody();
    printf("finalize pNbody done.\n");

  }




