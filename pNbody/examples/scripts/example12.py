#!/usr/bin/env python3

###########################################################################################
#  package:   pNbody
#  file:      example12.py
#  brief:     Select stars
#  copyright: GPLv3
#             Copyright (C) 2019 EPFL (Ecole Polytechnique Federale de Lausanne)
#             LASTRO - Laboratory of Astrophysics of EPFL
#  author:    Yves Revaz <yves.revaz@epfl.ch>
#
# This file is part of pNbody.
###########################################################################################

from pNbody import *
from pNbody import ic

"""
select component
"""

nb = Nbody('snap.dat', ftype='gadget')
nb = nb.select(1)
nb.show(obs=None, view='yz', size=[50, 50])
