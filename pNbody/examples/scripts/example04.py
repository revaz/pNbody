#!/usr/bin/env python3

###########################################################################################
#  package:   pNbody
#  file:      example04.py
#  brief:     Create a pNbody object
#  copyright: GPLv3
#             Copyright (C) 2019 EPFL (Ecole Polytechnique Federale de Lausanne)
#             LASTRO - Laboratory of Astrophysics of EPFL
#  author:    Yves Revaz <yves.revaz@epfl.ch>
#
# This file is part of pNbody.
###########################################################################################

from pNbody import *
import numpy as np

pos = np.ones((10, 3), np.float32)
nb = Nbody(pos=pos, ftype='gadget')
nb.info()
