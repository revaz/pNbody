#!/usr/bin/env python3

###########################################################################################
#  package:   pNbody
#  file:      example11.py
#  brief:     Select particles based on position
#  copyright: GPLv3
#             Copyright (C) 2019 EPFL (Ecole Polytechnique Federale de Lausanne)
#             LASTRO - Laboratory of Astrophysics of EPFL
#  author:    Yves Revaz <yves.revaz@epfl.ch>
#
# This file is part of pNbody.
###########################################################################################


from pNbody import *
from pNbody import ic
from numpy import fabs

"""
select component
"""

nb = Nbody('snap.dat', ftype='gadget')
nb = nb.selectc(fabs(nb.x()) < 5)
nb.show(obs=None, view='xy', size=[50, 50])
