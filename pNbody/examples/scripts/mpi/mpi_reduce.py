#!/usr/bin/env python3

###########################################################################################
#  package:   pNbody
#  file:      mpi_reduce.py
#  brief:     Reduce data
#  copyright: GPLv3
#             Copyright (C) 2019 EPFL (Ecole Polytechnique Federale de Lausanne)
#             LASTRO - Laboratory of Astrophysics of EPFL
#  author:    Yves Revaz <yves.revaz@epfl.ch>
#
# This file is part of pNbody.
###########################################################################################

from pNbody import *

if mpi.mpi_IsMaster():
    x = 1
else:
    x = 2

x = mpi.mpi_reduce(x)
mpi.mpi_iprint(x)
