#!/usr/bin/env python3

###########################################################################################
#  package:   pNbody
#  file:      split.py
#  brief:     Write file in parallel => split
#  copyright: GPLv3
#             Copyright (C) 2019 EPFL (Ecole Polytechnique Federale de Lausanne)
#             LASTRO - Laboratory of Astrophysics of EPFL
#  author:    Yves Revaz <yves.revaz@epfl.ch>
#
# This file is part of pNbody.
###########################################################################################

import sys
from pNbody import *

files = sys.argv[1:]

for file in files:
    nb = Nbody(file, ftype='gadget')
    nb.set_pio('yes')  # enable parallel input/output
    nb.write()
