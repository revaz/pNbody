#!/usr/bin/env python3

###########################################################################################
#  package:   pNbody
#  file:      mpi_rprint.py
#  brief:     Print message from master only
#  copyright: GPLv3
#             Copyright (C) 2019 EPFL (Ecole Polytechnique Federale de Lausanne)
#             LASTRO - Laboratory of Astrophysics of EPFL
#  author:    Yves Revaz <yves.revaz@epfl.ch>
#
# This file is part of pNbody.
###########################################################################################

from pNbody import *
from pNbody import mpiwrapper as mpi
mpi.mpi_rprint("hi, I am the master !")
