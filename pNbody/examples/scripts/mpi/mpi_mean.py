#!/usr/bin/env python3

###########################################################################################
#  package:   pNbody
#  file:      mpi_mean.py
#  brief:     Compute the mean
#  copyright: GPLv3
#             Copyright (C) 2019 EPFL (Ecole Polytechnique Federale de Lausanne)
#             LASTRO - Laboratory of Astrophysics of EPFL
#  author:    Yves Revaz <yves.revaz@epfl.ch>
#
# This file is part of pNbody.
###########################################################################################



from pNbody import *
import numpy as np

if mpi.mpi_IsMaster():
    x = np.arange(0, 3).astype(float)
else:
    x = np.arange(3, 6).astype(float)

mpi.mpi_iprint(x)
mean = mpi.mpi_mean(x)

mpi.mpi_iprint("mean= %g " % mean)
