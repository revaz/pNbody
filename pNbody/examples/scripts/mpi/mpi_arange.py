#!/usr/bin/env python3

###########################################################################################
#  package:   pNbody
#  file:      mpi_np.arange.py
#  brief:     np.arange data
#  copyright: GPLv3
#             Copyright (C) 2019 EPFL (Ecole Polytechnique Federale de Lausanne)
#             LASTRO - Laboratory of Astrophysics of EPFL
#  author:    Yves Revaz <yves.revaz@epfl.ch>
#
# This file is part of pNbody.
###########################################################################################



from pNbody import *

x = mpi.mpi_np.arange(10)
mpi.mpi_iprint(x)
