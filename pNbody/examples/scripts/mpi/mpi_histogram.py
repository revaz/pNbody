#!/usr/bin/env python3

###########################################################################################
#  package:   pNbody
#  file:      mpi_histogram.py
#  brief:     Makes histogram
#  copyright: GPLv3
#             Copyright (C) 2019 EPFL (Ecole Polytechnique Federale de Lausanne)
#             LASTRO - Laboratory of Astrophysics of EPFL
#  author:    Yves Revaz <yves.revaz@epfl.ch>
#
# This file is part of pNbody.
###########################################################################################



from pNbody import *
from numpy import random

x = random.rand(10)


binx = np.arange(0, 1, 0.25)
h = mpi.mpi_histogram(x, binx)

mpi.mpi_iprint(x)
mpi.mpi_iprint(binx)
mpi.mpi_iprint(h)
