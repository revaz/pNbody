#!/usr/bin/env python3

###########################################################################################
#  package:   pNbody
#  file:      mpi_argfind.py
#  brief:     Find value in arrays
#  copyright: GPLv3
#             Copyright (C) 2019 EPFL (Ecole Polytechnique Federale de Lausanne)
#             LASTRO - Laboratory of Astrophysics of EPFL
#  author:    Yves Revaz <yves.revaz@epfl.ch>
#
# This file is part of pNbody.
###########################################################################################



from pNbody import *
import numpy as np

if mpi.mpi_IsMaster():
    x = np.array([3, 0, 4])
else:
    x = np.array([2, 5, 6])

mpi.mpi_iprint(x)
idx_min = mpi.mpi_argmin(x)
idx_max = mpi.mpi_argmax(x)
mpi.mpi_iprint(idx_min)
mpi.mpi_iprint(idx_max)
mn = mpi.mpi_getval(x, idx_min)
mx = mpi.mpi_getval(x, idx_max)
mpi.mpi_iprint(mn)
mpi.mpi_iprint(mx)
