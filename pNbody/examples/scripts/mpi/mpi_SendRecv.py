#!/usr/bin/env python3

###########################################################################################
#  package:   pNbody
#  file:      mpi_SendRecv.py
#  brief:     Send and receive messages
#  copyright: GPLv3
#             Copyright (C) 2019 EPFL (Ecole Polytechnique Federale de Lausanne)
#             LASTRO - Laboratory of Astrophysics of EPFL
#  author:    Yves Revaz <yves.revaz@epfl.ch>
#
# This file is part of pNbody.
###########################################################################################

from pNbody import *

if mpi.mpi_IsMaster():
    mpi.mpi_send(123, 1)
else:
    x = mpi.mpi_recv(0)
    msg = """[1] : get %d from node 0""" % x
    print(msg)
