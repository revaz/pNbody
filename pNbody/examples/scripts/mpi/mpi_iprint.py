#!/usr/bin/env python3

###########################################################################################
#  package:   pNbody
#  file:      mpi_iprint.py
#  brief:     Print with MPI
#  copyright: GPLv3
#             Copyright (C) 2019 EPFL (Ecole Polytechnique Federale de Lausanne)
#             LASTRO - Laboratory of Astrophysics of EPFL
#  author:    Yves Revaz <yves.revaz@epfl.ch>
#
# This file is part of pNbody.
###########################################################################################



from pNbody import *
mpi.mpi_iprint("hi !")
