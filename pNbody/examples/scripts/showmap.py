#!/usr/bin/env python3

###########################################################################################
#  package:   pNbody
#  file:      showmap.py
#  brief:     Display snapshot
#  copyright: GPLv3
#             Copyright (C) 2019 EPFL (Ecole Polytechnique Federale de Lausanne)
#             LASTRO - Laboratory of Astrophysics of EPFL
#  author:    Yves Revaz <yves.revaz@epfl.ch>
#
# This file is part of pNbody.
###########################################################################################

import sys
from pNbody import *

file = sys.argv[1]

nb = Nbody(file, ftype='gadget', pio='yes')
nb.display(size=(10000, 10000), shape=(256, 256), palette='light')
