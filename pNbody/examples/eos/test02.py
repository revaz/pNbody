#!/usr/bin/env python3

from pNbody import *
from pNbody import eos

nb = Nbody("/home/revaz/Snap/snapshot_0545.hdf5",ftype="gh5",verbose=0)
nb = nb.select(0)

T = nb.T()
print(T)

lu = nb.localsystem_of_units.to_unitSystem()
U = nb.U()*lu.UnitEnergySpec
T = eos.Temperature_fromEnergySpec(U)
print(T)

