#!/usr/bin/env python3

from astropy import units as u
from pNbody.units import unitSystem
from pNbody import eos

# default units
#lu = unitSystem()
lu = unitSystem(ULength=3.085678e21*u.cm,UMass=1.989e43*u.g,UVelocity=1e5*u.cm/u.s)

# define to specific energy, temperature and density
# (values taken from a simulation)
U0 = 5124.3013*lu.UnitEnergySpec
T0 = 244025.5990*lu.UnitTemperature
Rho0 = 2.8927514e-07*lu.UnitDensity

T = eos.Temperature_fromEnergySpec(U0)
print("Temperature",T)

U = eos.Energy_fromTemperature(T0)
print("Energy Spec",U.to(lu.UnitEnergySpec))

P = eos.Pressure_fromEnergySpec(Rho0,U0)
print("Pressure",P.to(lu.UnitPressure))

P = eos.Pressure_fromTemperature(Rho0,T0)
print("Pressure",P.to(lu.UnitPressure))

C = eos.SoundSpeed_fromEnergySpec(Rho0,U0)
print("Sound Speed",C.to(lu.UnitVelocity))

C = eos.SoundSpeed_fromTemperature(Rho0,T0)
print("Sound Speed",C.to(lu.UnitVelocity))

Jl = eos.JeansLength_fromEnergySpec(Rho0,U0)
print("Jeans Length",Jl.to(lu.UnitLength))

Jl = eos.JeansLength_fromTemperature(Rho0,T0)
print("Jeans Length",Jl.to(lu.UnitLength))

Jm = eos.JeansMass_fromEnergySpec(Rho0,U0)
print("Jeans Mass",Jm.to(lu.UnitMass))

Jm = eos.JeansMass_fromTemperature(Rho0,T0)
print("Jeans Mass",Jm.to(lu.UnitMass))

print()

U = eos.EnergySpec_fromSoundSpeed(Rho0,C)
print("Energy Spec (fromSoundSpeed)",U.to(lu.UnitEnergySpec))

Cs = eos.SoundSpeed_fromJeansMass(Rho0,Jm)
print("Sound Speed (fromJeansMass)",C.to(lu.UnitVelocity))

U = eos.EnergySpec_fromJeansMass(Rho0,Jm)
print("Energy Spec (fromJeansMass)",U.to(lu.UnitEnergySpec))

T = eos.Temperature_fromSoundSpeed(Rho0,C)
print("Temperature (fromSoundSpeed)",T)

T = eos.Temperature_fromJeansMass(Rho0,Jm)
print("Temperature (fromJeansMass)",T)


print()

# now change units

lu.setUnitVelocity(1*u.km/u.h)

U0 = 5124.3013*lu.UnitEnergySpec
T0 = 244025.5990*lu.UnitTemperature
Rho0 = 2.8927514e-07*lu.UnitDensity


C = eos.SoundSpeed_fromEnergySpec(Rho0,U0)
print("Sound Speed",C.to(lu.UnitVelocity))
print("Sound Speed",C.cgs)
print()

