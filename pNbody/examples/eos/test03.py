#!/usr/bin/env python3
"""
Compute the critical temperature for a fixed jeans mass
"""
from astropy import units as u
from astropy import constants as c

from pNbody.units import unitSystem
from pNbody import eos
import numpy as np

lu = unitSystem(ULength=3.085678e21*u.cm,UMass=1.989e43*u.g,UVelocity=1e5*u.cm/u.s)

rhos = 10**np.linspace(-7,2,100) * c.m_p/u.cm**3
Jm   = 1e5 * c.M_sun

NJ = 10 # number of particles required to resolve the jeans mass
T = eos.Temperature_fromJeansMass(rhos, Jm)*NJ**(2/3)
  

print(T)


