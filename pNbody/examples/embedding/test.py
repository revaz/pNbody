#!/usr/bin/env python3

###########################################################################################
#  package:   pNbody
#  file:      test.py
#  brief:     Test
#  copyright: GPLv3
#             Copyright (C) 2019 EPFL (Ecole Polytechnique Federale de Lausanne)
#             LASTRO - Laboratory of Astrophysics of EPFL
#  author:    Yves Revaz <yves.revaz@epfl.ch>
#
# This file is part of pNbody.
###########################################################################################


import mkimage
from numpy import random

Imgk = mkimage.ImageMaker()

pos = random.random((1000, 3)) * 10


Imgk.dump(pos)
Imgk.dump(pos)
Imgk.dump(pos)
