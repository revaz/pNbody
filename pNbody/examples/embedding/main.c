#include <Python.h>
#include <numpy/arrayobject.h>
#include <math.h>

int
main(int argc, char *argv[])
{





  Py_Initialize();
  import_array();







  /***************/
  PyArrayObject *pos;	 

  int N=1000;
  int i;
  npy_intp ld[2];   
  
   
  
  ld[0]=N;
  ld[1]=3;

  pos = (PyArrayObject *) PyArray_SimpleNew(2,ld,NPY_FLOAT);
  
  
  for (i=0;i<N;i++)
    {
      
      *(float*)(pos->data + i*(pos->strides[0]) + 0*pos->strides[1]) = (float)random()/(float)RAND_MAX ;
      *(float*)(pos->data + i*(pos->strides[0]) + 1*pos->strides[1]) = (float)random()/(float)RAND_MAX ;
      *(float*)(pos->data + i*(pos->strides[0]) + 2*pos->strides[1]) = (float)random()/(float)RAND_MAX ;
    }      
  
  
  
  /***************/

  printf("hello world 1\n");

  /* import a module */
  PyObject *pName,*pModule,*pFuncCreate,*pFuncDump;
  PyObject *pArgs, *pValue,*pList,*pNb;
  PyObject *pArgsDump;
  PyObject *pArgs3;

  PyObject *pDirectory,*pBname,*pNumber;
  PyObject *pImageMaker,*pImageMakerArgs;
  PyObject *PImageMakerObject;
  
		     
  /* import module from name */		      
  pName = PyUnicode_FromString("mkimage");
  pModule = PyImport_Import(pName);
  Py_DECREF(pName);

  printf("hello world 2\n");



  /* load the function */
  pFuncCreate = PyObject_GetAttrString(pModule, "createnbodyfromvec");	
 
  
  /* load the function */
  pFuncDump = PyObject_GetAttrString(pModule, "dumpimage");	  
  
  pArgs = PyTuple_New(1);
  PyTuple_SetItem(pArgs, 0, (PyObject*)pos);	

  /* call the function */	
  pNb = PyObject_CallObject(pFuncCreate, pArgs);
	
	
  /* dump the object */	
  pDirectory = PyUnicode_FromString("png");
  pBname     = PyUnicode_FromString("image");
  pNumber    = PyLong_FromLong(99);


  pArgsDump = PyTuple_New(4);
  PyTuple_SetItem(pArgsDump, 0, pNb);	
  PyTuple_SetItem(pArgsDump, 1, pDirectory);
  PyTuple_SetItem(pArgsDump, 2, pBname);
  PyTuple_SetItem(pArgsDump, 3, pNumber);
  PyObject_CallObject(pFuncDump, pArgsDump);


  /* try to create an object */
  pFuncCreate = PyObject_GetAttrString(pModule, "ImageMaker");	
  
  pImageMakerArgs = PyTuple_New(1);
  PyTuple_SetItem(pImageMakerArgs, 0, pDirectory);
  
  PImageMakerObject = PyObject_CallObject(pFuncCreate, pImageMakerArgs);

  /* example of method call */
  /* info                   */
  PyObject_CallMethod(PImageMakerObject, "info",NULL);


  /* dump                   */	
  PyObject_CallMethod(PImageMakerObject, "dump","O",pos);
  PyObject_CallMethod(PImageMakerObject, "dump","O",pos);
  PyObject_CallMethod(PImageMakerObject, "dump","O",pos);
  	
  PyObject_CallMethod(PImageMakerObject, "info",NULL);	
  
  
  
  
		     
  Py_Finalize();
  return 0;
}



