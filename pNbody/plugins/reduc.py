###########################################################################################
#  package:   pNbody
#  file:      reduc.py
#  brief:
#  copyright: GPLv3
#             Copyright (C) 2019 EPFL (Ecole Polytechnique Federale de Lausanne)
#             LASTRO - Laboratory of Astrophysics of EPFL
#  author:    Yves Revaz <yves.revaz@epfl.ch>
#
# This file is part of pNbody.
###########################################################################################
self.X = self.X.reduc(10)

# redisplay
self.display()
self.display_info(self.X)
