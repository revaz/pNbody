#!/usr/bin/env python3
###########################################################################################
#  package:   pNbody
#  file:      cutgmov
#  copyright: GPLv3
#             Copyright (C) 2019 EPFL (Ecole Polytechnique Federale de Lausanne)
#             LASTRO - Laboratory of Astrophysics of EPFL
#  author:    Yves Revaz <yves.revaz@epfl.ch>
#
# This file is part of pNbody.
###########################################################################################

import sys
import os
import string
import getopt
import math

from pNbody import Movie

##########################################################################


def version():
    ##########################################################################
    print('version 1.0')
    sys.exit(0)


##########################################################################
def help_message():
    ##########################################################################
    print("""Usage : cutgmov -f filmin -o filmout --n1=n1 --n2=n2

  This function allows to cut a movie.

  Options: -h        -- this help message
           -f 	     -- name of the input
	   -o	     -- name of the output
  	   --n1	     -- number of the first frame
	   --n2	     -- number of the last frame
	   --info    -- give info on the film
           --help    -- this help message
           --version -- displays version
""")
    sys.exit(0)


##########################################################################
# read film
##########################################################################

def list_info(films):

    for film in films:
        print(film.info())


##########################################################################
def check_arguments(options, xarguments):
    ##########################################################################

    n1 = 0
    n2 = 0
    output = "out.mov"
    input = None
    info = None

    for a in options[:]:
        if a[0] == '-h':
            help_message()

        if a[0] == '--help':
            help_message()

        if a[0] == '--version':
            version()

        if a[0] == '--info':
            info = 1

        if a[0] == '--n1':
            if a[1] == '':
                help_message()
            else:
                n1 = a[1]
                continue

        if a[0] == '--n2':
            if a[1] == '':
                help_message()
            else:
                n2 = a[1]
                continue

        if a[0] == '-o':
            if a[1] == '':
                help_message()
            else:
                output = a[1]
                continue

        if a[0] == '-f':
            if a[1] == '':
                help_message()
            else:
                input = a[1]
                continue

    return input, output, n1, n2, info


##########################################################################
#
#  MAIN
#
##########################################################################


try:
    options, xarguments = getopt.getopt(
        sys.argv[1:], 'o:f:h', ['info', 'help', 'version', 'n1=', 'n2=', 'info'])
except getopt.error:
    help_message()
    sys.exit(0)


# check arguments
film, output, n1, n2, info = check_arguments(options, xarguments)

n1 = int(n1)
n2 = int(n2)

# check that the film exists
if (os.path.exists(film) == 0):
    print("Error : the file ", film, " do no not exist.")
    sys.exit(0)

# open the file
fi = Movie.Movie(film)
fi.open(readall=1)
if n2 == 0:
    n2 = fi.npic

if info:
    fi.info()
    sys.exit()

# create the new movie

fo = Movie.Movie(output)
fo.new(fi.numByte, fi.numLine)

for i in range(fi.npic):

    data = fi.read_one()

    if i >= n2:
        fo.close()
        fi.close()
        sys.exit()

    if i >= n1:
        fo.write_pic(fi.current_time, data)

    print(i)
