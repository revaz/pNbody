#!/usr/bin/env python3
###########################################################################################
#  package:   pNbody
#  file:      gavi2mp4
#  copyright: GPLv3
#             Copyright (C) 2019 EPFL (Ecole Polytechnique Federale de Lausanne)
#             LASTRO - Laboratory of Astrophysics of EPFL
#  author:    Yves Revaz <yves.revaz@epfl.ch>
#
# This file is part of pNbody.
###########################################################################################

from optparse import OptionParser
import os
import sys
import string

##########################################################################


def parse_options():
    ##########################################################################

    usage = "usage: %prog [options] file"
    parser = OptionParser(usage=usage)

    parser.add_option("--fps",
                      action="store",
                      dest="fps",
                      type="int",
                      default=24,
                      help="frame per seconds")

    (options, args) = parser.parse_args()

    if len(args) == 0:
        print("you must specify at least a filename")
        sys.exit(0)

    return args, options


##########################################################################
#
#  MAIN
#
##########################################################################


files, opt = parse_options()


for file in files:
    print(file)

    # bname
    bname = os.path.splitext(file)[0]

    if os.path.exists("%s.h264" % (bname)):
        os.remove("%s.h264" % (bname))

    if os.path.exists("%s.mp4" % (bname)):
        os.remove("%s.mp4" % (bname))

    cmd = "mplayer %s.avi -dumpvideo -dumpfile %s.h264" % (bname, bname)
    os.system(cmd)

    cmd = "mp4creator -create=%s.h264 -r %d %s.mp4" % (bname, opt.fps, bname)
    os.system(cmd)

    cmd = "mp4creator -list %s.mp4" % (bname)
    os.system(cmd)

    os.remove("%s.h264" % (bname))
