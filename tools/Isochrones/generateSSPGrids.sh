#!/usr/bin/bash
###########################################################################################
#  package:   pNbody
#  file:      generateSSPGrids.sh
#  brief:     Generate grids of SSP properties like luminosities or magnitudes
#             Thoe files are stored in config/opt/filters/
#  copyright: GPLv3
#             Copyright (C) 2023 EPFL (Ecole Polytechnique Federale de Lausanne)
#             LASTRO - Laboratory of Astrophysics of EPFL
#  author:    Yves Revaz <yves.revaz@epfl.ch>
#
# This file is part of pNbody.
###########################################################################################




# fetch the BastI files
wget https://obswww.unige.ch/~revaz/DATA/pNbody/BastI.tar.gz
tar -xzf BastI.tar.gz

##############################
# GAIA
DB=./BastI/P04O1D1E1Y247_gaia-dr3/
isochrones_generate_magnitudes_grid  --rebinAges --M0 1e6 --filter_key G               -d $DB  -o GAIA_G_BastI_1e6.hdf5
isochrones_generate_magnitudes_grid  --rebinAges --M0 1e6 --filter_key G_BP            -d $DB  -o GAIA_G_BP_BastI_1e6.hdf5
isochrones_generate_magnitudes_grid  --rebinAges --M0 1e6 --filter_key G_RP            -d $DB  -o GAIA_G_RP_BastI_1e6.hdf5
isochrones_generate_magnitudes_grid  --rebinAges --M0 1e6 --luminosity_key 'log(L/Lo)' -d $DB  -o BolLum_BastI_1e6.hdf5

##############################
# SDSS
DB=./BastI/P04O1D1E1Y247_sloan/
isochrones_generate_magnitudes_grid  --rebinAges --M0 1e6 --filter_key u               -d $DB  -o SDSS_u_BastI_1e6.hdf5
isochrones_generate_magnitudes_grid  --rebinAges --M0 1e6 --filter_key g               -d $DB  -o SDSS_g_BastI_1e6.hdf5
isochrones_generate_magnitudes_grid  --rebinAges --M0 1e6 --filter_key r               -d $DB  -o SDSS_r_BastI_1e6.hdf5
isochrones_generate_magnitudes_grid  --rebinAges --M0 1e6 --filter_key i               -d $DB  -o SDSS_i_BastI_1e6.hdf5
isochrones_generate_magnitudes_grid  --rebinAges --M0 1e6 --filter_key z               -d $DB  -o SDSS_z_BastI_1e6.hdf5

##############################
# HST
DB=./BastI/P04O1D1E1Y247_hst_wfc3/
isochrones_generate_magnitudes_grid  --rebinAges --M0 1e6 --filter_key F475X           -d $DB  -o HST_F475X_BastI_1e6.hdf5

##############################
# EUCLID
DB=./BastI/P04O1D1E1Y247_euclid/
isochrones_generate_magnitudes_grid  --rebinAges --M0 1e6 --filter_key VIS             -d $DB  -o Euclid_VIS_BastI_1e6.hdf5
isochrones_generate_magnitudes_grid  --rebinAges --M0 1e6 --filter_key NISP_J          -d $DB  -o Euclid_J_BastI_1e6.hdf5
isochrones_generate_magnitudes_grid  --rebinAges --M0 1e6 --filter_key NISP_Y          -d $DB  -o Euclid_Y_BastI_1e6.hdf5
isochrones_generate_magnitudes_grid  --rebinAges --M0 1e6 --filter_key NISP_H          -d $DB  -o Euclid_H_BastI_1e6.hdf5

