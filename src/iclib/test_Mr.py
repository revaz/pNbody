#!/usr/bin/env python3
"""
 @package   pNbody
 @file      test_Mr.py
 @brief     Test
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
"""


from pNbody import *
import sys
from pNbody import iclib
import numpy as np

n = 10000

Rmax = 100.
rs = arange(0, Rmax, 1)
Mr = rs
rand = np.random.rand(n)
pos = iclib.generic_Mr(
    n,
    50,
    rs.astype(float32),
    Mr.astype(float32),
    rand,
    rand,
    rand,
    0)

pos = pos.astype(float32)


nb = Nbody('Mr.dat', ftype='gadget', pos=pos, status='new')
nb.npart = array([0, 0, n, 0, 0, 0])
nb.npart_tot = nb.npart
nb.write()
