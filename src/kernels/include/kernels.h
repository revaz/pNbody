
double Wcub_1D(double r, double h);
double Wqua_1D(double r, double h);
double Wqui_1D(double r, double h);

double Wcub_2D(double r, double h);
double Wqua_2D(double r, double h);
double Wqui_2D(double r, double h);

double Wcub_3D(double r, double h);
double Wqua_3D(double r, double h);
double Wqui_3D(double r, double h);
