#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "allvars.h"
#include "proto.h"


#ifdef PY_INTERFACE

/*! \file allocate.c
 *  \brief routines for allocating particle and tree storage
 */

/*! Allocates a number of small buffers and arrays, the largest one being
 *  the communication buffer. The communication buffer itself is mapped
 *  onto various tables used in the different parts of the force
 *  algorithms. We further allocate space for the top-level tree nodes, and
 *  auxiliary arrays for the domain decomposition algorithm.
 */
void allocate_commbuffersQ(void)
{
  size_t bytes;
  
  DomainStartListQ = malloc(NTask * sizeof(int));
  DomainEndListQ = malloc(NTask * sizeof(int));

  TopNodesQ = malloc(MAXTOPNODES * sizeof(struct topnode_data));

  DomainWorkQ = malloc(MAXTOPNODES * sizeof(double));
  DomainCountQ = malloc(MAXTOPNODES * sizeof(int));
  DomainCountSphQ = malloc(MAXTOPNODES * sizeof(int));
  DomainTaskQ = malloc(MAXTOPNODES * sizeof(int));


  All.BunchSizeDomain =
    (All.BufferSize * 1024 * 1024) / (sizeof(struct particle_data) + sizeof(struct sph_particle_data) +
				      sizeof(peanokey));

  if(All.BunchSizeDomain & 1)
    All.BunchSizeDomain -= 1;	/* make sure that All.BunchSizeDomain is even */


  
  if(!(CommBufferQ = malloc(bytes = All.BufferSize * 1024 * 1024)))
    {
      printf("failed to allocate memory for `CommBufferQ' (%g MB).\n", bytes / (1024.0 * 1024.0));
      endrun(2);
    }

  DomainPartBufQ = (struct particle_data *) CommBufferQ;
  DomainSphBufQ = (struct sph_particle_data *) (DomainPartBufQ + All.BunchSizeDomain);
  DomainKeyBufQ = (peanokey *) (DomainSphBufQ + All.BunchSizeDomain);



}


#endif
