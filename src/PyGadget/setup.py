#!/usr/bin/env python
"""
 @package   pNbody
 @file      setup.py
 @brief     Setup
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
"""

import os
import sys
import numpy

from distutils.core import setup, Extension
from distutils.sysconfig import get_python_inc
from distutils.sysconfig import get_python_lib


"""
In order to compile with mpi,

export CC=mpicc
python setup.py build


"""


incdir = os.path.join(get_python_inc(plat_specific=1), 'numpy')
libdir = os.path.join(get_python_lib(plat_specific=1), 'numpy')

gadget_files = ["src/main.c",
                "src/run.c",
                "src/predict.c",
                "src/begrun.c",
                "src/endrun.c",
                "src/global.c",
                "src/timestep.c",
                "src/init.c",
                "src/restart.c",
                "src/io.c",
                "src/accel.c",
                "src/read_ic.c",
                "src/ngb.c",
                "src/system.c",
                "src/allocate.c",
                "src/density.c",
                "src/gravtree.c",
                "src/hydra.c",
                "src/driftfac.c",
                "src/domain.c",
                "src/allvars.c",
                "src/potential.c",
                "src/forcetree.c",
                "src/peano.c",
                "src/gravtree_forcetest.c",
                "src/pm_periodic.c",
                "src/pm_nonperiodic.c",
                "src/longrange.c",
                "src/sph.c",
                "src/python_interface.c",
                "src/domainQ.c",
                "src/allocateQ.c"]

PYTHON_INC = "/usr/include/python3.6/"
MPI_DIR = "/usr/lib64/openmpi/lib/"
MPI_LIB = "mpi"
MPI_INC = "/usr/include/openmpi-x86_64/"


setup(
    name='PyGadget',
    version='0.0',
    description='Python Gadget Wrapping',
    author='Greg Ward',
    author_email='yves.revaz@epfl.ch',
    url='http://obswww.unige.ch/~revaz/pNbody',
    packages=['PyGadget'],
    ext_modules=[
        Extension(
            'PyGadget.gadget',
            gadget_files,
            include_dirs=[
                MPI_INC,
                "src/",
                PYTHON_INC,
                numpy.get_include()],
            define_macros=[
                ('PY_INTERFACE',
                 '1'),
                ('UNEQUALSOFTENINGS',
                 '1'),
                ('PERIODIC',
                 '1'),
                ('PEANOHILBERT',
                 '1'),
                ('NOGRAVITY',
                 1),
                ('ISOTHERM_EQS',
                 1)],
            library_dirs=[MPI_DIR],
            libraries=[
                'gsl',
                'gslcblas',
                'm',
                MPI_LIB])])
