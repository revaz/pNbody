#!/usr/bin/env python3
"""
 @package   pNbody
 @file      test.py
 @brief     Test cooling library
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
"""


import sys
from pNbody import coolinglib
import numpy as np

Redshift = 0.

Egyspec = np.array([0.4, 0.4, 0.4], dtype=np.float64)
Density = np.array([0.4, 0.4, 0.4], dtype=np.float64)
Mu, Lambda = coolinglib.cooling(Egyspec, Density, 0.76, Redshift)

print(Mu)
print(Lambda)

Temperature = np.array([10, 100, 1000], dtype=np.float64)
HydrogenDensity = np.array([1, 1, 1], dtype=np.float64)
Mu, Ne, Lambda = coolinglib.cooling_from_nH_and_T(
    Temperature, HydrogenDensity, 0.76, Redshift)

print()
print(Mu)
print(Ne)
print(Lambda)
