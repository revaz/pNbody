#!/usr/bin/env python3

import numpy as np
from astropy import units as u
from matplotlib import pylab as plt
from pNbody.Mockimgs import spectra


Zdic = {}
Zdic[1e-5] = "zem5" 
Zdic[1e-4] = "zem4" 
Zdic[1e-3] = "z001"
Zdic[2e-3] = "z002"
Zdic[3e-3] = "z003"
Zdic[4e-3] = "z004"
Zdic[6e-3] = "z006"
Zdic[8e-3] = "z008"
Zdic[1e-2] = "z010"
Zdic[2e-2] = "z020"
Zdic[3e-2] = "z030"
Zdic[4e-2] = "z040"

mass   = 1e6     # msol
age    = 14e9    # yr  12e9
Z      = 0.0002  # 0.0002


ns = np.arange(51)
ages = 10**(6+0.1*(ns-2))
Zs = np.array(list(Zdic.keys()))





idx_a = np.argmin(np.fabs(age-ages))
idx_m = np.argmin(np.fabs(Z-Zs))

idx_m = Zs[idx_m]

filename = "bpass_v2.3.a+02/spectra-bin-imf135_300.a+02.%s.dat"%Zdic[idx_m]
print(filename)
data = np.loadtxt(filename)

# wavelengths
lmbd = data[:,0]*u.AA            # in angstroms
sed  = data[:,idx_a]*u.L_sun/u.AA    # in Solar Luminosities per Angstrom
sed  = sed.to(u.erg/u.s/u.AA)        # in erg/s/AA
sed  = sed/1e6*mass                  # for one Msol 
sed  = sed.value

SED = spectra.SED(sed*u.erg/u.s/u.angstrom,lmbd*u.angstrom)
SED.resample()


# plot SED
plt.plot(SED.get_wavelength(),SED.get_sed())
plt.xlim(1e2,20000)
#plt.ylim(1e31,0.2e36)
plt.xlabel("wavelength [A]")
plt.show()



################################################
# filters
################################################
from pNbody.Mockimgs import filters

SDSSu = filters.FilterCl("../filters/SLOAN_SDSS.u.dat")
SDSSg = filters.FilterCl("../filters/SLOAN_SDSS.g.dat")
SDSSr = filters.FilterCl("../filters/SLOAN_SDSS.r.dat")
SDSSi = filters.FilterCl("../filters/SLOAN_SDSS.i.dat")
SDSSz = filters.FilterCl("../filters/SLOAN_SDSS.z.dat")

SDSSu.resample()
SDSSg.resample()
SDSSr.resample()
SDSSi.resample()
SDSSz.resample()


################################################
# compute the mean flux in each filters
################################################

SED.computeAbsoluteFlux()


Mu = SED.ABMagnitude(SDSSu)
Mg = SED.ABMagnitude(SDSSg)
Mr = SED.ABMagnitude(SDSSr)
Mi = SED.ABMagnitude(SDSSi)
Mz = SED.ABMagnitude(SDSSz)

print(Mu)
print(Mg)
print(Mr)
print(Mi)
print(Mz)


