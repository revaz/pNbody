#!/usr/bin/python3

import numpy as np
from pNbody.Mockimgs import filters
from pNbody.Mockimgs import spectra
from pNbody import iofunc

from astropy import constants as cte
from astropy import units as u
import argparse
import sys
from tqdm import tqdm

from matplotlib import pylab as plt
import os

####################################################################
# option parser
####################################################################

description="""Test pNbody filters of hdf5 format

"""

epilog     ="""
Examples:
--------
mockimgs_test_hdf5_filters BPASS23_SLOAN_SDSSg.hdf5 

"""

parser = argparse.ArgumentParser(description=description,epilog=epilog,formatter_class=argparse.RawDescriptionHelpFormatter)




parser.add_argument(action="store", 
                    dest="files", 
                    metavar='FILES', 
                    type=str,
                    default=None,
                    nargs='*',
                    help='the list of hdf5 filter files') 


parser.add_argument("--MH",
                    action="store", 
                    dest="MH", 
                    metavar='FLOAT', 
                    type=float,
                    default=-1,
                    help='metallicity [MH]') 


parser.add_argument("--Age",
                    action="store", 
                    dest="Age", 
                    metavar='FLOAT', 
                    type=float,
                    default=12,
                    help='age [Gyr]') 
                    
parser.add_argument("--Mass",
                    action="store", 
                    dest="Mass", 
                    metavar='FLOAT', 
                    type=float,
                    default=1e6,
                    help='mass [Msol]') 
                    


parser.add_argument("--Age_min",
                    action="store", 
                    dest="Age_min", 
                    metavar='FLOAT', 
                    type=float,
                    default=0.001,
                    help='age [Gyr]') 

parser.add_argument("--Age_max",
                    action="store", 
                    dest="Age_max", 
                    metavar='FLOAT', 
                    type=float,
                    default=14,
                    help='age [Gyr]')                     

parser.add_argument("--Age_nbins",
                    action="store", 
                    dest="Age_nbins", 
                    metavar='INT', 
                    type=int,
                    default=100,
                    help='number of age bins')    

parser.add_argument("--mode",
                    action="store", 
                    dest="mode", 
                    metavar='STRING', 
                    type=str,
                    default="diff",
                    help='mode : diff or normal')  
                    


params = {
    "axes.titlesize": 18,
    "font.size": 16,
    "axes.labelsize": 22,
    "legend.fontsize": 16,
    "xtick.labelsize": 16,
    "ytick.labelsize": 16,
    "text.usetex": True,
    "figure.subplot.left": 0.11,
    "figure.subplot.right": 0.95,
    "figure.subplot.bottom": 0.12,
    "figure.subplot.top": 0.9,
    "figure.subplot.wspace": 0.14,
    "figure.subplot.hspace": 0.12,
    "figure.figsize" : (12,10),
    "lines.markersize": 6,
    "lines.linewidth": 3.0,
}
plt.rcParams.update(params)


                    

                    
####################################################################
# main
####################################################################

from pNbody.Mockimgs import filters

def Do(opt):
  
  logAge_min = np.log10(opt.Age_min)
  logAge_max = np.log10(opt.Age_max)
  Ages = 10**np.linspace(logAge_min,logAge_max,opt.Age_nbins)
  
  fig, ax = plt.subplots()
  
  opt.mode=="diff"
  
  if opt.mode == "diff":

    # setup the filter
    filter_file = opt.files[0]
    print(filter_file)
    Filter = filters.Filter(filter_file,filter_type="hdf5")
      
    Mags_ref = np.zeros(len(Ages))
    for i,Age in enumerate(Ages):
      Mags_ref[i] = Filter.Magnitudes(opt.Mass, Age, MH=opt.MH)
    
    
    for filter_file in opt.files:
      
      # setup the filter
      print(filter_file)
      Filter = filters.Filter(filter_file,filter_type="hdf5")
      
      Mags = np.zeros(len(Ages))
      for i,Age in enumerate(Ages):
        Mags[i] = Filter.Magnitudes(opt.Mass, Age, MH=opt.MH) - Mags_ref[i]
          
      ax.plot(Ages,Mags,label=os.path.basename(filter_file))    

  
  
  else:

    for filter_file in opt.files:
      
      # setup the filter
      print(filter_file)
      Filter = filters.Filter(filter_file,filter_type="hdf5")
      
      Mags = np.zeros(len(Ages))
      for i,Age in enumerate(Ages):
        Mags[i] = Filter.Magnitudes(opt.Mass, Age, MH=opt.MH)
          
      ax.plot(Ages,Mags,label=os.path.basename(filter_file))
    

  
  # labels
  ax.set_xlabel("Ages [Gyr]")
  
  if opt.mode=="diff":
    ax.set_ylabel("Magnitude difference")
  else:
    ax.set_ylabel("Magnitude") 
  
  ax.legend()  
  plt.title("[MH]=%s"%opt.MH)
  
  plt.show()    


if __name__ == '__main__':
  
  opt = parser.parse_args()
  
  Do(opt)
 
