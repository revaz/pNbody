#!/usr/bin/env python3

import numpy as np
from astropy import units as u
from matplotlib import pylab as plt
from pNbody.Mockimgs import spectra
from pNbody.Mockimgs import filters

import argparse



####################################################################
# option parser
####################################################################

description="""Compute the luminosity of a SSP (bolometric or in a given set of filters) assuming a given mass, metallicity and age"""

epilog     ="""
Examples:
--------
get_Luminosity_from_SED.py --sed_files bpass_v2.3.a+02/spectra-bin-*.dat --Age 0.1 --MH -1 --Mass 1 ../filters/SLOAN_SDSS.*.dat

"""

parser = argparse.ArgumentParser(description=description,epilog=epilog,formatter_class=argparse.RawDescriptionHelpFormatter)


parser.add_argument("--sed_files",
                    action="store", 
                    dest="sed_files", 
                    metavar='FILENAMES', 
                    type=str,
                    default=None,
                    nargs='*',
                    help='the list of sed files') 
                    
parser.add_argument("--MH",
                    action="store", 
                    dest="MH", 
                    metavar='FLOAT', 
                    type=float,
                    default=0,
                    help='[M/H]')                     
                    
parser.add_argument("--Age",
                    action="store", 
                    dest="Age", 
                    metavar='FLOAT', 
                    type=float,
                    default=4.5,
                    help='Age in Gyr')    

parser.add_argument("--Mass",
                    action="store", 
                    dest="Mass", 
                    metavar='FLOAT', 
                    type=float,
                    default=1,
                    help='Mass in Msol')   
                    
parser.add_argument(action="store", 
                    dest="files", 
                    metavar='FILE', 
                    type=str,
                    default=None,
                    nargs='*',
                    help='a list of filters')     
                    
                                        

def Read_BPASS_SED(files):
  """
  read and SED from BPASS files
  """
  
  # metallicity - name conversion dictionary
  Zdic = {}
  Zdic[1.0e-5] = "zem5" 
  Zdic[1.0e-4] = "zem4" 
  Zdic[1.0e-3] = "z001"
  Zdic[2.0e-3] = "z002"
  Zdic[3.0e-3] = "z003"
  Zdic[4.0e-3] = "z004"
  Zdic[6.0e-3] = "z006"
  Zdic[8.0e-3] = "z008"
  Zdic[1.0e-2] = "z010"
  Zdic[1.4e-2] = "z014"
  Zdic[2.0e-2] = "z020"
  Zdic[3.0e-2] = "z030"
  Zdic[4.0e-2] = "z040"
  
  # load the first file to get some info
  data = np.loadtxt(files[0])
  
  
  
  # number of bins in Z
  nZ   = len(Zdic)
  # number of bins in Age
  nAge = data.shape[1] - 1
  # number of bins in wavelength
  nl   = len(data[:,0])
  
  # create an empty sed
  sed = np.zeros((nl,nZ,nAge)) 
  
  # create   
  ages   = 10**(6+0.1*(np.arange(nAge)))
  metals = np.array(list(Zdic.keys()))
  lmbd   = data[:,0]
  
  # ratio, initial mass, final mass 
  # this is a very crude approximation
  MiniMfin = np.ones((nZ,nAge))*0.75
  
  
  # loop over all files, i.e, over metallicities:
  
  for f in files:
    # find the metallicity
    for Z,string in Zdic.items():
      if f.find(string)>-1:
        break
    
    # find the metallicity index
    Z_idx = list(Zdic.keys()).index(Z)
    
    # read the file
    print("%2d %8.3f %s"%(Z_idx, Z, f))
    data = np.loadtxt(f)
        
    # copy at the right place
    sed[:,Z_idx,:] = data[:,1:]

  
  # units conversion
  sed  = sed*u.L_sun/u.AA              # in Solar Luminosities per Angstrom
  sed  = sed.to(u.erg/u.s/u.AA)        # in erg/s/AA
  sed  = sed/1e6                       # for one Msol 
  sed  = sed.value
  

  print("Done with BPASS files.")

  return ages,metals,lmbd,sed,MiniMfin  
    
    
####################################################################
# main
####################################################################

def Do(opt):
    

  binsAge,binsZ,lmbd,sed,MiniMfin = Read_BPASS_SED(opt.sed_files)  
  # turn Z to MH : 0.02 is the solar abundance also used in the fits file
  binsMH = np.log10(binsZ/0.02)   
  
  # Sun properties
  mass   = opt.Mass       # msol
  age    = opt.Age*1e9    # in years
  #Z      = 0.02    # 0.0002
  MH     = opt.MH
  
  
  i = np.argmin(np.fabs(age-binsAge))
  #j = np.argmin(np.fabs(Z-binsZ))
  j = np.argmin(np.fabs(MH-binsMH))
  
  
  # get the SED per unit of solar mass
  sedM =  sed[:,j,i] * mass
  SED = spectra.SED(sedM*u.erg/u.s/u.angstrom,lmbd*u.angstrom)
  SED.resample()
  
  
  
  
  # plot SED
  SED.computeAbsoluteFlux()
  plt.plot(SED.get_wavelength(),SED.get_sed())
  plt.xlim(1e2,20000)
  #plt.ylim(1e31,0.2e36)
  plt.xlabel("wavelength [A]")
  plt.ylabel("SED [erg/s/A]")
  plt.show()
  
  
  # compute the bolometric luminosity
  I = SED.BolometricLuminosity()  # erg/s
  L = I.to(u.L_sun).value         # Luminosity in Lsun
  ML = mass/L
  
  
  print("Bolometric")
  print("Luminosity :",I)
  print("Luminosity :",L*u.L_sun)
  print("M/L        :",ML)
  
  
  for filter_file in opt.files:
  
    # compute the luminosity in a given filter
    f = filters.FilterCl(filter_file)
    f.resample()
  
    I = SED.Luminosity(f)
    L = I.to(u.L_sun).value          # Luminosity in Lsun
    ML = mass/L  
    
    print()
    print(filter_file)
    print("Luminosity :",I)
    print("Luminosity :",L*u.L_sun)
    print("M/L        :",ML)  
    


if __name__ == '__main__':
  
  opt = parser.parse_args()
  
  Do(opt)




