#!/bin/bash

wget https://obswww.unige.ch/~revaz/DATA/pNbody/BPASS/bpass_v2.2.1.tar.gz
tar -xzf bpass_v2.2.1.tar.gz

wget https://obswww.unige.ch/~revaz/DATA/pNbody/BPASS/bpass_v2.3.a+02.tar.gz
tar -xzf bpass_v2.3.a+02.tar.gz
