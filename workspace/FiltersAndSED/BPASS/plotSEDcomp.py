#!/usr/bin/python3

import numpy as np
from pNbody.Mockimgs import filters
from pNbody.Mockimgs import spectra
from pNbody import iofunc

from astropy import constants as cte
from astropy import units as u
import argparse
import sys
from tqdm import tqdm



####################################################################
# option parser
####################################################################

description="""Generate a grid of magnitude. The grid is a 2d matrix discretized in [M/H] and ages. 
It is actually stored as an hdf5 file.
The script takes as input a fits file containing a set of Spectral Energy Distribution (SED) for a given
age and metallicity.

The current SED used is the one used by default by SKIRT and described here:
https://skirt.ugent.be/skirt9/class_starburst99_s_e_d_family.html

The file my be downloaded from the following link:
https://bitbucket.org/lutorm/sunrise/downloads/Patrik-imfKroupa-Zmulti-ml.fits.gz

The IMF used (Kroupa 2001) is parametrized by
IMF_exponent = '1.3,2.3 ' / Kroupa                                     
IMF_limits = '.1,.5,100' / Kroupa   



The magnitudes stored in the output file are in AB system.

"""

epilog     ="""
Examples:
--------

sed_generate_magnitudes_grid  --M0 1e6 --filter SDSSr.txt  --sed Patrik-imfKroupa-Zmulti-ml.fits -o SB99_SDSSr_1e6.hdf5
"""

parser = argparse.ArgumentParser(description=description,epilog=epilog,formatter_class=argparse.RawDescriptionHelpFormatter)


parser.add_argument("--sed",
                    action="store", 
                    dest="sed", 
                    metavar='FILENAME', 
                    type=str,
                    default=None,
                    help='the name of sed database') 

parser.add_argument("--sed_files",
                    action="store", 
                    dest="sed_files", 
                    metavar='FILENAMES', 
                    type=str,
                    default=None,
                    nargs='*',
                    help='the list of sed files') 


parser.add_argument("--filter",
                    action="store", 
                    dest="filter", 
                    metavar='FILENAME', 
                    type=str,
                    default=None,
                    help='the name of the filter transmission file (transmission vs wavelength in angstrom)') 


parser.add_argument("-o",
                    action="store",
                    type=str,
                    dest="outputfilename",
                    default=None,
                    help="Name of the output file")  

                

parser.add_argument("--info",
                    action="store_true", 
                    dest="info", 
                    default=False,
                    help='get info (list of keys) and exit.')  
                    

def Read_BPASS_SED(files,plt):
  """
  read and SED from BPASS files
  """
  
  # metallicity - name conversion dictionary
  Zdic = {}
  Zdic[1.0e-5] = "zem5" 
  Zdic[1.0e-4] = "zem4" 
  Zdic[1.0e-3] = "z001"
  Zdic[2.0e-3] = "z002"
  Zdic[3.0e-3] = "z003"
  Zdic[4.0e-3] = "z004"
  Zdic[6.0e-3] = "z006"
  Zdic[8.0e-3] = "z008"
  Zdic[1.0e-2] = "z010"
  Zdic[1.4e-2] = "z014"
  Zdic[2.0e-2] = "z020"
  Zdic[3.0e-2] = "z030"
  Zdic[4.0e-2] = "z040"
  
  # load the first file to get some info
  data = np.loadtxt(files[0])
  
  
  
  # number of bins in Z
  nZ   = len(Zdic)
  # number of bins in Age
  nAge = data.shape[1] - 1
  # number of bins in wavelength
  nl   = len(data[:,0])
  
  # create an empty sed
  sed = np.zeros((nl,nZ,nAge)) 
  
  # create   
  ages   = 10**(6+0.1*(np.arange(nAge)))
  metals = np.array(list(Zdic.keys()))
  lmbd   = data[:,0]
  
  # ratio, initial mass, final mass 
  # this is a very crude approximation
  MiniMfin = np.ones((nZ,nAge))*0.75
  
  
  # loop over all files, i.e, over metallicities:
  
  for f in files:
    # find the metallicity
    for Z,string in Zdic.items():
      if f.find(string)>-1:
        break
    
    # find the metallicity index
    Z_idx = list(Zdic.keys()).index(Z)
    
    # read the file
    print("%2d %8.3f %s"%(Z_idx, Z, f))
    data = np.loadtxt(f)
        
    # copy at the right place
    sed[:,Z_idx,:] = data[:,1:]
    
  # units conversion
  sed  = sed*u.L_sun/u.AA              # in Solar Luminosities per Angstrom
  sed  = sed.to(u.erg/u.s/u.AA)        # in erg/s/AA
  sed  = sed/1e6                       # for one Msol 
  sed  = sed.value
  

  print("Done with BPASS files.")

  return ages,metals,lmbd,sed,MiniMfin  
    
  
  




def ReadFitsSED(filename):
  """
  read and SED in a fits format as defined in
  Patrik-imfKroupa-Zmulti-ml.fits
  """

  import astropy.io.fits as pyfits
  hdulist = pyfits.open(filename)
  
  # SED
  data2 = hdulist[2]
  
  # current_mass        # SSP initial mass [Msol] at current age
  data3 = hdulist[3]      
  MiniMfin = 1/data3.data # initial mass/current mass
  
  # axes
  data4 = hdulist[4]  
  data4.data.columns  # ColDefs(
                      #  name = 'time'; format = 'D'; unit = 'yr'
                      #  name = 'metallicity'; format = 'D'
                      #  name = 'lambda'; format = 'D'; unit = 'm'
                      # )
  
  tmp = np.array(list(data4.data))
  
  nl = data2.shape[0]
  nm = data2.shape[1]
  na = data2.shape[2]
  
  ages  = tmp[:,0][:na]    # ages      
  metals = tmp[:,1][:nm]   # metals    
  lmbd   = tmp[:,2][:nl]   # lmbd  
  
  # units conversion
  lmbd = lmbd*1e10   # m to angstrom
  
  
  # sed : convert to erg/s/angstrom
  sed   = 10**data2.data  # in W/m normalized to 1 Msol (Initial IMF mass)
  sed = sed*u.W/u.m
  sed = sed.to(u.erg/u.s/u.angstrom)
  sed = sed.value
  
  return ages,metals,lmbd,sed,MiniMfin


                    
####################################################################
# main
####################################################################


# compute cmdline
cmdline = ""
for elt in sys.argv:
 cmdline = cmdline + " " + elt
cmdline = cmdline[1:]


def Do(opt):
  
  
  #############################################
  # Open the SED file
  ############################################# 
  from matplotlib import pylab as plt
  from pNbody.Mockimgs import filters
  
  Filter = filters.FilterCl("../filters/SLOAN_SDSS.z.dat")
  Filter.resample()
  
  
  MH  = -1.3
  Age = 12e9   # 12e9  
  Z   = 0.02* 10**(MH)  
  
  binsAge,binsZ,lmbd,sed,MiniMfin = ReadFitsSED(opt.sed)
  idx_a = np.argmin(np.fabs(Age-binsAge))
  idx_m = np.argmin(np.fabs(Z-binsZ))
  sed = sed[:,idx_m,idx_a]
  SED = spectra.SED(sed*u.erg/u.s/u.angstrom,lmbd*u.angstrom)
  SED.resample()
  SED.computeAbsoluteFlux()
  plt.plot(SED.get_wavelength(),SED.get_sed(),label="SB99")
  #plt.scatter(SED.get_wavelength(),SED.get_sed(),s=1)
  M = SED.ABMagnitude(Filter)
  print("SB99",M,10**(M/-2.5)) 
  

  
  # BPASS
  binsAge,binsZ,lmbd,sed,MiniMfin = Read_BPASS_SED(opt.sed_files,plt)
  idx_a = np.argmin(np.fabs(Age-binsAge))
  idx_m = np.argmin(np.fabs(Z-binsZ))
  print(binsZ[idx_m])
  sed = sed[:,idx_m,idx_a]
  SED = spectra.SED(sed*u.erg/u.s/u.angstrom,lmbd*u.angstrom)
  SED.resample()
  SED.computeAbsoluteFlux()
  plt.plot(SED.get_wavelength(),SED.get_sed(),label="BPASS")  
  M = SED.ABMagnitude(Filter)
  print("BPASS",M,10**(M/-2.5))   
  
  
  # BPASS jablonka
  '''
  from astropy.io import ascii
  data = ascii.read('bpass_v2.2.1/spectra-bin-imf_chab300.z020.dat')
  lmbd = data['col1']
  n = 43
  age =  10**(6+0.1*(n-2)) /1.e9
  sed = data['col'+str(n)]
  sed = sed*u.L_sun/u.AA       
  sed = sed.to(u.erg/u.s/u.AA) 
  sed = sed/1e6   
  sed = sed.value             
  print(sed)
  plt.plot(lmbd, sed, label= str(age)+' Gyr' )
  '''

  '''
  # BPASS raw (working)
  data = np.loadtxt("bpass_v2.2.1/spectra-bin-imf_chab300.z020.dat")
  lmbd   = data[:,0]
  sed = data[:,1:]
  sed = sed*u.L_sun/u.AA       
  sed = sed.to(u.erg/u.s/u.AA) 
  sed = sed/1e6   
  sed = sed.value       
  sed = sed[:,n-2]        # ok
  print(sed)
  plt.plot(lmbd, sed, label= str(age)+' Gyr' )
  '''




  
  
  #plt.loglog()
  #plt.semilogy()
  plt.xlim(3000,9000)
  #plt.ylim(1e31,0.2e36)
  
  plt.xlabel("wavelength [A]")
  
  plt.legend()
  
  plt.show()

  
  
  
  
  
 
 
 
 
    

if __name__ == '__main__':
  
  opt = parser.parse_args()
  
  Do(opt)
 
