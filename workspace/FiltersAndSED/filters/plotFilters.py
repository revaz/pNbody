#!/usr/bin/env python3

from pNbody.Mockimgs import filters
from astropy import units as u
from matplotlib import pyplot as plt
import argparse

def SetAxis(ax,xmin, xmax, ymin, ymax, log=None):
  """
  Set ticks for the axis
  """
  
  # slightly extend the limits
  if xmin is not None and xmax is not None:
    if xmin == xmax:
        xmin = xmin - 0.05 * xmin
        xmax = xmax + 0.05 * xmax
    else:
        xmin = xmin - 0.05 * (xmax - xmin)
        xmax = xmax + 0.05 * (xmax - xmin)  
  
  if ymin is None and ymax is None:
    ymin,ymax = ax.get_ylim()
    
  if ymin == ymax:
      ymin = ymin - 0.05 * ymin
      ymax = ymax + 0.05 * ymax
  else:
      ymin = ymin - 0.05 * (ymax - ymin)
      ymax = ymax + 0.05 * (ymax - ymin)
  
       


  plt.axis([xmin, xmax, ymin, ymax])


  if log is None:
      log = 'None'

  if str.find(log, 'x') == -1:
      ax.xaxis.set_major_locator(plt.AutoLocator())
      x_major = ax.xaxis.get_majorticklocs()
      dx_minor = (x_major[-1] - x_major[0]) / (len(x_major) - 1) / 5.
      ax.xaxis.set_minor_locator(plt.MultipleLocator(dx_minor))


  if str.find(log, 'y') == -1:
      ax.yaxis.set_major_locator(plt.AutoLocator())
      y_major = ax.yaxis.get_majorticklocs()
      dy_minor = (y_major[-1] - y_major[0]) / (len(y_major) - 1) / 5.
      ax.yaxis.set_minor_locator(plt.MultipleLocator(dy_minor))


####################################################################
# option parser
####################################################################

description="""Plot SEDs from hdf5 files
"""

epilog     ="""
Examples:
--------
ark_plotSED SED.hdf5
ark_plotSED --normalize SED1.hdf5 SED2.hdf5
"""

parser = argparse.ArgumentParser(description=description,epilog=epilog,formatter_class=argparse.RawDescriptionHelpFormatter)



parser.add_argument("-o",
                    action="store",
                    type=str,
                    dest="outputfilename",
                    default=None,
                    help="Name of the output file")  

                    
                    
parser.add_argument('--xmin',
                    action="store", 
                    dest="xmin", 
                    metavar='FLOAT', 
                    type=float,
                    default=2000,
                    help='x min')

parser.add_argument('--xmax',
                    action="store", 
                    dest="xmax", 
                    metavar='FLOAT', 
                    type=float,
                    default=16000,
                    help='x max')
                    
parser.add_argument('--ymin',
                    action="store", 
                    dest="ymin", 
                    metavar='FLOAT', 
                    type=float,
                    default=None,
                    help='y min')

parser.add_argument('--ymax',
                    action="store", 
                    dest="ymax", 
                    metavar='FLOAT', 
                    type=float,
                    default=None,
                    help='y max')

parser.add_argument('--log',
                    action="store", 
                    dest="log", 
                    metavar='STR', 
                    type=str,
                    default=None,
                    help='log scale (None,x,y,xy)')



def Do(opt):
  

    


  params = {
    "axes.labelsize": 14,
    "axes.titlesize": 18,
    "font.size": 12,
    "legend.fontsize": 12,
    "xtick.labelsize": 14,
    "ytick.labelsize": 14,
    "text.usetex": True,
    "figure.subplot.left": 0.15,
    "figure.subplot.right": 0.95,
    "figure.subplot.bottom": 0.15,
    "figure.subplot.top": 0.95,
    "figure.subplot.wspace": 0.02,
    "figure.subplot.hspace": 0.02,
    "figure.figsize" : (8, 6),
    "lines.markersize": 6,
    "lines.linewidth": 2.0,
  }
  plt.rcParams.update(params)



  # create the plot
  fig = plt.gcf()
  fig.set_size_inches(8,6)
  ax  = plt.gca()

  ARRA_VIS1 = filters.FilterCl("./ARRAKIHS_VIS1.dat")
  ARRA_VIS2 = filters.FilterCl("./ARRAKIHS_VIS2.dat")
  ARRA_NIR1 = filters.FilterCl("./ARRAKIHS_NIR1.dat")
  ARRA_NIR2 = filters.FilterCl("./ARRAKIHS_NIR2.dat")
  
  
  
  
  ax.plot(ARRA_VIS1.get_wavelength(),ARRA_VIS1.get_response(),lw=3)
  ax.plot(ARRA_VIS2.get_wavelength(),ARRA_VIS2.get_response(),lw=3)
  ax.plot(ARRA_NIR1.get_wavelength(),ARRA_NIR1.get_response(),lw=3)
  ax.plot(ARRA_NIR2.get_wavelength(),ARRA_NIR2.get_response(),lw=3)
  
  ax.set_xlabel("wavelength [A]")
  ax.set_ylabel("filter response")
  
  SetAxis(ax,opt.xmin,opt.xmax,opt.ymin,opt.ymax,log=None)
  
    
  # save or display
  if opt.outputfilename:
    plt.savefig(opt.outputfilename)
  else:
    plt.show()    






if __name__ == '__main__':
  
  opt = parser.parse_args()
  
  Do(opt)
 
