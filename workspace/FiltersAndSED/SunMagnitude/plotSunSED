#!/usr/bin/env python3

import argparse

from pNbody.Mockimgs import filters
from pNbody.Mockimgs import spectra
import numpy as np
import astropy.io.fits as pyfits
from astropy import units as u

from matplotlib import pylab as plt



####################################################################
# option parser
####################################################################

description="""compute the sun magnitude in different filters
"""


epilog     ="""
Examples:
--------
getSunMagnitude ../filters/*.dat
getSunMagnitude --photometric-systems ST ../filters/*.dat
"""


parser = argparse.ArgumentParser(description=description,epilog=epilog,formatter_class=argparse.RawDescriptionHelpFormatter)


             

def readSunSpectrum(mode=None):
  """
  return the sun spectrum 
  l  (wavelength) in angstrom
  fl (flux)       in erg/ (s cm**2 angstrom)
  
  this corresponds to the sun as observed from the Earth
  """
  
  if mode == 'fast':
    # this is an approximation of the sun spectrum of Willmer 2018
    data = np.loadtxt("SunSpectrum.txt")
    l  = data[:,0]*u.micrometer
    fl = data[:,1]*u.erg/(u.s*u.cm**2*u.angstrom) 
    l = l.to(u.angstrom)
  
  else:  
    # sun spectrum from Willmer 2018
    # http://mips.as.arizona.edu/~cnaw/sun_composite.fits    
    #name = 'WAVE'; format = 'D'; unit = 'Angstrom'
    #name = 'FLUX'; format = 'D'; unit = 'erg s**-1 cm**-2 A**-1'    
    hdulist = pyfits.open("sun_composite.fits")
    data = np.array(list(hdulist[1].data))
    l  = data[:,0]*u.AA
    fl = data[:,1]*u.erg/(u.s*u.cm**2*u.AA)
  
  return l,fl




####################################################################
# main
####################################################################


if __name__ == '__main__':
  
  opt = parser.parse_args()


  ################################################
  # SUN SED
  ################################################
  
  l,fl = readSunSpectrum()  # u.erg/(u.s*u.cm**2*u.AA)
  SunSED = spectra.SED(fl,l)
  SunSED.resample()
  #SunSED.toAbsoluteFlux()
  
  

  # plot SED
  plt.plot(SunSED.get_wavelength(),SunSED.data)
  plt.xlim(1e2,20000)
  #plt.ylim(1e31,0.2e36)
  plt.xlabel("wavelength [A]")
  plt.ylabel("SED [erg/(s*cm**2*A)]")
  plt.show()
  
  
  
  
  
  



  




